#!/bin/bash
################################################################
#
# Install or update scripts and configuration
# Install or update the database
# Assumption: Everything runs on one server
#
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
################################################################

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

if [ $EUID -ne 0 ]
then
	echo "You must be 'root' to execute this script"
	exit 1
fi

DEBIAN=0
[ -f /etc/debian_version ] && DEBIAN=1

if [ $DEBIAN -ne 0 ]
then
	DPKG2INSTALL=''
	# Install the necessary debian packages
	for DPKG in libstring-random-perl libdbd-mysql-perl
	do
		# Skip already installed packages
		dpkg-query -W $DPKG 2> /dev/null
		[ $? -ne 0 ] && DPKG2INSTALL="$DPKG $DPKG2INSTALL"
	done
	# Install any missing packages
	[ -z "$DPKG2INSTALL" ] || apt-get install $DPKG2INSTALL
fi

# Determine the user name and password for the database
#  (used in several scripts)
DBUser=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-8})
DBPass=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-16})

# Get the base URL for the landing site for fake emails
HTTPBase=''
while [ -z "$HTTPBase" ]
do
    read -p 'Base URL for web page referenced in emails [e.g. "http://sev.btoy1.net"]  ? ' UIN
    [ -z "$UIN" ] && continue

    # Check whether host exists
    HTTPHost=$(sed -e 's@http://@@;s@https://@@;s@/.*$@@' <<< $UIN)
    host -t A -t CNAME $HTTPHost &> /dev/null
    [ $? -ne 0 ] && continue

    HTTPBase="$UIN"
done

# Get the name or IP address of the database server
#  (the default installation is "everything on one server")
DBSERVER='localhost'
while [ 1 ]
do
    read -p "Name or IP address of database server [default='$DBSERVER'] ? " UIN
    [ -z "$UIN" ] || DBSERVER="$UIN"
    nc -z -w 20 $DBSERVER 3306
    [ $? -eq 0 ] && break
done

# Get the directory for the web scripts
WEBDIR='/var/www'
read -p "Directory for web scripts [default='$WEBDIR'] ? " UIN
[ -z "$UIN" ] || WEBDIR="$UIN"
mkdir -p "$WEBDIR"

# Adapt the SQL input file
sed -i -e "s/%%%DBUSER%%%/$DBUser/;s/%%%DBPASS/$DBPass/" ${SRC_DIR}/sea.sql

# Tell operator to install database on remote server
if [ "T$DBSERVER" = 'Tlocalhost' ]
then
    cat << EOT
Copy the file "sea.sql" to the database server and import it into the database:
mysql -v < sea.sql
EOT
fi

# Install the processing script
touch /usr/local/sbin/sea-processing.pl
chmod 0700 /usr/local/sbin/sea-processing.pl
sed -e "s/%%%DBUSER%%%/$DBUser/;s/%%%DBPASS/$DBPass/" \
  ${SRC_DIR}/usr/local/sbin/sea-processing.pl > /usr/local/sbin/sea-processing.pl
chmod 0700 /usr/local/sbin/sea-processing.pl

# Install the web server scripts
cp -a web ${WEBDIR}
sed -e "s/%%%DBUSER%%%/$DBUser/;s/%%%DBPASS/$DBPass/;s/%%%DBSERVER%%%/$DBServer/" \
  ${SRC_DIR}/web/admin/db.inc > ${WEBDIR}/admin/db.inc

# Install the cron job
cat << EOT > /etc/cron.d/sea
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin
MAILTO=%%%MAILTO%%%
##############################################################################
# Process any Social Engineering Assessments
* * * * *       root    [ -x /usr/local/sbin/sea-processing.pl ] && /usr/local/sbin/sea-processing.pl -D %%%DBSERVER%%% -B %%%HTTPBASE%%% 
EOT
sed -i -e "s/%%%DBSERVER%%%/$DBServer/;s/%%%HTTPBASE%%%/$HTTPBase/" \
  ${SRC_DIR}/etc/cron.d/sea > /etc/cron.d/sea

# We are done 
exit 0
