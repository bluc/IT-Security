<?
/*
 * Original can be found at:
 * http://www.phpkode.com/source/s/login-script-with-pdo/login-script-with-pdo/sys.class.php
 */

require_once rtrim($_SERVER["DOCUMENT_ROOT"],"/")."/admin/db.inc";

// author oran - email: hide@address.com
class Settings
{
  protected $dbh;
  function __construct()
  {
    try {
      $GLOBALS['dbh'] = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_UserID'], $GLOBALS['DB_Passwd'],
                                array(PDO::ATTR_PERSISTENT => true));
    } catch (PDOException $e) {
      echo 'Connection failed: ' . $e->getMessage();
    }
  }
}

class Registration extends Settings
{
  protected $Username;
  protected $Password;
  protected $Email;
	
  /**
   * @param $username
   * @return insert to private var
   */
  function SetUsername($username)
  {
    return $this->Username = $username;
  }
  /**
   * @return username
  */
  function GetUsername()
  {
    return $this->Username;
  }
  /**
   * @param $password
   * @return insert to private password var
   */
  function SetPassword($password)
  {
    return $this->Password = sha1($password);
  }
  /**
   * @param $Email
   * @return insert to private var
   */
  function SetEmail($email)
  {
    return $this->Email = $email;
  }
  /**
   * @return email
   */
  function GetEmail()
  {
    return $this->Email;
  }
  /**
   * @return array of erros if any
   */
  function Validate()
  {
    $errors =  array();
		
    // username must be at list 3 charecters
    if((strlen($this->Username)) < 3 )
    {
      $errors[] = "Username Must be at list 3 charcters";
    } // end username check
  // valid mail
    $EmailPattern = '/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])'
     . '(([a-z0-9-])*([a-z0-9]))+'
     . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i';
		
    $IsEmailValid = preg_match($EmailPattern, $this->Email);
    if($IsEmailValid < 1)
    {
      $errors[] = "Email not Valid";
    }
    if((strlen($this->Password)) < 5 )
    {
      $errors[] = "password Must be at list 5 charcters";
    }

    return $errors;
  }
  /**
   * @return return array of errors if false
   * return true if o.k
   */
  function InsertUserToSql()
  {
    $error = $this->Validate();
    if(count($error) > 0 )
    {
      return $error;	
    }
    else
    {
      $stmt = $GLOBALS['dbh']->prepare("INSERT INTO users (username, password, email, regdate) VALUES (:username, :password, :email, :regdate)");
      $stmt->bindParam(':username', $this->Username);
      $stmt->bindParam(':password', $this->Password);
      $stmt->bindParam(':email', $this->Email);
      $stmt->bindParam(':regdate', time());
      $stmt->execute();
      $arr = array();
      $arr = $stmt->errorInfo();
      return $arr;
    }
  }	
}

class login extends Registration
{
  /**
   * @return if false return false if ok return session 
   */
  function CheckLogin()
  {
    $stmt = $GLOBALS['dbh']->prepare("SELECT username, password FROM users
      WHERE username = :username AND password = :password");
    $stmt->bindParam(':username', $this->Username);
    $stmt->bindParam(':password', $this->Password);
    $stmt->execute();
			
    if($stmt->rowCount() > 0 )
    {	
      $_SESSION['username'] = $this->Username;
    }
    else
    {
      return false;
    }
  }
}

class ChangeSetting extends Registration
{
  /**
   * @return return true on succsess
   */
  function ChangeMail()
  {
    $EmailPattern = '/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])' .
			'(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i';
		
    $IsEmailValid = preg_match($EmailPattern, $this->Email);
    if($IsEmailValid > 0)
    {
      $stmt = $GLOBALS['dbh']->prepare("UPDATE users SET email = :email WHERE username = :username");
      $stmt->bindParam(':email', $this->Email);
      $stmt->bindParam(':username', $_SESSION['username']);
      $stmt->execute();
      return true;
    }
    else
    {
      return false;
    }
  }

  protected  $oldpass;
			
  function SetOldPass($oldpass)
  {
    return $this->oldpass = sha1($oldpass);
  }
		
  function ChangePass()
  {
    $stmt = $GLOBALS['dbh']->prepare("UPDATE users SET password = :password  WHERE username = :username AND password = :oldpass");
    $stmt->bindParam(':password', $this->Password);
    $stmt->bindParam(':oldpass', $this->oldpass);
    $stmt->bindParam(':username', $_SESSION['username']);
    $stmt->execute();
  }

  function SendMailForNewPass()
  {
    $voucher = rand() . rand() . rand();
    $stmt = $GLOBALS['dbh']->prepare("UPDATE users SET CpassReqDate = :CpassReqDate, voucher = :voucher WHERE email = :email");
    $stmt->bindParam(':CpassReqDate', time());
    $stmt->bindParam(':email', $this->Email);
    $stmt->bindParam(':voucher', $voucher);
    $stmt->execute();
			
    mail($this->Email,"new pass ",
       "this is link for change pass : <a href='localhost/loginsys/sample.php/?newpassv=$voucher>'");
  }

  function SetNewPass()
  {
    if(!empty($_GET['newpassv']))
    {
      $voucher = $_GET['newpassv'];
      $d = time() - 1800;
      $stmt = $GLOBALS['dbh']->prepare("SELECT CpassReqDate, email FROM users WHERE voucher = :voucher");
      $stmt->bindParam(':voucher', $voucher);
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
    $TimeLimit = $result[0]['CpassReqDate'] - time();
    if($TimeLimit > 1800)
    {
      echo "Voucher expired";
    }
    else
    {
      $stmt = $GLOBALS['dbh']->prepare("UPDATE users SET password = :password WHERE email = :email");
      $email = $result[0]['email'];
      $pass = rand() . rand();
      $stmt->bindParam(':password', sha1($pass));
      $stmt->bindParam(':email', $email);
      $stmt->execute();
      mail($email,"your new pass", "the new pass is<b>:  $pass</b>");
      }
    }
  }
		
  function Logout()
  {
    session_start();
    unset($_SESSION['username']);
  }
}
?>
