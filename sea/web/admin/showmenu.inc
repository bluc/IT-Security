<?
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// The "Assessments" submenu
print <<< END
    <p>
    =&gt; <a href="{$_SERVER['PHP_SELF']}?action=assessments">Assessments</a>

END;
if ($ScriptAction == 'assessments')
{
  $NewAssessmentID = sprintf ("%012s-%012d", date('U'), intval(rand(0,9999990)));
  print <<< END
    <br>
    &nbsp;&raquo; <a href="{$_SERVER['PHP_SELF']}?action=assessments&subaction=new&assessmentid=$NewAssessmentID">Create new assessment</a>
    <p>

END;
  // Show existing assessments (from the database)
  $Results = GetAssessments();
  if (($Results[0] == 'no error') && (count($Results) > 1))
  {
    // Show the assessments (starting at index 1)
    foreach(array_slice($Results, 1) as $val)
    {
      $ExistingAssessmentID = $val['id'];
      print <<< END
    &nbsp;&raquo; {$val['Name']} (Status: {$val['Status']})
    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &brvbar;<a href="{$_SERVER['PHP_SELF']}?action=assessments&subaction=edit&assessmentid=$ExistingAssessmentID">Edit</a>
END;
      if (! $val['ro'])
      {
        print <<< END
    &brvbar;<a href="{$_SERVER['PHP_SELF']}?action=assessments&subaction=delete&assessmentid=$ExistingAssessmentID" onclick="return AreYouSure();">Delete</a>
END;
      }
      if (preg_match("/^(?:R|F)$/", $val['Status']))
      {
        print <<< END
    &brvbar;<a href="{$_SERVER['PHP_SELF']}?action=assessments&subaction=results&assessmentid=$ExistingAssessmentID">Results</a>
    &brvbar;<a href="{$_SERVER['PHP_SELF']}?action=assessments&subaction=results_csv&assessmentid=$ExistingAssessmentID">CSV results</a>
END;
      }
      print <<< END
    <p>

END;
    }
  }
}

// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// The "Email Recipient Lists" submenu
print <<< END
    <p>
    =&gt; <a href="{$_SERVER['PHP_SELF']}?action=email_recipient_lists">Email recipient lists</a>

END;

if ($ScriptAction == 'email_recipient_lists')
{
  $NewListID = sprintf ("%012s-%012d", date('U'), intval(rand(0,9999990)));
  print <<< END
    <br>
    &nbsp;&raquo; <a href="{$_SERVER['PHP_SELF']}?action=email_recipient_lists&subaction=upload&listid=$NewListID">Upload new list</a> (csv file)
    <br>
    &nbsp;&raquo; <a href="{$_SERVER['PHP_SELF']}?action=email_recipient_lists&subaction=new&listid=$NewListID">Create new list</a><br>
    <p>

END;
  // Show existing lists (from the database)
  $Results = GetEmailRecipientsLists();
  if (($Results[0] == 'no error') && (count($Results) > 1))
  {
    // Show the lists (starting at index 1)
    foreach(array_slice($Results, 1) as $val)
    {
      $ExistingListID = $val['id'];
      print <<< END
    &nbsp;&raquo; {$val['Name']} ({$val['LastUpdate']})
    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &brvbar;<a href="{$_SERVER['PHP_SELF']}?action=email_recipient_lists&subaction=edit&listid=$ExistingListID">Edit</a>
END;
      if (! $val['ro'])
      {
        print <<< END
    &brvbar;<a href="{$_SERVER['PHP_SELF']}?action=email_recipient_lists&subaction=delete&listid=$ExistingListID" onclick="return AreYouSure();">Delete</a>
END;
      }
      print <<< END
    <p>

END;
    }
  }
}

// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// The "Email Templates" submenu
print <<< END
    <p>
    =&gt; <a href="{$_SERVER['PHP_SELF']}?action=email_templates">Email templates</a>

END;
if ($ScriptAction == 'email_templates')
{
  $NewTemplateID = sprintf ("%012s-%012d", date('U'), intval(rand(0,9999990)));
  print <<< END
    <br>
    &nbsp;&raquo; <a href="{$_SERVER['PHP_SELF']}?action=email_templates&subaction=new&templateid=$NewTemplateID">Create new template</a>
    <p>

END;
  // Show existing templates (from the database)
  $Results = GetEmailTemplates();
  if (($Results[0] == 'no error') && (count($Results) > 1))
  {
    // Show the templates (starting at index 1)
    foreach(array_slice($Results, 1) as $val)
    {
      $ExistingTemplateID = $val['id'];
      print <<< END
    &nbsp;&raquo; {$val['Name']} ({$val['LastUpdate']})
    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &brvbar;<a href="{$_SERVER['PHP_SELF']}?action=email_templates&subaction=edit&templateid=$ExistingTemplateID">Edit</a>
END;
      if (! $val['ro'])
      {
        print <<< END
    &brvbar;<a href="{$_SERVER['PHP_SELF']}?action=email_templates&subaction=delete&templateid=$ExistingTemplateID" onclick="return AreYouSure();">Delete</a>
END;
      }
      print <<< END
    <p>

END;
    }
  }
}

// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// The "Web Templates" submenu
print <<< END
    <p>
    =&gt; <a href="{$_SERVER['PHP_SELF']}?action=web_templates">Web templates</a>

END;
if ($ScriptAction == 'web_templates')
{
  $NewTemplateID = sprintf ("%012s-%012d", date('U'), intval(rand(0,9999990)));
  print <<< END
    <br>
    &nbsp;&raquo; <a href="{$_SERVER['PHP_SELF']}?action=web_templates&subaction=new&templateid=$NewTemplateID">Create new template</a>
    <p>
END;
  // Show existing templates (from the database)
  $Results = GetWebTemplates();
  if (($Results[0] == 'no error') && (count($Results) > 1))
  {
    // Show the templates (starting at index 1)
    foreach(array_slice($Results, 1) as $val)
    {
      $ExistingTemplateID = $val['id'];
      print <<< END
    &nbsp;&raquo; {$val['Name']} ({$val['LastUpdate']})
    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &brvbar;<a href="{$_SERVER['PHP_SELF']}?action=web_templates&subaction=edit&templateid=$ExistingTemplateID">Edit</a>
END;
      if (! $val['ro'])
      {
        print <<< END
    &brvbar;<a href="{$_SERVER['PHP_SELF']}?action=web_templates&subaction=delete&templateid=$ExistingTemplateID" onclick="return AreYouSure();">Delete</a>
END;
      }
      print <<< END
    <p>

END;
    }
  }
}

// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// The "catch all" "return to main menu"
print <<< END
    <p>
    Return to the <a href="{$_SERVER['PHP_SELF']}">main page</a>
 </div>

END;
?>
