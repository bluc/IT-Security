<?
// See: http://www.phpzag.com/convert-mysql-datetime-to-timestamp/
switch ($ScriptAction)
{
  case 'assessments':
    // # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    // The "Assessments" contents
    $SB_Value = '';

    // Get the list of valid email recipient lists
    $ValidERL = array();
    $Results = GetEmailRecipientsLists();
    if (($Results[0] == 'no error') && (count($Results) > 1))
    {
      // Show the lists (starting at index 1)
      $ValidERL = array_slice($Results, 1);
    }

    // Get the list of valid email templates
    $ValidET = array();
    $Results = GetEmailTemplates();
    if (($Results[0] == 'no error') && (count($Results) > 1))
    {
      // Show the lists (starting at index 1)
      $ValidET = array_slice($Results, 1);
    }

    // Get the list of valid web templates
    $ValidWT = array();
    $Results = GetWebTemplates();
    if (($Results[0] == 'no error') && (count($Results) > 1))
    {
      // Show the lists (starting at index 1)
      $ValidWT = array_slice($Results, 1);
    }

    switch($SubAction)
    {
      case 'new':
        // Create a new assessment

        // Check for usable email recipients lists
        if (count($ValidERL) == 0)
        {
          feedback("Please upload or create email recipient lists first", false);
        }

        // Check for usable email templates
        if (count($ValidET) == 0)
        {
          feedback("Please create email templates first", false);
        }

        // Check for usable web templates
        if (count($ValidWT) == 0)
        {
          feedback("Please create web templates first", false);
        }

        // Meta info
        $A_Name = '';
        $A_Status = '';

        // Email related data
        $A_EmailSender = '';
        $A_EmailSubject = '';
        $A_Signature = '';

        // Web related data
        $A_WebURL = '';
        $A_CompanyLogoURL = '';

        // Start and end times
        $A_StartTime = date('r');
        $A_EndTime = date('r');

        // The references to the used templates
        $A_ER_id = '';
        $A_ET_id = '';
        $A_WT_id = '';

        $SB_Value = 'Save';
        $A_ReadOnly = '';
        break;

      case 'edit':

        $Results = GetAssessments("$AssessmentID");
        if (($Results[0] == 'no error') && (count($Results) == 2))
        {
          // Show the assessment (starting at index 1)
          foreach(array_slice($Results, 1) as $val)
          {
            // Meta info
            $A_Name = $val['Name'];
            $A_Status = $val['Status'];

            // Email related data
            $A_EmailSender = $val['EmailSender'];
            $A_EmailSubject = $val['EmailSubject'];
            $A_Signature = $val['Signature'];

            // Web related data
            $A_WebURL = $val['WebURL'];
            $A_CompanyLogoURL = $val['CompanyLogoURL'];

            // Start and end times
            $A_StartTime = date('r',$val['StartTime']);
            $A_EndTime = date('r',$val['EndTime']);

            // The references to the used templates
            $A_ER_id = $val['ER_id'];
            $A_ET_id = $val['ET_id'];
            $A_WT_id = $val['WT_id'];

            // Read-only?
            $A_ReadOnly = $val['ro'] ? ' disabled="disabled"' : '';
          }
        }
        $SB_Value = 'Update';
        break;

      case 'delete':
        DeleteAssessment ($AssessmentID);
        feedback ("Assessment '$AssessmentID' deleted", false);
        break;

      case 'results':
        // Show assessment results (if present)
        $Results = GetAssessmentResults($AssessmentID);
        if (($Results[0] == 'no error') && (count($Results) > 1))
        {
          print <<< END
  <table border='5' cellspacing='0'>
   <tr>
    <th>Email</th>
    <th>Date</th>
    <th>IP (Client/Real)</th>
    <th>Browser ID</th>
    <th>Other</th>
   </tr>
END;
          // Show the results (starting at index 1)
          foreach(array_slice($Results, 1) as $val)
          {
          print <<< END
   <tr>
    <td>{$val['R_Email']}</td>
    <td>{$val['R_Date']}</td>
    <td>{$val['R_ClientIP']} {$val['R_RealIP']}</td>
    <td>{$val['R_BrowserID']}</td>
    <td>{$val['R_Other']}</td>
   </tr>
END;
          }
          print <<< END
  </table>
END;
      } else
      {
        print "No results recorded\n";
      }
      break;

      case 'results_csv':
        $out = fopen('php://output', 'w');
        $Results = GetAssessmentResults($AssessmentID);
        if (($Results[0] == 'no error') && (count($Results) > 1))
        {
          // Show the results (starting at index 1)
          fputs ($out, "### ");
          fputcsv ($out, array_keys(array_slice($Results[1], 1)));
          fputs ($out, "<br>\n");
          foreach(array_slice($Results, 1) as $val)
          {
            fputcsv($out, array_slice($val,1), ',', '"');
            fputs ($out, "<br>\n");
          }
        }
        fclose($out);
        break;
    }

    if (strlen ($SB_Value))
    {
      // Show the template contents
      $array = explode("\n", trim($A_Signature));
      $rows = count($array) + 5;
      print <<< END
    <div class="textwrapper">
      <span class="bold13">&nbsp;&nbsp;Assessment: </span>
      <input type="text" name="A_Name" size="20" maxlength="254" value="$A_Name" placeholder="Input the assessment's name" required x-moz-errormessage="Input the assessment's name">
      Status: <select name="A_Status">

END;
     print "        <option value='N'";
     if ($A_Status == 'N') print " selected";
     print ">new</option>\n";

     print "        <option value='S'";
     if ($A_Status == 'S') print " selected";
     print ">scheduled</option>\n";

     print "        <option value='R' disabled";
     if ($A_Status == 'R') print " selected";
     print ">running</option>\n";

     print "        <option value='F'";
     if ($A_Status == 'F') print " selected";
     print ">finished</option>\n";
      print <<< END
      </select>
      <hr>
      Email <u>from</u> address: <input type="text" name="A_EmailSender" size="20" maxlength="254" value="$A_EmailSender" placeholder="Input the email sender" required x-moz-errormessage="Input the email sender">
      Email subject: <input type="text" name="A_EmailSubject" size="20" maxlength="254" value="$A_EmailSubject" placeholder="Input the email subject" required x-moz-errormessage="Input the email subject">
      <br>
      Email signature:<br>
      <textarea cols='1' rows='$rows' name='A_Signature'>$A_Signature</textarea>
      <hr>
      Web URL: <input type="text" name="A_WebURL" size="20" maxlength="254" value="$A_WebURL" placeholder="Input the web URL" required x-moz-errormessage="Input the web URL">
      URL for company logo: <input type="text" name="A_CompanyLogoURL" size="20" maxlength="254" value="$A_CompanyLogoURL" placeholder="Input the URL of company logo" required x-moz-errormessage="Input the URL of company logo">
      <hr>
      Specify a start date and time: <input id="A_StartTime" name='A_StartTime' type="text" size="30" value="$A_StartTime">
        <a href="javascript:NewCal('A_StartTime','ddmmmyyyy',true,12)">
         <img src="cal.gif" width="16" height="16" border="0" alt="Pick a date">
       </a>
      <br>
      Specify a end date and time: &nbsp;<input id="A_EndTime" name='A_EndTime' type="text" size="30" value="$A_EndTime">
        <a href="javascript:NewCal('A_EndTime','ddmmmyyyy',true,12)">
         <img src="cal.gif" width="16" height="16" border="0" alt="Pick a date">
       </a>
      <hr>
      Email recipient list to use:
      <select name="A_ER_id">
END;
      // All valid email recipient lists
      foreach($ValidERL as $val)
      {
        $t = "  <option value='" . $val['id'] . "'";
        if ($val['id'] == $A_ER_id) $t .= ' selected';
        $t .= ">" . $val['Name'] . "</option>\n";
        print "$t\n";
      }
      print <<< END
      </select>

      Email template to use:
      <select name="A_ET_id">
END;
      // All valid email template lists
      foreach($ValidET as $val)
      {
        $t = "  <option value='" . $val['id'] . "'";
        if ($val['id'] == $A_ET_id) $t .= ' selected';
        $t .= ">" . $val['Name'] . "</option>\n";
        print "$t\n";
      }
      print <<< END
      </select>

      Web template to use:
      <select name="A_WT_id">
END;
      // All valid web template lists
      foreach($ValidWT as $val)
      {
        $t = "  <option value='" . $val['id'] . "'";
        if ($val['id'] == $A_WT_id) $t .= ' selected';
        $t .= ">" . $val['Name'] . "</option>\n";
        print "$t\n";
      }
      print <<< END
      </select>
    </div>
    &nbsp;&nbsp;<input type="submit" value=" $SB_Value " class="submit" name="A_Save" $A_ReadOnly>
    &nbsp;&nbsp;<input type="reset" value=" Revert " class="reset">
    <input type="hidden" name="A_ID" value="$AssessmentID">
END;
    }
    break;

  case 'email_recipient_lists':
    // # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    // The "Email Recipient Lists" contents
    $SB_Value = '';
    switch ($SubAction)
    {
      case 'upload':
        $L_Name = '';

        print <<< END
    <div class="textwrapper">
      <span class="bold13">&nbsp;&nbsp;List: </span>
      <input type="text" name="L_Name" size="20" maxlength="254" value="$L_Name" placeholder="Input the list's name" required x-moz-errormessage="Input the list's name">
      <p>
      &nbsp;&nbsp;Select file: <input type="file" name="L_NewCSV">
    </div>
    &nbsp;&nbsp;<input type="submit" value=" Upload " class="submit" name="L_Upload">
    &nbsp;&nbsp;<input type="reset" value=" Start over " class="reset">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
    <input type="hidden" name="L_ID" value="$ListID">

END;
        break;

      case 'new':
        // Create a new list
        $L_Contents = '';
        $L_Name = '';
        $SB_Value = 'Save';
        $L_ReadOnly = '';
        break;

      case 'edit':

        $Results = GetEmailRecipientsLists("$ListID");
        if (($Results[0] == 'no error') && (count($Results) == 2))
        {
          // Show the templates (starting at index 1)
          foreach(array_slice($Results, 1) as $val)
          {
            $L_Contents = $val['RecipientList'];
            $L_Name = $val['Name'];

            // Read-only?
            $L_ReadOnly = $val['ro'] ? ' disabled="disabled"' : '';
          }
        }
        $SB_Value = 'Update';
        break;

      case 'delete':
        DeleteEmailRecipientsList ($ListID);
        feedback ("List '$ListID' deleted", false);
        break;
    }
    if (strlen ($SB_Value))
    {
      // Show the template contents
      $array = explode("\n", trim($L_Contents));
      $rows = count($array) + 5;
    print <<< END
    <div class="textwrapper">
      <span class="bold13">&nbsp;&nbsp;List: </span>
      <input type="text" name="L_Name" size="20" maxlength="254" value="$L_Name" placeholder="Input the list's name" required x-moz-errormessage="Input the list's name">
      <p>
      <i>List format/requirements:</i>
      <br>
       &nbsp;One line per record, at least three fields per line<br>
       &nbsp;Field must be separated by a comma (',') and enclosed with double quotes ('"')<br>
       &nbsp;Field order: First name, last name, email address<br>
      <textarea cols='1' rows='$rows' name='L_Contents'>$L_Contents</textarea>
    </div>
    &nbsp;&nbsp;<input type="submit" value=" $SB_Value " class="submit" name="L_Save" $L_ReadOnly>
    &nbsp;&nbsp;<input type="reset" value=" Revert " class="reset">
    <input type="hidden" name="L_ID" value="$ListID">

END;
    }
    break;

  case 'email_templates':
    // # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    // The "Email Templates" contents
    $SB_Value = '';
    switch ($SubAction)
    {
      case 'new':
        // Create a new template
        $T_Contents = 'Dear %%%FIRSTNAME%%%:

As part of our ongoing employee satisfaction campaign, we would like to reward you with a thank you on this %%%LINK%%%.

Sincerely,
%%%SIGNATURE%%%';
        $T_Name = '';
        $SB_Value = 'Save';
        $T_ReadOnly = '';
        break;

      case 'edit':

        $Results = GetEmailTemplates("$TemplateID");
        if (($Results[0] == 'no error') && (count($Results) == 2))
        {
          // Show the templates (starting at index 1)
          foreach(array_slice($Results, 1) as $val)
          {
            $T_Contents = $val['Template'];
            $T_Name = $val['Name'];

            // Read-only?
            $T_ReadOnly = $val['ro'] ? ' disabled="disabled"' : '';
          }
        }
        $SB_Value = 'Update';
        break;

      case 'delete':
        DeleteEmailTemplate ($TemplateID);
        feedback ("Template '$TemplateID' deleted", false);
        break;
    }
    if (strlen ($SB_Value))
    {
      // Show the template contents
      $array = explode("\n", trim($T_Contents));
      $rows = count($array) + 5;
    print <<< END
    <div class="textwrapper">
      <span class="bold13">&nbsp;&nbsp;Template: </span>
      <input type="text" name="T_Name" size="20" maxlength="254" value="$T_Name" placeholder="Input the template's name" required x-moz-errormessage="Input the template's name">
      <p>
      <i>Available placeholders:</i>
      <br>
       &nbsp;%%%SENDER%%% &raquo; sender's email address<br>
       &nbsp;%%%FIRSTNAME%%%% &raquo; first name of email recipient<br>
       &nbsp;%%%LASTNAME%%% &raquo;  last name of email recipient<br>
       &nbsp;%%%LINK%%% &raquo; URL of web site to "lure" the email recipient to<br>
       &nbsp;%%%SIGNATURE%%% &raquo; signature or name of supposed email sender<br>
      <textarea cols='1' rows='$rows' name='T_Contents'>$T_Contents</textarea>
    </div>
    &nbsp;&nbsp;<input type="submit" value=" $SB_Value " class="submit" name="T_Save" $T_ReadOnly>
    &nbsp;&nbsp;<input type="reset" value=" Revert " class="reset">
    <input type="hidden" name="T_ID" value="$TemplateID">

END;
    }
    break;

  case 'web_templates':
    // # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    // The "Web Templates" contents
    $SB_Value = '';
    switch ($SubAction)
    {
      case 'new':
        // Create a new template
        $T_Contents = '<!-- Created on ' . date ("r") . ' -->';
        $T_Name = '';
        $SB_Value = 'Save';
        $T_ReadOnly = '';
        break;

      case 'edit':

        $Results = GetWebTemplates("$TemplateID");
        if (($Results[0] == 'no error') && (count($Results) == 2))
        {
          // Show the templates (starting at index 1)
          foreach(array_slice($Results, 1) as $val)
          {
            $T_Contents = $val['Template'];
            $T_Name = $val['Name'];

            // Read-only?
            $T_ReadOnly = $val['ro'] ? ' disabled="disabled"' : '';
          }
        }
        $SB_Value = 'Update';
        break;

      case 'delete':
        DeleteWebTemplate ($TemplateID);
        feedback ("Template '$TemplateID' deleted", false);
        break;
    }
    if (strlen ($SB_Value))
    {
      // Show the template contents
      $array = explode("\n", trim($T_Contents));
      $rows = count($array) + 5;
    print <<< END
    <div class="textwrapper">
      <span class="bold13">&nbsp;&nbsp;Template: </span>
      <input type="text" name="T_Name" size="20" maxlength="254" value="$T_Name" placeholder="Input the template's name" required x-moz-errormessage="Input the template's name">
      <p>
      <i>Available placeholders:</i>
      <br>
      &nbsp;%%%URL%%% &raquo; Full URL of website to redirect to
      <textarea cols='1' rows='$rows' name='T_Contents'>$T_Contents</textarea>
    </div>
    &nbsp;&nbsp;<input type="submit" value=" $SB_Value " class="submit" name="T_Save" $T_ReadOnly>
    &nbsp;&nbsp;<input type="reset" value=" Revert " class="reset">
    <input type="hidden" name="T_ID" value="$TemplateID">

END;
    }
    break;
}

print <<< END
  </div>

END;
?>
