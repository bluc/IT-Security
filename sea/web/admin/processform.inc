<?
  // We have a create|edit|delete|results|results_csv operation
  switch ($ScriptAction)
  {
    //++++++++++++++++++++++++++++++++++++++++++++
    case 'assessments':
      if (($SubAction == 'new') || ($SubAction == 'edit'))
      {

        // Make sure that we validate the input
        if (isset($_POST['A_Save']))
        {
          if (isset ($_POST['A_Name']))
          {
            if (preg_match('/\w+/', $_POST['A_Name']) === false)
            {
              feedback("ERROR: Invalid name for assessment: {$_POST['A_Name']}");
            }
            $A_Name = $_POST['A_Name'];
            if (isset ($_POST['A_ID']))
            {
              if (filter_var($_POST['A_ID'], FILTER_VALIDATE_REGEXP,
                  array('options' => array('regexp' => '/\d{12,12}-\d{12,12}/'))) === false)
              {
                feedback("ERROR: Invalid ID for assessment: {$_POST['A_ID']}");
              }
              if (preg_match("/-99999999999[0-9]$/", $_POST['A_ID']))
              {
                feedback("ERROR: You can overwrite a builtin assessment");
              }

              if (isset ($_POST['A_Status']))
              {
                if (preg_match("/^(?:N|S|R|F)$/", $_POST['A_Status']) === false)
                {
                  feedback("ERROR: Invalid status '{$_POST['A_Status']}'");
                }
                if (isset ($_POST['A_EmailSender']))
                {
                  if (filter_var($_POST['A_EmailSender'], FILTER_VALIDATE_EMAIL) === false)
                  {
                    feedback("ERROR: Invalid email sender '{$_POST['A_EmailSender']}'");
                  }
                  if (isset ($_POST['A_EmailSubject']))
                  {
                    if (strlen($_POST['A_EmailSubject']) == 0)
                    {
                      feedback("ERROR: Email subject can not be empty");
                    }
                    if (isset ($_POST['A_StartTime']))
                    {
                      if (strlen($_POST['A_StartTime']) == 0)
                      {
                        feedback("ERROR: Start time can not be empty");
                      }
                      if (isset ($_POST['A_EndTime']))
                      {
                        if (strlen($_POST['A_EndTime']) == 0)
                        {
                          feedback("ERROR: End time can not be empty");
                        }
                        if (isset ($_POST['A_ER_id']))
                        {
                          if (filter_var($_POST['A_ER_id'], FILTER_VALIDATE_REGEXP,
                              array('options' => array('regexp' => '/\d{12,12}-\d{12,12}/'))) === false)
                          {
                            feedback("ERROR: Invalid ID for email recipient list: {$_POST['A_ER_id']}");
                          }
                          if (isset ($_POST['A_ET_id']))
                          {
                            if (filter_var($_POST['A_ET_id'], FILTER_VALIDATE_REGEXP,
                                array('options' => array('regexp' => '/\d{12,12}-\d{12,12}/'))) === false)
                            {
                              feedback("ERROR: Invalid ID for email template: {$_POST['A_ET_id']}");
                            }
                            if (isset ($_POST['A_WT_id']))
                            {
                              if (filter_var($_POST['A_WT_id'], FILTER_VALIDATE_REGEXP,
                                  array('options' => array('regexp' => '/\d{12,12}-\d{12,12}/'))) === false)
                              {
                                feedback("ERROR: Invalid ID for web template: {$_POST['A_WT_id']}");
                              }
                              SaveAssessment ($_POST['A_ID'], $A_Name, $_POST['A_Status'],
                                              $_POST['A_EmailSender'], $_POST['A_EmailSubject'], $_POST['A_Signature'],
                                              $_POST['A_WebURL'], $_POST['A_CompanyLogoURL'],
                                              strtotime($_POST['A_StartTime']), strtotime($_POST['A_EndTime']),
                                              $_POST['A_ER_id'], $_POST['A_ET_id'], $_POST['A_WT_id']);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      break;

     case 'email_recipient_lists':
      if (($SubAction == 'new') || ($SubAction == 'edit'))
      {
        // Make sure that we validate the input
        if (isset($_POST['L_Save']))
        {
          if (isset ($_POST['L_Name']))
          {
            if (preg_match('/\w+/', $_POST['L_Name']) === false)
            {
              feedback("ERROR: Invalid name for template: {$_POST['L_Name']}");
            }
            $L_Name = $_POST['L_Name'];
            if (isset ($_POST['L_ID']))
            {
              if (filter_var($_POST['L_ID'], FILTER_VALIDATE_REGEXP,
                  array('options' => array('regexp' => '/\d{12,12}-\d{12,12}/'))) === false)
              {
                feedback("ERROR: Invalid ID for template: {$_POST['L_ID']}");
              }
              if (preg_match("/-99999999999[0-9]$/", $_POST['L_ID']))
              {
                feedback("ERROR: You can overwrite a builtin recipient list");
              }
              if (isset ($_POST['L_Contents']))
              {
                $FileContents = array();
                foreach(explode(PHP_EOL, $_POST['L_Contents']) as $Line)
                {
                  $Line = CheckCSVLine(trim($Line));
                  if (strlen($Line)) $FileContents[] = "$Line";
                }
                if (count($FileContents))
                {
                  // Save what we found
                  $L_Contents = implode (PHP_EOL, $FileContents);
                  SaveEmailRecipientsList ($_POST['L_ID'], $L_Name, $L_Contents);
                }
              }
            }
          }
        }
      } elseif ($SubAction == 'upload')
      {
        if (isset ($_POST['L_Upload']) && count($_FILES['L_NewCSV']))
        {
          if ($_FILES['L_NewCSV']['error'])
          {
            feedback("ERROR: File upload failed with error {$_FILES['L_NewCSV']['error']}");
          }
          if ($_FILES['L_NewCSV']['type'] != 'text/csv')
          {
            feedback("ERROR: Uploaded file '{$_FILES['L_NewCSV']['name']}' is not a 'text/csv' file");
          }
          if (pathinfo ($_FILES['L_NewCSV']['name'], PATHINFO_EXTENSION) != 'csv')
          {
            feedback("ERROR: Uploaded file '{$_FILES['L_NewCSV']['name']}' is not a '.csv' file");
          }
           if (isset ($_POST['L_Name']))
          {
            if (preg_match('/\w+/', $_POST['L_Name']) === false)
            {
              feedback("ERROR: Invalid name for template: {$_POST['L_Name']}");
            }
            $L_Name = $_POST['L_Name'];
            if (isset ($_POST['L_ID']))
            {
              if (filter_var($_POST['L_ID'], FILTER_VALIDATE_REGEXP,
                  array('options' => array('regexp' => '/\d{12,12}-\d{12,12}/'))) === false)
              {
                feedback("ERROR: Invalid ID for template: {$_POST['L_ID']}");
              }
              if (preg_match("/-99999999999[0-9]$/", $_POST['L_ID']))
              {
                feedback("ERROR: You can overwrite a builtin recipient list");
              }

              // Check the uploaded file
              $FileHandle = fopen("{$_FILES['L_NewCSV']['tmp_name']}", "r");
              $FileContents = array();
              if ($FileHandle)
              {
                // Opened file for read
                while (($Line = fgets($FileHandle, 4096)) !== false)
                {
                }
                if (!feof($FileHandle))
                {
                  feedback("ERROR: Unexepected error of uploaded file '$FileName'");
                }
                fclose($FileHandle);
              }
            }
          }
        }
      }
      break;

    //++++++++++++++++++++++++++++++++++++++++++++
     case 'email_templates':
      if (($SubAction == 'new') || ($SubAction == 'edit'))
      {
        // Make sure that we validate the input
        if (isset($_POST['T_Save']))
        {
          if (isset ($_POST['T_Name']))
          {
            if (preg_match('/\w+/', $_POST['T_Name']) === false)
            {
              feedback("ERROR: Invalid name for template: {$_POST['T_Name']}");
            }
            $T_Name = $_POST['T_Name'];
            if (isset ($_POST['T_Contents']))
            {
              $T_Contents = $_POST['T_Contents'];
              if (isset ($_POST['T_ID']))
              {
                if (filter_var($_POST['T_ID'], FILTER_VALIDATE_REGEXP,
                    array('options' => array('regexp' => '/\d{12,12}-\d{12,12}/'))) === false)
                {
                  feedback("ERROR: Invalid ID for template: {$_POST['T_ID']}");
                }
                if (preg_match("/-99999999999[0-9]$/", $_POST['T_ID']))
                {
                  feedback("ERROR: You can overwrite a builtin template");
                }
                SaveEmailTemplate ($_POST['T_ID'], $T_Name, $T_Contents);
              }
            }
          }
        }
      }
      break;

    //++++++++++++++++++++++++++++++++++++++++++++
    case 'web_templates':
      if (($SubAction == 'new') || ($SubAction == 'edit'))
      {
        // Make sure that we validate the input
        if (isset($_POST['T_Save']))
        {
          if (isset ($_POST['T_Name']))
          {
            if (preg_match('/\w+/', $_POST['T_Name']) === false)
            {
              feedback("ERROR: Invalid name for template: {$_POST['T_Name']}");
            }
            $T_Name = $_POST['T_Name'];
            if (isset ($_POST['T_Contents']))
            {
              $T_Contents = $_POST['T_Contents'];
              if (isset ($_POST['T_ID']))
              {
                if (filter_var($_POST['T_ID'], FILTER_VALIDATE_REGEXP,
                    array('options' => array('regexp' => '/\d{12,12}-\d{12,12}/'))) === false)
                {
                  feedback("ERROR: Invalid ID for template: {$_POST['T_ID']}");
                }
                if (preg_match("/-99999999999[0-9]$/", $_POST['T_ID']))
                {
                  feedback("ERROR: You can overwrite a builtin template");
                }
                SaveWebTemplate ($_POST['T_ID'], $T_Name, $T_Contents);
              }
            }
          }
        }
      }
    break;
  }
?>
