<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="Content-Language" content="en-us">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Social Engineering Toolkit</title>
 <meta name="copyright" content="Copyright 2013, Thomas Bullinger">
 <style type="text/css">
 body
 {
  width:640px;margin-left:auto;margin-right:auto;
  background:#F0FFFF;
  background:-moz-radial-gradient(center center,#F7FFF0,#F0FFFF);
  background:-webkit-gradient(radial,50% 50%,50% 100%,from(#F7FFFF),to(#F0FFFF));
 }
 div,title,span,p,.p,h1,h2,h3,h4,h5,h6,ul,li,a,td,th,select,input,textarea,br
 {
  font-family:Verdana,Arial,Helvetica,Swiss,Futura,sans-serif;font-size:11px;color:#090909;
 }
 h1
 {
  font-size:12px;
 }
 h2
 {
  font-size:11px;
 }
 h3
 {
  font-size:10px;
 }
 h4
 {
  font-size:9px;
 }
 h5,h6
 {
  font-size:8px;
 }
 select,textarea{
  -moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;
 }
 input.reset{
  color:#c09;font-size:90%;background:#CCFFCC none;-moz-border-radius:5px;-webkit-border-radius:5px;
 }
 input.submit{
  color:maroon;font-weight:bold;background:#CCFFCC none;-moz-border-radius:5px;-webkit-border-radius:5px;
 }
 </style>
</head>

<body>
<?
function ShowForm()
{
  // Show the input form
  $default_email = <<< EOT
To: %%%EMAILADDR%%%

Dear %%%FIRSTNAME%%%,

Our company holds its 7th annual summer picnic this year.  Please RVSP on
%%%LINK%%% if you plan to attend.

Sincerely,
%%%SIGNATURE%%%
EOT;
  print <<<EOT
<form method="post" action="{$_SERVER['PHP_SELF']}">
  <h1>Schedule an email for a Social Engineering Assessment</h1>
  <h3>Type in your email below:</h3>
  <textarea cols=80 rows=20 name="email">
$default_email
  </textarea>
  <p>
  The following placeholders can be used:
  <ul>
   <li>%%%EMAILADDR%%% - email address of recipient (<b>MANDATORY</b>)
   <li>%%%LINK%%% - URL of web page to send recipient to (<b>MANDATORY</b>)
   <li>%%%FIRSTNAME%%% - the first name of the email recipient
   <li>%%%LASTNAME%%%  - the last name of the email recipient
   <li>%%%SIGNATURE%%% - signature of the sender
  </ul>
  <p>
  <input name="f_submit" type="submit" value="Schedule email">
  <input name="f_reset" type="reset" value="Start over">
</form>
EOT;
}

function ScheduleEmail($email)
{
  // Interpret the email and schedule it for sending
  echo $email;
}

// Get and check parameters (CreateEmail.php?email=<email>)
if (isset ($_POST['email']))
{
  if (empty($_POST['email']))
  {
    ShowForm();
  } else
  {
    ScheduleEmail($_POST['email']);
  }
} else
{
  ShowForm();
}
?>
