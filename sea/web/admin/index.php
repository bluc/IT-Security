<?
/*
################################################################
#
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
*/

/* Required ubuntu packages:
 Anything that implements a web server and PHP5, e.g.:
  nginx (via http://nginx.org/packages/ubuntu/)
  php5-fpm
  php5-suhosin
 The native MySQL driver for PGP:
  php5-mysqlnd
*/

// ################################################################
// Sanitize environment PHP_SELF
function SanitizeSelf()
{
  // As per http://www.phpeveryday.com/articles/2-5-Environment-Variables-P791.html
  $uname = basename(__FILE__);
  $_SERVER["PHP_SELF"] = substr($_SERVER["PHP_SELF"],0,strpos($_SERVER["PHP_SELF"], $uname)) . $uname;
}

// ################################################################
// Show the main header
function show_header()
{
  print <<< END
<body>
  <div id="header">
    <p class="boldcenter18">Social Engineering Toolkit</p>
    <p class="copyright">&copy; (C) 2013 B-LUC Consulting</p>
  </div>

END;
}

// ################################################################
// Show a feeback to the client and exit
function feedback($feedback_text, $ShowHeader = true)
{
  if ($ShowHeader) show_header();
  print <<< END
  <div id="feedback">
    <p>$feedback_text</p>
    <p>
    Click <a href=${_SERVER['PHP_SELF']}>here</a> to return to main window
  </div>
END;
  exit();
}

// ################################################################
// Check whether the URL came from the same site
function CheckReferer()
{
  // Check the referer
  if (empty($_SERVER['HTTP_REFERER']))
  {
    feedback('ERROR: You can not reach this page directly');
  }
  $URL = "{$_SERVER['SERVER_NAME']}" . "{$_SERVER['PHP_SELF']}";
  if (preg_match("@$URL@", $_SERVER['HTTP_REFERER']) === false)
  {
    feedback("ERROR: You can not reach this page from http(s)://$URL");
  }
}

// ################################################################
// Check whether a given line is valid CSV
//  returns cleaned up line (valid) or '' (invalid)
function CheckCSVLine($Line)
{
  // Remove spaces around ","
  $CSVLine = preg_replace ('/"\s*,\s*"/', '","', $Line);
  // Remove spaces around '"'
  $CSVLine = preg_replace ('/\s*"\s*/', '"', $CSVLine);
  // Check line syntax
  if (filter_var($CSVLine, FILTER_VALIDATE_REGEXP,
      array('options' => array('regexp' => '/^"[^"]*","[^"]*","[^"]+"/'))) === false)
  {
    // Show some debug info in log file and skip line
    error_log ("Invalid line in csv: '$CSVLine'");
    return '';
  }
  // The email address must be valid
  $Fields = explode ('","', $CSVLine);
  if (filter_var(preg_replace ('/"$/', "", $Fields[2]), FILTER_VALIDATE_EMAIL) === false)
  {
    // Show some debug info in log file and skip line
    error_log ("Invalid email in csv: '$CSVLine'");
    return '';
  }
  return ($CSVLine);
}

// ################################################################
// Sanitize some variables
SanitizeSelf();

// ################################################################
// The HTML header
// The date/time picker: http://www.javascriptkit.com/script/script2/tengcalendar.shtml
print <<< EOH
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Social Engineering Toolkit</title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="style.css" />
  <script language="javascript" type="text/javascript">
    function AreYouSure() { return confirm('Are you sure?'); }
  </script>
  <script language="javascript" type="text/javascript" src="datetimepicker.js">
  </script>
  <noscript><META HTTP-EQUIV="Refresh" CONTENT="0;URL=noscript.php"></noscript>
</head>

EOH;

require_once rtrim($_SERVER["DOCUMENT_ROOT"],"/")."/admin/db.inc";
require_once rtrim($_SERVER["DOCUMENT_ROOT"],"/")."/admin/dbfuncs.inc";

$ScriptAction = '';
$SubAction    = '';
$TemplateID   = '';
$ListID       = '';
$AssessmentID = '';

// ################################################################
// Handle GET parameters
if (count($_GET))
{
  CheckReferer();

  if (isset ($_GET['action']))
  {
    // We have a main action
    $ScriptAction = $_GET['action'];

    if (isset ($_GET['subaction']))
    {
      // We have a sub-function
      $SubAction = $_GET['subaction'];

      if (isset ($_GET['templateid']))
      {
        // We have a sub-function
        $TemplateID = $_GET['templateid'];
      }
      if (isset ($_GET['listid']))
      {
        // We have a sub-function
        $ListID = $_GET['listid'];
      }
      if (isset ($_GET['assessmentid']))
      {
        // We have a sub-function
        $AssessmentID = $_GET['assessmentid'];
      }
    }
  }

  // ################################################################
  // Handle POST values (only valid if valid GET parameters are present)
  if (count($_POST))
  {
    require_once rtrim($_SERVER["DOCUMENT_ROOT"],"/")."/admin/processform.inc";
  }
}

// ################################################################
// The start of the HTML body
show_header();

// ################################################################
// The menu list
print <<< END

  <div id="menu">
    <p>
    <span class="bold13">Manage:</span>

END;

require_once rtrim($_SERVER["DOCUMENT_ROOT"],"/")."/admin/showmenu.inc";

if ((mb_strlen ($ScriptAction) > 0) && (mb_strlen ($SubAction) > 0))
{
  print <<< END
 <form action="${_SERVER['PHP_SELF']}?action=$ScriptAction&subaction=$SubAction" method="post" enctype="multipart/form-data">

END;
}

// ################################################################
// The contents for each menu
print <<< END
  <div id="content">

END;
require_once rtrim($_SERVER["DOCUMENT_ROOT"],"/")."/admin/showcontents.inc";

if ((mb_strlen ($ScriptAction) > 0) && (mb_strlen ($SubAction) > 0))
{
  print <<< END
 </form>

END;
}

// ################################################################
// The trailer portion of the HTML body
print <<< END
</body>
</html>

END;
