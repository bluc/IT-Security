<?

// The database connection
try {
   $GLOBALS['dbh'] = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_UserID'], $GLOBALS['DB_Passwd'],
                             array(PDO::ATTR_PERSISTENT => true));
} catch (PDOException $e) {
    print "FATAL ERROR: " . $e->getMessage() . "<br/>";
    die();
}

/********/
// Functions that deal with web templates
/********/
function GetWebTemplates($T_ID='%')
{
  // Prepare the SQL statement
  if ($T_ID == '%')
  {
    $sql = 'SELECT id,Name,Template,LastUpdate FROM web_templates';
  } else
  {
    $sql = 'SELECT id,Name,Template,LastUpdate FROM web_templates WHERE id = ?';
  }
  try {
    $Stmt = $GLOBALS['dbh']->prepare($sql);
  }
  catch (PDOException $e) {
    print $e->getMessage();
  }
  if ($T_ID != '%') $Stmt->bindParam(1, $T_ID);

  $Results = array( 0 => 'no error');
  try {
    $Stmt->execute(array("$T_ID"));
  }
  catch (PDOException $e) {
    $Results[0] = $e->getMessage();
    return ($Results);
  }

  // Get the results as an associative array
  while ($r = $Stmt->fetch(PDO::FETCH_ASSOC))
  {
    // Builtins are read-only
    $r['ro'] = preg_match("/-99999999999[0-9]$/", $r['id']);
    $Results[] = $r;
  }

  // Return the results (1st element is return code)
  return ($Results);
}

function SaveWebTemplate($T_ID, $T_Name, &$T_Contents)
{
  // Prepare the SQL statement
  $sql = 'REPLACE INTO web_templates (id, Name, Template) VALUES (?, ?, ?)';
  $Stmt = $GLOBALS['dbh']->prepare($sql);
  $Stmt->bindParam(1, $T_ID);
  $Stmt->bindParam(2, $T_Name);
  $Stmt->bindParam(3, $T_Contents);
  // Execute the statement
  try {
    $Stmt->execute();
  }
  catch (PDOException $e) {
    feedback ($e->getMessage());
  }
}

function DeleteWebTemplate($T_ID)
{
  if (preg_match("/-99999999999[0-9]$/", $T_ID))
  {
    feedback ("Builtin templates can not be deleted", false);
  }
  // Prepare the SQL statement
  $sql = 'DELETE FROM web_templates WHERE id = ?';
  $Stmt = $GLOBALS['dbh']->prepare($sql);
  $Stmt->bindParam(1, $T_ID);
  // Execute the statement
  try {
    $Stmt->execute();
  }
  catch (PDOException $e) {
    feedback ($e->getMessage());
  }
}

/********/
// Functions that deal with email templates
/********/
function GetEmailTemplates($T_ID='%')
{
  // Prepare the SQL statement
  if ($T_ID == '%')
  {
    $sql = 'SELECT id,Name,Template,LastUpdate FROM email_templates';
  } else
  {
    $sql = 'SELECT id,Name,Template,LastUpdate FROM email_templates WHERE id = ?';
  }
  try {
    $Stmt = $GLOBALS['dbh']->prepare($sql);
  }
  catch (PDOException $e) {
    print $e->getMessage();
  }
  if ($T_ID != '%') $Stmt->bindParam(1, $T_ID);

  $Results = array( 0 => 'no error');
  try {
    $Stmt->execute(array("$T_ID"));
  }
  catch (PDOException $e) {
    $Results[0] = $e->getMessage();
    return ($Results);
  }

  // Get the results as an associative array
  while ($r = $Stmt->fetch(PDO::FETCH_ASSOC))
  {
    // Builtins are read-only
    $r['ro'] = preg_match("/-99999999999[0-9]$/", $r['id']);
    $Results[] = $r;
  }

  // Return the results (1st element is return code)
  return ($Results);
}

function SaveEmailTemplate($T_ID, $T_Name, &$T_Contents)
{
  // Prepare the SQL statement
  $sql = 'REPLACE INTO email_templates (id, Name, Template) VALUES (?, ?, ?)';
  $Stmt = $GLOBALS['dbh']->prepare($sql);
  $Stmt->bindParam(1, $T_ID);
  $Stmt->bindParam(2, $T_Name);
  $Stmt->bindParam(3, $T_Contents);
  // Execute the statement
  try {
    $Stmt->execute();
  }
  catch (PDOException $e) {
    feedback ($e->getMessage());
  }
}

function DeleteEmailTemplate($T_ID)
{
  if (preg_match("/-99999999999[0-9]$/", $T_ID))
  {
    feedback ("Builtin templates can not be deleted", false);
  }
  // Prepare the SQL statement
  $sql = 'DELETE FROM email_templates WHERE id = ?';
  $Stmt = $GLOBALS['dbh']->prepare($sql);
  $Stmt->bindParam(1, $T_ID);
  // Execute the statement
  try {
    $Stmt->execute();
  }
  catch (PDOException $e) {
    feedback ($e->getMessage());
  }
}

/********/
// Functions that deal with email recipient lists
/********/
function GetEmailRecipientsLists($L_ID='%')
{
  // Prepare the SQL statement
  if ($L_ID == '%')
  {
    $sql = 'SELECT id,Name,RecipientList,LastUpdate FROM email_recipients';
  } else
  {
    $sql = 'SELECT id,Name,RecipientList,LastUpdate FROM email_recipients WHERE id = ?';
  }
  try {
    $Stmt = $GLOBALS['dbh']->prepare($sql);
  }
  catch (PDOException $e) {
    print $e->getMessage();
  }
  if ($L_ID != '%') $Stmt->bindParam(1, $L_ID);

  $Results = array( 0 => 'no error');
  try {
    $Stmt->execute(array("$L_ID"));
  }
  catch (PDOException $e) {
    $Results[0] = $e->getMessage();
    return ($Results);
  }

  // Get the results as an associative array
  while ($r = $Stmt->fetch(PDO::FETCH_ASSOC))
  {
    // Builtins are read-only
    $r['ro'] = preg_match("/-99999999999[0-9]$/", $r['id']);
    $Results[] = $r;
  }

  // Return the results (1st element is return code)
  return ($Results);
}

function SaveEmailRecipientsList($L_ID, $L_Name, $L_List)
{
  // Prepare the SQL statement
  $sql = 'REPLACE INTO email_recipients (id, Name, RecipientList) VALUES (?, ?, ?)';
  $Stmt = $GLOBALS['dbh']->prepare($sql);
  $Stmt->bindParam(1, $L_ID);
  $Stmt->bindParam(2, $L_Name);
  $Stmt->bindParam(3, $L_List);
  // Execute the statement
  try {
    $Stmt->execute();
  }
  catch (PDOException $e) {
    feedback ($e->getMessage());
  }
}

function DeleteEmailRecipientsList($L_ID)
{
  if (preg_match("/-99999999999[0-9]$/", $L_ID))
  {
    feedback ("Builtin recipient lists can not be deleted", false);
  }
  // Prepare the SQL statement
  $sql = 'DELETE FROM email_recipients WHERE id = ?';
  $Stmt = $GLOBALS['dbh']->prepare($sql);
  $Stmt->bindParam(1, $L_ID);
  // Execute the statement
  try {
    $Stmt->execute();
  }
  catch (PDOException $e) {
    feedback ($e->getMessage());
  }
}

/********/
// Functions that deal with assessments
/********/
function GetAssessments($A_ID='%')
{
  // Prepare the SQL statement
  if ($A_ID == '%')
  {
    $sql = 'SELECT * FROM assessments';
  } else
  {
    $sql = 'SELECT * FROM assessments WHERE id = ?';
  }
  try {
    $Stmt = $GLOBALS['dbh']->prepare($sql);
  }
  catch (PDOException $e) {
    print $e->getMessage();
  }
  if ($A_ID != '%') $Stmt->bindParam(1, $A_ID);

  $Results = array( 0 => 'no error');
  try {
    $Stmt->execute(array("$A_ID"));
  }
  catch (PDOException $e) {
    $Results[0] = $e->getMessage();
    return ($Results);
  }

  // Get the results as an associative array
  while ($r = $Stmt->fetch(PDO::FETCH_ASSOC))
  {
    // Builtins are read-only
    $r['ro'] = preg_match("/-99999999999[0-9]$/", $r['id']);
    $Results[] = $r;
  }

  // Return the results (1st element is return code)
  return ($Results);
}

function SaveAssessment($A_ID, $A_Name, $A_Status, $A_EmailSender, $A_EmailSubject, $A_Signature,
                        $A_WebURL, $A_CompanyLogoURL, $A_StartTime, $A_EndTime, $A_ER_id, $A_ET_id, $A_WT_id)
{
  // Prepare the SQL statement
  $sql = 'REPLACE INTO assessments (id, Name, EmailSender, EmailSubject, Signature,
                                   WebURL, CompanyLogoURL, StartTime, EndTime, Status, ER_id, ET_id, WT_id)
                                   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
  $Stmt = $GLOBALS['dbh']->prepare($sql);
  $Stmt->bindParam(1, $A_ID);
  $Stmt->bindParam(2, $A_Name);
  $Stmt->bindParam(3, $A_EmailSender);
  $Stmt->bindParam(4, $A_EmailSubject);
  $Stmt->bindParam(5, $A_Signature);
  $Stmt->bindParam(6, $A_WebURL);
  $Stmt->bindParam(7, $A_CompanyLogoURL);
  $Stmt->bindParam(8, $A_StartTime);
  $Stmt->bindParam(9, $A_EndTime);
  $Stmt->bindParam(10, $A_Status);
  $Stmt->bindParam(11, $A_ER_id);
  $Stmt->bindParam(12, $A_ET_id);
  $Stmt->bindParam(13, $A_WT_id);
  // Execute the statement
  try {
    $Stmt->execute();
  }
  catch (PDOException $e) {
    feedback ($e->getMessage());
  }
}

function DeleteAssessment($A_ID)
{
  if (preg_match("/-99999999999[0-9]$/", $A_ID))
  {
    feedback ("Builtin assessments can not be deleted", false);
  }
  // Prepare the SQL statement
  $sql = 'DELETE FROM assessments WHERE id = ?';
  $Stmt = $GLOBALS['dbh']->prepare($sql);
  $Stmt->bindParam(1, $A_ID);
  // Execute the statement
  try {
    $Stmt->execute();
  }
  catch (PDOException $e) {
    feedback ($e->getMessage());
  }
}

/********/
// Functions that deal with assessment results
/********/
function GetAssessmentResults($AR_ID='%')
{
  // Prepare the SQL statement
  if ($AR_ID == '%')
  {
    $sql = 'SELECT * FROM as_results';
  } else
  {
    $sql = 'SELECT * FROM as_results WHERE AS_id = ?';
  }
  $sql .= ' ORDER BY R_Date DESC';
  try {
    $Stmt = $GLOBALS['dbh']->prepare($sql);
  }
  catch (PDOException $e) {
    print $e->getMessage();
  }
  if ($AR_ID != '%') $Stmt->bindParam(1, $AR_ID);

  $Results = array( 0 => 'no error');
  try {
    $Stmt->execute(array("$AR_ID"));
  }
  catch (PDOException $e) {
    $Results[0] = $e->getMessage();
    return ($Results);
  }

  // Get the results as an associative array
  while ($r = $Stmt->fetch(PDO::FETCH_ASSOC))
  {
    // Builtins are read-only
    $Results[] = $r;
  }

  // Return the results (1st element is return code)
  return ($Results);
}

function SaveAssessmentResults($R_ID, $R_Date, $R_Email, $R_BrowserID, $R_ClientIP, $R_RealIP, $R_Other)
{
  // Prepare the SQL statement
  $sql = 'INSERT INTO as_results (AS_id, R_Date, R_Email, R_BrowserID, R_ClientIP, R_RealIP, R_Other)
                                   VALUES (?, ?, ?, ?, ?, ?, ?)';
  $Stmt = $GLOBALS['dbh']->prepare($sql);
  $Stmt->bindParam(1, $R_ID);
  $Stmt->bindParam(2, $R_Date);
  $Stmt->bindParam(3, $R_Email);
  $Stmt->bindParam(4, $R_BrowserID);
  $Stmt->bindParam(5, $R_ClientIP);
  $Stmt->bindParam(6, $R_RealIP);
  $Stmt->bindParam(7, $R_Other);
  // Execute the statement
  try {
    $Stmt->execute();
  }
  catch (PDOException $e) {
    feedback ($e->getMessage());
  }
}


?>
