<?
print <<< END
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Social Engineering Toolkit</title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
  <div id="header">
    <p class="boldcenter18">You MUST have JavaScript enabled in the browser</p>
    <p class="copyright">&copy; (C) 2013 B-LUC Consulting</p>
  </div>
  <div id="feedback">
    For full functionality of this site it is necessary to enable JavaScript.<br>
    Here are the <a href="http://www.enable-javascript.com/" target="_blank">
    instructions how to enable JavaScript in your web browser</a>.
    <p>
    Once enabled, click <a href="index.php">here</a> to return to main window.
  </div>
</body>
END;
?>
