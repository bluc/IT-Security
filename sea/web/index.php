<?
// Invocation: index.php?assessment_id=<id>&email=<email>

// Make sure we capture the current time as early as possible
$Now = date('Y-m-d H:i:s');

if (!isset ($_SERVER['QUERY_STRING']))
{
  print "Thank you";
  error_log ("Missing query string");
  exit;
}
if (empty ($_SERVER['QUERY_STRING']))
{
  print "Thank you";
  error_log ("Empty query string");
  exit;
}
// Decode the query string - it should look similar to this:
// email=consult@btoy1.net&assessment_id=001389724293-000007480011
list($Email_GET, $Assessment_ID_GET) = explode("&", base64_decode($_SERVER['QUERY_STRING']));
if (substr($Email_GET,0,6) != 'email=')
{
  print "Thank you";
  error_log ("Invalid email parameter '$Email_GET'");
  exit;
}
if (substr($Assessment_ID_GET,0,14) != 'assessment_id=')
{
  print "Thank you";
  error_log ("Invalid assessment ID parameter '$Assessment_ID_GET'");
  exit;
}
error_log ("query string = $Email_GET, $Assessment_ID_GET");

// Get the "assessment ID" parameter
list ($dummy, $Assessment_ID) = explode("=", $Assessment_ID_GET);
if (filter_var($Assessment_ID, FILTER_VALIDATE_REGEXP,
    array('options' => array('regexp' => '/\d{12,12}-\d{12,12}/'))) === false)
{
  print "Thank you";
  error_log ("Invalid Assessment ID '$Assessment_ID'");
  exit;
}

// Get the "email" parameter
list ($dummy, $Email) = explode("=", $Email_GET);
if (filter_var($Email, FILTER_VALIDATE_EMAIL) === false)
{
  print "Thank you";
  error_log ("Invalid email '$Email'");
  exit;
}

// Get the assessment parameters from the database
require_once rtrim($_SERVER["DOCUMENT_ROOT"],"/")."/admin/db.inc";
require_once rtrim($_SERVER["DOCUMENT_ROOT"],"/")."/admin/dbfuncs.inc";
$Res_A = GetAssessments($Assessment_ID);
if (($Res_A[0] == 'no error') && (count($Res_A) == 2))
{
  // Setup a default web content
  $BodyContent = '<h1>Thank you</h1>';
  foreach(array_slice($Res_A, 1) as $val_A)
  {
    // Get the URLs
    $WebURL = $val_A['WebURL'];
    $LogoURL = $val_A['CompanyLogoURL'];

    // Get the web template
    $Res_W = GetWebTemplates($val_A['WT_id']);
    if (($Res_W[0] == 'no error') && (count($Res_W) == 2))
    {
      foreach(array_slice($Res_W, 1) as $val_W)
      {
        $BodyContent = preg_replace ('@%%%URL%%%@', $WebURL, $val_W['Template']);
      }
    } else
    {
      print "Thank you";
      error_log ("Unknown web template ID '{$val_A['WT_id']}' for assessment '$Assessment_ID'");
      exit;
    }
  }
} else
{
  print "Thank you";
  error_log ("Unknown assessment ID '$Assessment_ID'");
  exit;
}

// HTML HEADER
header("Refresh: 15; URL=$WebURL");
print <<< END
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta http-equiv="Content-Language" content="en-us">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title>Thank you</title>
 <meta name="copyright" content="Copyright 2013, Thomas Bullinger">
 <link rel="stylesheet" type="text/css" href="style.css" />
</head>
END;

// HTML BODY
print <<< END
<body>
  <div id="header">
    <img src="$LogoURL">
  </div>
  <div id="feedback">
    $BodyContent
  </div>
</body>
</html>
END;

// Notate the infos in the database

$R_RealIP = (isset ($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $R_RealIP = $_SERVER['HTTP_X_FORWARDED_FOR'] : '';
SaveAssessmentResults($Assessment_ID, $Now, $Email, $_SERVER['HTTP_USER_AGENT'],
  $_SERVER['REMOTE_ADDR'], $R_RealIP, '');
?>
