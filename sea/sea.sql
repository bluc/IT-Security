-- MySQL dump 10.13  Distrib 5.5.34, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sea
-- ------------------------------------------------------
-- Server version	5.5.34-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Create the database
USE mysql;
CREATE DATABASE IF NOT EXISTS sea;

-- Create the database user
GRANT INSERT,SELECT,UPDATE,DELETE,ALTER,CREATE TEMPORARY TABLES ON sea.* TO %%%DBUSER%%%;
SET PASSWORD FOR %%%DBUSER%%%=PASSWORD('%%%DBPASS%%%');

-- Create the tables
USE sea;

--
-- Table structure for table `users`
-- As per http://www.phpkode.com/source/s/login-script-with-pdo/login-script-with-pdo/readme.txt
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS users (
  P_Id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  password varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  regdate varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  CpassReqDate varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  voucher varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (P_Id),
  UNIQUE KEY username (username)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

-- Table structure for table as_results
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS as_results (
  -- "assesssments" id
  AS_id char(25) collate utf8_unicode_ci NOT NULL DEFAULT '',
  R_Date timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  R_Email varchar(255) collate utf8_unicode_ci NOT NULL DEFAULT '',
  R_BrowserID varchar(255) collate utf8_unicode_ci NOT NULL DEFAULT '',
  R_ClientIP char(30) collate utf8_unicode_ci NOT NULL DEFAULT '',
  R_RealIP char(30) collate utf8_unicode_ci DEFAULT '',
  R_Other text,
  KEY AS_id (AS_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table assessments
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS assessments (
   -- perl -e '$t=time();$r=int(rand(9999990));printf "%012d-%012d\n", $t, $r'
  id char(25) collate utf8_unicode_ci NOT NULL DEFAULT '',
  Name varchar(255) collate utf8_unicode_ci NOT NULL DEFAULT '',
  EmailSender varchar(255) collate utf8_unicode_ci NOT NULL DEFAULT '',
  EmailSubject varchar(255) collate utf8_unicode_ci NOT NULL DEFAULT '',
  StartTime integer unsigned NOT NULL DEFAULT 0,
  EndTime integer unsigned NOT NULL DEFAULT 0,
  -- 'N' - new (not yet scheduled)
  -- 'S' - scheduled
  -- 'R' - running
  -- 'F' - finished
  Status char(1) NOT NULL DEFAULT 'N',
  -- "email_recipients" id
  ER_id char(25) collate utf8_unicode_ci DEFAULT NULL,
  -- "email_templates" id
  ET_id char(25) collate utf8_unicode_ci DEFAULT NULL,
  -- "web_templates" id
  WT_id char(25) collate utf8_unicode_ci DEFAULT NULL,
  WebURL varchar(255) collate utf8_unicode_ci NOT NULL DEFAULT '',
  CompanyLogoURL varchar(255) collate utf8_unicode_ci DEFAULT '',
  Signature text,
  PRIMARY KEY (id),
  UNIQUE KEY Name (Name),
  KEY EmailSender (EmailSender),
  KEY Status (Status),
  KEY ER_id (ER_id),
  KEY ET_id (ET_id),
  KEY WT_id (WT_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
LOCK TABLES assessments WRITE;
/*!40000 ALTER TABLE assessments DISABLE KEYS */;
REPLACE INTO assessments VALUES ('999999999999-999999999999','Builtin','consult@btoy1.net','Social Engineering Assessment - Builtin','0000-00-00 00:00:00','0000-00-00 00:00:00','F','999999999999-999999999999','999999999999-999999999999','999999999999-999999999999','http://consult.btoy1.net','http://consult.btoy1.net/images/logo.png','B-LUC Consulting');
/*!40000 ALTER TABLE assessments ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table email_recipients
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS email_recipients (
  id char(25) collate utf8_unicode_ci NOT NULL DEFAULT '',
  Name varchar(255) collate utf8_unicode_ci NOT NULL DEFAULT '',
  RecipientList text,
  LastUpdate timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY Name (Name)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
LOCK TABLES email_recipients WRITE;
/*!40000 ALTER TABLE email_recipients DISABLE KEYS */;
REPLACE INTO email_recipients VALUES ('999999999999-999999999999','Builtin','\"B-LUC\",\"Consulting\",\"consult@btoy1.net\"','2013-11-30 14:01:54');
/*!40000 ALTER TABLE email_recipients ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table email_templates
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS email_templates (
  id char(25) collate utf8_unicode_ci NOT NULL DEFAULT '',
  Name varchar(255) collate utf8_unicode_ci NOT NULL DEFAULT '',
  Template text,
  LastUpdate timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY Name (Name)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
LOCK TABLES email_templates WRITE;
/*!40000 ALTER TABLE email_templates DISABLE KEYS */;
REPLACE INTO email_templates VALUES ('999999999999-999999999999','Builtin','Dear %%%FIRSTNAME%%%:\r\n\r\nAs part of our ongoing employee satisfaction campaign, we would like to reward you with a thank you on this <a href="%%%LINK%%%">web site</a>.\r\n\r\nSincerely,\r\n%%%SIGNATURE%%%','2013-11-30 16:32:16');
/*!40000 ALTER TABLE email_templates ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table web_templates
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS web_templates (
  id char(25) collate utf8_unicode_ci NOT NULL DEFAULT '',
  Name varchar(255) collate utf8_unicode_ci NOT NULL DEFAULT '',
  Template text,
  LastUpdate timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY Name (Name)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
LOCK TABLES web_templates WRITE;
/*!40000 ALTER TABLE web_templates DISABLE KEYS */;
REPLACE INTO web_templates VALUES ('999999999999-999999999999','Builtin','<h1>Thank you.</h1>\r\n\r\nPlease click <a href=\"%%%URL%%%\">here</a> if you don\'t get redirected within 15 seconds.\r\n','2013-11-30 14:06:58');
/*!40000 ALTER TABLE web_templates ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
