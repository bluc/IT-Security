#!/bin/bash

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin


if [ $# -eq 0 ]
then
    echo "Usage: $0 URL"
    exit 1
fi

# Remove any temp files when we end
rm -f "rm -f /tmp/$$*" EXIT

# Get the URL (as argument)
URL="$1"

wget -qO /tmp/$$.tinyurl http://tinyurl.com/create.php?url="$URL"
ShortURL=$(grep 'success' /tmp/$$.tinyurl)
ShortURL=${ShortURL#*b>}
ShortURL=${ShortURL%%</*}
echo $ShortURL
