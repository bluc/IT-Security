#!/usr/bin/perl -w
#--------------------------------------------------------------------
# Distributed under GPLv2 (http://www.gnu.org/copyleft/gpl.html)
# Required debian packages:
# libstring-random-perl libdbd-mysql-perl
#
# (c) CopyRight 2015 B-LUC Consulting and Thomas Bullinger
#--------------------------------------------------------------------
require 5.008;

# Constants
my $ProgName = '';
( $ProgName = $0 ) =~ s%.*/%%;
my $CopyRight = "(c) CopyRight 2015 B-LUC Consulting Thomas Bullinger";

#--------------------------------------------------------------------
# Needed packages
use Fcntl ':flock';
use Sys::Syslog qw(:macros :standard);
use Time::Piece;
use Net::SMTP;
use MIME::Base64;
use String::Random;
use DBI;
use Getopt::Std;
use strict;

# Program options
our $opt_d = 0;
our $opt_h = 0;
our $opt_f = 0;
our $opt_B = 'http://sev.btoy1.net';
our $opt_D = 'localhost';
our $opt_r = '';
our $opt_A = 0;

# Global variables;
my $RandomString = new String::Random;
my $DummyDomain  = $RandomString->randregex('\w{8}') . '.'
    . $RandomString->randregex('[A-Z]{2}');

#--------------------------------------------------------------------
# Send an email using the local postfix server
sub SendEmail($$$$)
{
    my ( $EmailSender, $EmailRecipient, $EmailHeadersRef, $EmailBodyRef )
        = @_;

    syslog 5, "Sending email from '$EmailSender' to '$EmailRecipient'";
    warn "EmailSender = '$EmailSender', EmailRecipient = '$EmailRecipient'"
        if ($opt_d);

    my $smtp = Net::SMTP->new(
        Host  => 'localhost',
        Hello => 'mail.' . "$DummyDomain",
        Debug => $opt_d
    ) or return 0;

    # The SMTP envelope
    $smtp->mail("$EmailSender")  or return 0;
    $smtp->to("$EmailRecipient") or return 0;

    $smtp->data() or return 0;

    # The email headers (with a random message id)
    my $MsgID = sprintf( "<%d-%d\@%s>",
        rand( time() ),
        rand(999999999), "$DummyDomain" );
    $smtp->datasend(
        "From: $EmailSender\nTo: $EmailRecipient\nMessage-ID: $MsgID\n")
        or return 0;
    if ( defined $EmailHeadersRef )
    {
        $smtp->datasend( join( "\n", @{$EmailHeadersRef} ) ) or return 0;
    } ## end if ( defined $EmailHeadersRef...)
    $smtp->datasend("\n") or return 0;

    if ( defined $EmailBodyRef )
    {
        # The email body
        $smtp->datasend( join( "\n", @{$EmailBodyRef} ) ) or return 0;
    } ## end if ( defined $EmailBodyRef...)
    $smtp->dataend() or return 0;
    $smtp->quit or return 0;

    # The email is sent successfully
    syslog 5, "Sent email from '$EmailSender' to '$EmailRecipient'";
    return 1;
} ## end sub SendEmail($$$$)

#--------------------------------------------------------------------
# Create a random IP address, but make sure that it is NOT blacklisted
sub CreateRandomIP()
{
    my $RandomIP
        = $RandomString->randregex('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}');
    while (1)
    {
        warn "Checking RandomIP '$RandomIP'" if ($opt_d);
        if ( $RandomIP
            =~ m/^([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])$/
            )
        {
            my $RetCode = system(
                "/usr/local/bin/blcheck.sh $RandomIP >/dev/null 2>&1");
            warn "RetCode = $RetCode" if ($opt_d);
            last unless ($RetCode);
        } ## end if ( $RandomIP =~ ...)
        $RandomIP
            = $RandomString->randregex('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}');
    } ## end while (1)
    return ($RandomIP);
} ## end sub CreateRandomIP

#--------------------------------------------------------------------
# Check the status of assessments, send email(s) if
#  necessary and set them to 'finished' if they are
sub CheckAssessments()
{
    # The database connection to the local server
    my $DBHandle = DBI->connect(
        "DBI:mysql:sea:$opt_D",
        'jeepOpa',
        's3shuv29n',
        {   RaiseError => 0,
            PrintError => $opt_d,
            AutoCommit => 1,
        }
    ) or die "ERROR: Can not connect to local database ($DBI::errstr)\n";
    DBI->trace($opt_d);

    # NOW!
    my $NowTime = localtime();

    # Search for all assessments which are scheduled
    my $StmtHandle_AS
        = $DBHandle->prepare(
        'SELECT id,EmailSender,EmailSubject,StartTime,ER_id,ET_id,Signature FROM assessments WHERE Status = ?'
        );
    $StmtHandle_AS->execute('S') or return;

    my $StmtHandle_SetRunning = $DBHandle->prepare(
        "UPDATE assessments SET Status = 'R' WHERE id=?");
    my $buffer   = '';
    my $RowCache = [];
    while (
        my $rowref = (
            shift(@$RowCache)
                ||    # get row from cache, or reload cache :
                shift(
                @{  $RowCache
                        = $StmtHandle_AS->fetchall_arrayref( undef, 1000 )
                        || []
                }
                )
        )
        )
    {
        warn "Scheduled assessment: " . join( " ", @$rowref ) if ($opt_d);

        # Assessment attributes
        my $AssessmentID       = $$rowref[0];
        my $EmailSender        = $$rowref[1];
        my $EmailSubject       = $$rowref[2];
        my $StartTime          = $$rowref[3];
        my $EmailRecipients_ID = $$rowref[4];
        my $EmailTemplate_ID   = $$rowref[5];
        my $Signature          = $$rowref[6];
        warn "Scheduled ID: '$AssessmentID', EmailSender: '$EmailSender', "
            . "EmailSubject: '$EmailSubject',
          StartTime: '"
            . localtime($StartTime)
            . "', Recipients_ID: '$EmailRecipients_ID', EmailTemplate_ID: '$EmailTemplate_ID', "
            . "Signature: '$Signature'"
            if ($opt_d);
        syslog 5,
            "Scheduled assessment '$AssessmentID' from '$EmailSender' with subject '$EmailSubject'";

        # Only handle assessments with start times before (or at) NOW
        if ( $StartTime > $NowTime )
        {
            syslog 5,
                "Scheduled assessment '$AssessmentID' not ready to run yet";
            next;
        } ## end if ( $StartTime > $NowTime...)

        # Get the email from template
        my $EmailTemplate = '';
        my $StmtHandle_ET
            = $DBHandle->prepare(
            "SELECT Template FROM email_templates WHERE id = '$EmailTemplate_ID'"
            );
        $StmtHandle_ET->execute() or next;
        my $ET_buffer   = '';
        my $ET_RowCache = [];
        while (
            my $ET_rowref = (
                shift(@$ET_RowCache)
                    ||    # get row from cache, or reload cache :
                    shift(
                    @{  $ET_RowCache
                            = $StmtHandle_ET->fetchall_arrayref( undef, 1000 )
                            || []
                    }
                    )
            )
            )
        {
            # Email template
            $EmailTemplate = $$ET_rowref[0];
            syslog 5, "Email template '$EmailTemplate'";
            warn "EmailTemplate: '$EmailTemplate'" if ($opt_d);
        } ## end while ( my $ET_rowref = (...))
            # We have an error if we don't have an email template
        next unless ( length($EmailTemplate) );

        # Get the list of recipients
        my %EmailRecipients = ();
        my $StmtHandle_ER
            = $DBHandle->prepare(
            "SELECT RecipientList FROM email_recipients WHERE id = '$EmailRecipients_ID'"
            );
        $StmtHandle_ER->execute() or next;
        my $ER_buffer   = '';
        my $ER_RowCache = [];
        while (
            my $ER_rowref = (
                shift(@$ER_RowCache)
                    ||    # get row from cache, or reload cache :
                    shift(
                    @{  $ER_RowCache
                            = $StmtHandle_ER->fetchall_arrayref( undef, 1000 )
                            || []
                    }
                    )
            )
            )
        {
            foreach my $ER ( split( /\n/, $$ER_rowref[0] ) )
            {
                # Email attributes
                my ( $FirstName, $LastName, $EmailRecipient )
                    = split( /,/, $ER );
                $EmailRecipient =~ s/"//g;
                $EmailRecipients{"$EmailRecipient"} = "$FirstName,$LastName";
                syslog 5, "Email recipient '$ER'";
                warn "EmailRecipient: '$ER'" if ($opt_d);
            } ## end foreach my $ER ( split( /\n/...))
        } ## end while ( my $ER_rowref = (...))
            # We have an error if we don't have an email recipients
        next unless ( scalar keys %EmailRecipients );

        # Send the email to each recipient
        foreach my $EmailRecipient ( keys %EmailRecipients )
        {
            # Get the first and last name for each recipient
            my ( $FirstName, $LastName )
                = split( /,/, $EmailRecipients{"$EmailRecipient"} );
            $FirstName =~ s/"//g;
            $LastName =~ s/"//g;

            # The link to the web site in email:
            #  => encode the URL parameters
            my $URLLink = "$opt_B/index.php?"
                . encode_base64(
                "email=$EmailRecipient&assessment_id=$AssessmentID", '' );
            chomp $URLLink;

            #  => shorten the URL
            my $ShortLink = "$URLLink";
            if ( open( SU, '-|', "/usr/local/bin/ShortenURL.sh '$URLLink'" ) )
            {
                chomp( $ShortLink = <SU> );
                close(SU);
            } ## end if ( open( SU, '-|', ...))

            # Build the full email
            my $FullEmail = $EmailTemplate;

            # Replace LF by HTML '<br>' tags
            $FullEmail =~ s/\n/<br>\n/g;

            # Replace placeholders
            $FullEmail =~ s/%%%SENDER%%%/$EmailSender/g;
            $FullEmail =~ s/%%%FIRSTNAME%%%/$FirstName/g;
            $FullEmail =~ s/%%%LASTNAME%%%/$LastName/g;
            $FullEmail =~ s/%%%SIGNATURE%%%/$Signature/g;
            $FullEmail =~ s/%%%LINK%%%/$ShortLink/g;
            warn "FullEmail: '$FullEmail'" if ($opt_d);

            # Convert the email body into valid HTML
            my $MimeBoundary = sprintf(
                "==Part_%12s_%-12d",
                $RandomString->randregex('\w{12}'),
                $RandomString->randregex('\d{12}')
            );
            $MimeBoundary =~ s/\s+$//;

            my @BodyArray = ();
            push( @BodyArray,
                "This is a multi-part message in MIME format. To properly display this message you need a MIME-Version 1.0 compliant Email program.\n"
            );
            push( @BodyArray,
                "--$MimeBoundary\nContent-Type: text/html;charset=iso-8859-1\n\n"
            );
            push( @BodyArray, "<HTML>\n<BODY>\n" );
            push( @BodyArray, split( /\n/, $FullEmail ) );
            push( @BodyArray, "</BODY></HTML>\n" );
            push( @BodyArray, "--$MimeBoundary--\n" );

            # Create additional headers
            my @AddlHeaders = ("Subject: $EmailSubject");
            my $t           = localtime;

            # Create random IP addresses, but make sure that
            #  they are NOT blacklisted
            my $IPFrom = CreateRandomIP();
            my $IPBy   = CreateRandomIP();
            push( @AddlHeaders,
                      'Received: from '
                    . $RandomString->randregex('\w{8}')
                    . " ($IPFrom) by mail.$DummyDomain ($IPBy) with\n\tMicrosoft SMTP Server id "
                    . $RandomString->randregex('\d{1,2}\.\d{1,3}\.\d') . '; '
                    . $t->strftime() );
            if ($opt_r)
            {
                # Set "return receipt" email
                push( @AddlHeaders, "Disposition-Notification-To: $opt_r" );
                push( @AddlHeaders, "Return-Receipt-To: $opt_r" );
            } ## end if ($opt_r)
            push( @AddlHeaders, "X-SEA-ID: $AssessmentID" );
            # Create the correct MIME structure
            push( @AddlHeaders, 'MIME-Version: 1.0' );
            push( @AddlHeaders,
                "Content-Type: multipart/related; boundary=\"$MimeBoundary\""
            );

            # Send the email
            if (SendEmail(
                    "$EmailSender", "$EmailRecipient",
                    \@AddlHeaders,  \@BodyArray
                )
                )
            {
                # Update the status to "running"
                $StmtHandle_SetRunning->execute("$AssessmentID");
                warn
                    "WARNING: Assessment '$AssessmentID' could not be set to 'running' ("
                    . $StmtHandle_SetRunning->errstr . ")\n"
                    if ( $StmtHandle_SetRunning->rows <= 0 );
            } else
            {
                warn
                    "WARNING: Assessment '$AssessmentID' could not be emailed to '$EmailRecipient'\n"
                    if ( $StmtHandle_SetRunning->rows <= 0 );
            } ## end else [ if ( SendEmail( "$EmailSender"...))]
        } ## end foreach my $EmailRecipient ...
    } ## end while ( my $rowref = ( shift...))

    # Search for all running assessments
    $StmtHandle_AS = $DBHandle->prepare(
        'SELECT id,EndTime FROM assessments WHERE Status = ?');
    $StmtHandle_AS->execute('R') or return;

    my $StmtHandle_SetFinished = $DBHandle->prepare(
        "UPDATE assessments SET Status = 'F' WHERE id=?");
    $buffer   = '';
    $RowCache = [];
    while (
        my $rowref = (
            shift(@$RowCache)
                ||    # get row from cache, or reload cache :
                shift(
                @{  $RowCache
                        = $StmtHandle_AS->fetchall_arrayref( undef, 1000 )
                        || []
                }
                )
        )
        )
    {
        # Assessment attributes
        warn "Running ID: '$$rowref[0]', EndTime: '"
            . localtime( $$rowref[1] ) . "'"
            if ($opt_d);
        syslog 5, "Running assessment '$$rowref[0]' will end at '"
            . localtime( $$rowref[1] ) . "'";

        # Only handle assessments with end times before (or at) NOW
        next if ( $$rowref[1] > $NowTime );

        # Update the status to "finished"
        $StmtHandle_SetFinished->execute("$$rowref[0]");
        warn
            "WARNING: Assessment '$$rowref[0]' could not be set to 'finished' ("
            . $StmtHandle_SetFinished->errstr . ")\n"
            if ( $StmtHandle_SetFinished->rows <= 0 );
    } ## end while ( my $rowref = ( shift...))

    # Show finished assessments only if requested by '-f' option
    if ($opt_f)
    {
        # Search for all finished assessments
        print "List of finished assessments:\n" . '=' x 78 . "\n";
        my $StmtHandle_AR
            = $DBHandle->prepare(
            'SELECT R_Date,E_Email,R_BrowserID,R_ClientIP,R_RealIP,R_Other FROM as_results WHERE AS_id = ?'
            );
        my $StmtHandle_AS = $DBHandle->prepare(
            'SELECT id,EndTime FROM assessments WHERE Status = ?');
        $StmtHandle_AS->execute('F') or return;
        my $buffer_AS   = '';
        my $RowCache_AS = [];
        while (
            my $rowref_AS = (
                shift(@$RowCache_AS)
                    ||    # get row from cache, or reload cache :
                    shift(
                    @{  $RowCache_AS
                            = $StmtHandle_AS->fetchall_arrayref( undef, 1000 )
                            || []
                    }
                    )
            )
            )
        {
            # Assessment attributes
            print " Assessment '$$rowref_AS[0]' finished on '"
                . localtime( $$rowref_AS[1] ) . "'\n";
            syslog 5,
                "Finished assessment '$$rowref_AS[0]' ended at '"
                . localtime( $$rowref_AS[1] ) . "'";

            # Get the results for this assessment
            $StmtHandle_AR->execute("$$rowref_AS[0]") or next;
            my $buffer_AR   = '';
            my $RowCache_AR = [];
            while (
                my $rowref_AR = (
                    shift(@$RowCache_AR)
                        ||    # get row from cache, or reload cache :
                        shift(
                        @{  $RowCache_AR
                                = $StmtHandle_AR->fetchall_arrayref( undef,
                                1000 )
                                || []
                        }
                        )
                )
                )
            {
                print "  Date: $$rowref_AR[0], Email = $$rowref_AR[1]\n",
                    "  Client IP = $$rowref_AR[3], Real IP = $$rowref_AR[4]\n",
                    "  Browser ID = $$rowref_AR[2]\n",
                    "  Other info = $$rowref_AR[5]\n\n";
            } ## end while ( my $rowref_AR = (...))
        } ## end while ( my $rowref_AS = (...))
    } ## end if ($opt_f)
} ## end sub CheckAssessments

#--------------------------------------------------------------------
sub ShowUsage()
{
    print "Usage: $ProgName [options]\n",
        "       -D host  Specify database host [default=$opt_D]\n",
        "       -B URL   Specify base URL for link in email [default=$opt_B]\n",
        "       -r email Specify email address for return receipts [default=$opt_r]\n",
        "       -f       Show finished assessments as well [default=no]\n",
        "       -A       (re)activate the 'builtin' assessment\n",
        "       -h       Show this help [default=no]\n",
        "       -d       Show some debug info on STDERR [default=no]\n\n";

    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Main program starts here

# Ensure that we aren't tripping over each other
open( SELF, '<', "$0" )
    or die "Another process named '$0' already running!\n";
flock SELF, LOCK_EX | LOCK_NB or exit;

# Check options
getopts('AB:dD:fhr:') || ShowUsage();
ShowUsage() if ($opt_h);

# Make sure that we log notices
openlog "$ProgName", LOG_PID, LOG_DAEMON;

if ($opt_A)
{
    # (re)activate the 'builtin' assessment
    # The database connection to the local server
    my $DBHandle = DBI->connect(
        "DBI:mysql:sea:$opt_D",
        'jeepOpa',
        's3shuv29n',
        {   RaiseError => 0,
            PrintError => $opt_d,
            AutoCommit => 1,
        }
    ) or die "ERROR: Can not connect to local database ($DBI::errstr)\n";
    DBI->trace($opt_d);

    # Now and 1 hour from now same time!
    my $Now          = time();
    my $OneHourLater = $Now + 3600;
    $DBHandle->do(
        "UPDATE assessments SET Status='S',StartTime=$Now,EndTime=$OneHourLater WHERE Name='Builtin'"
    );
    $DBHandle->disconnect;
} ## end if ($opt_A)

# Check the status of assessments and do the right thing
CheckAssessments();

# We are done
closelog;
exit 0;
__END__
