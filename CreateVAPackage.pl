#!/usr/bin/perl -w
###############################################################################
use constant PROG_VERSION => '2.0';
#
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Authoritative repository:
# https://gitlab.com/bluc/IT-Security.git
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#==============================================================================
#
# Schema for (sqlite3) Master Score database:
#S3-SCHEMA#CREATE TABLE `PluginScores` (
#S3-SCHEMA#  `PluginID` char(255) NOT NULL DEFAULT '',
#S3-SCHEMA#  `PolicyID` integer NOT NULL DEFAULT '0',
#S3-SCHEMA#  `PolicyName` char(255) NOT NULL DEFAULT '',
#S3-SCHEMA#  `PolicyScore` integer DEFAULT NULL,
#S3-SCHEMA#  `PolicyComment` text NOT NULL,
#S3-SCHEMA#  PRIMARY KEY (`PluginID`,`PolicyName`)
#S3-SCHEMA#);
#S3-SCHEMA#CREATE TABLE `PolicyNames` (
#S3-SCHEMA#  `id` integer NOT NULL PRIMARY KEY AUTOINCREMENT,
#S3-SCHEMA#  `PolicyName` char(255) NOT NULL DEFAULT '',
#MS-SCHEMA#   PRIMARY KEY (`id`)
#S3-SCHEMA#);

# Schema for (mysql) Master Score database:
#MS-SCHEMA# -- Create database if it doesn't exist yet
#MS-SCHEMA# CREATE DATABASE IF NOT EXISTS '<whatever is set in configuration file>';
#MS-SCHEMA#
#MS-SCHEMA# -- User setup
#MS-SCHEMA# USE mysql;
#MS-SCHEMA# GRANT USAGE ON *.* TO '<whatever is set in configuration file>'@'%' IDENTIFIED BY PASSWORD '<whatever is set in configuration file>';
#MS-SCHEMA# GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE TEMPORARY TABLES ON '<whatever is set in configuration file>'.* TO '<whatever is set in configuration file>'@'%';
#MS-SCHEMA# -- SET PASSWORD FOR '<whatever is set in configuration file>'=PASSWORD('<whatever is set in configuration file');
#MS-SCHEMA#
#MS-SCHEMA# -- MasterScores database contents
#MS-SCHEMA# USE '<whatever is set in configuration file>';
#MS-SCHEMA# --
#MS-SCHEMA# -- Table structure for table `PluginScores`
#MS-SCHEMA# --
#MS-SCHEMA# CREATE TABLE IF NOT EXISTS `PluginScores` (
#MS-SCHEMA#   `PluginID` char(255) NOT NULL DEFAULT '',
#MS-SCHEMA#   `PolicyID` int(11) NOT NULL DEFAULT '0',
#MS-SCHEMA#   `PolicyName` char(255) NOT NULL DEFAULT '',
#MS-SCHEMA#   `PolicyScore` int(3) DEFAULT NULL,
#MS-SCHEMA#   `PolicyComment` text NOT NULL,
#MS-SCHEMA#   PRIMARY KEY (`PluginID`,`PolicyName`)
#MS-SCHEMA# ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
#MS-SCHEMA#
#MS-SCHEMA# --
#MS-SCHEMA# -- Table structure for table `PolicyNames`
#MS-SCHEMA# --
#MS-SCHEMA# CREATE TABLE IF NOT EXISTS `PolicyNames` (
#MS-SCHEMA#   `id` int(11) NOT NULL AUTO_INCREMENT,
#MS-SCHEMA#   `PolicyName` char(255) NOT NULL DEFAULT '',
#MS-SCHEMA#   PRIMARY KEY (`id`)
#MS-SCHEMA# ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

###############################################################################
$ENV{PATH} =
  '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin';

#--------------------------------------------------------------------
# Needed packages
#--------------------------------------------------------------------
BEGIN
{
    # Necessary debian packages:
    my @NeededPackages = (
        'lynx',                    'sed',
        'htmldoc',                 'ttf-dejavu-core',
        'libconfig-inifiles-perl', 'libdbd-sqlite3-perl',
        'libconfig-inifiles-perl', 'libgd-graph-perl',
        'libarchive-zip-perl',
    );

    my $MustInstallFirst = 0;
    foreach my $DebianPackage (@NeededPackages)
    {
        my $HavePackage = 0;
        if ( open( DP, '-|', "dpkg -l $DebianPackage 2>/dev/null" ) )
        {
            while (<DP>)
            {
                if (/^ii\s+$DebianPackage/)
                {
                    $HavePackage++;
                    last;
                } ## end if (/^ii\s+$DebianPackage/...)
            } ## end while (<DP>)
            close(DP);
        } ## end if ( open( DP, '-|', "dpkg -l $DebianPackage 2>/dev/null"...))
        next if ($HavePackage);

        # Install the missing debian package
        warn "Debian package '$DebianPackage' not yet installed\n";
        $MustInstallFirst++;
        # system ("sudo apt-get -y install $DebianPackage");
    } ## end foreach my $DebianPackage (...)
    die "$MustInstallFirst debian packages missing\n"
      if ($MustInstallFirst);
} ## end BEGIN

# Perl modules
use Cwd;
use File::Basename;
use DBI qw(:sql_types);
use Text::Wrap;
use Getopt::Std;
use File::Basename;
use HTML::Entities;
use HTTP::Date;
use XML::TreePP;
use GD::Graph::hbars;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use POSIX qw(strftime);
use Config::IniFiles;

#--------------------------------------------------------------------
# Globals
#--------------------------------------------------------------------
# Constants
my $ProgName = q{};
( $ProgName = $0 ) =~ s%.*/%%;

# Program options
our $opt_d = 0;            # debug
our $opt_f = q{};          # xml file
our $opt_h = 0;            # help
our $opt_p = q{};          # action
our $opt_u = q{};          # small logo to uuencode
our $opt_U = q{};          # large logo to uuencode
our $opt_v = 0;            # show progress
our $opt_c = $ProgName;    # configuration file
$opt_c =~ s/\.pl$/.conf/;

# The following variables MUST be set
#  in a seperate configuration file
my $ExternalConfig;

# Internal globals that can not be overwritten
#  by the configuration file

# Database connection
my %DBMS             = ();
my $dbh_Results      = undef;
my $dbh_MasterScores = undef;
my $ResultsDB        = './nit-results.db';

# Ancillary files
my $CommentsFile = q{};
my @Comments     = ();

# Default score and list of unknown plugins
my $UnknownScore   = 99;
my $UnknownPlugins = q{};

# In-memory databases
my %HostResult = ();
my @RiskText   = ();
my @RiskColor  = ();
my @DefaultPluginFields =
  qw(PluginID Name Family Protocol Port ServiceName);
my @ExtraPluginFields =
  qw(description synopsis Comment solution see_also xref bid plugin_output vuln_publication_date cvss_vector cvss_base_score cve patch_publication_date);
my %LogoFile = (
    'small' => '.logo.CVP_small.jpg',
    'large' => '.logo.CVP_large.jpg'
);

# Values for placeholders in templates
# (adapt if you have more than the default ones
#  - see function 'ReplaceTemplateValues')
my %TemplateValues = (
    'company_name'  => q{},
    'company_email' => q{},
    'company_url'   => q{},
    'cissp'         => q{}
);

# Misc. settings
my $ValueSep   = 'v^v^v^Afbe7Frokw8dadUtor^v^v^v';
my $HTMLFooter = "</body>\n</html>\n<!-- NEW PAGE -->\n";

# Text lines are at most 72 characters long
$Text::Wrap::columns = 72;

#--------------------------------------------------------------------
# Trim leading and trailing spaces from a string
#--------------------------------------------------------------------
sub trim ($)
{
    my ($Str2Trim) = @_;
    $Str2Trim =~ s/^\s+//;
    $Str2Trim =~ s/\s+$//;
    return $Str2Trim;
} ## end sub trim ($)

#--------------------------------------------------------------------
# Connect to the results database
#--------------------------------------------------------------------
sub ConnectResultsDB()
{
    # Connect/create the database
    my $dbargs = {
        AutoCommit => 1,
        PrintWarn  => 1,
        PrintError => 1
    };

    $dbh_Results =
      DBI->connect( 'dbi:SQLite:dbname=nit-results.db', q{}, q{},
        $dbargs );
    die "$DBI::errstr\n" if ( $dbh_Results->err() );

 # Speedups as per http://search.cpan.org/dist/DBD-SQLite/lib/DBD/SQLite.pm
    $dbh_Results->do('PRAGMA synchronous = 0');
    if ($opt_d)
    {

        # Debugging
        $dbh_Results->trace(3);
        $dbh_Results->do('PRAGMA vdbe_trace = ON');
        $dbh_Results->do('PRAGMA parser_trace = ON');
    } ## end if ($opt_d)
} ## end sub ConnectResultsDB

#--------------------------------------------------------------------
# Read in .xml, CustomerName.in and NI.iprange files
#--------------------------------------------------------------------
sub ReadInput()
{

    # Connect/create the results database
    if ( -s "$ResultsDB" )
    {
        print "WARNING: Results database '$ResultsDB' already exists.\n",
          "         Do you want to delete it and continue ('N' exits the program) [y/N] ? ";
        my $UserIN = <>;
        exit 0 unless ( $UserIN =~ /^y/i );
        unlink("$ResultsDB");
    } ## end if ( -s "$ResultsDB" )
    ConnectResultsDB();

    # Create the necessary tables for the database (if necessary)
    $dbh_Results->do( 'CREATE TABLE IF NOT EXISTS GeneralInfo '
          . '(PolicyName CHAR(255), InputFile CHAR(255), CustomerName CHAR(255), PhaseDone CHAR(255), ScannedIPs TEXT, OverallScore INT(3))'
    );
    die "Create GeneralInfo: $DBI::errstr\n" if ( $dbh_Results->err() );
    $dbh_Results->do( 'CREATE TABLE IF NOT EXISTS HostInfo '
          . '(HostIP CHAR(255), HostFQDN CHAR(255), OSGuess CHAR(255), ScanStart CHAR(255), ScanEnd CHAR(255), HighestScore INT(3), SumCritOrMedScore INT(6), PRIMARY KEY (HostIP))'
    );
    die "Create HostInfo: $DBI::errstr\n" if ( $dbh_Results->err() );
    $dbh_Results->do( 'CREATE TABLE IF NOT EXISTS HostPluginInfo '
          . '(HostIP CHAR(255), PluginID CHAR(255), Protocol CHAR(255), Port INT(6), ServiceName CHAR(255), Comment TEXT)'
    );
    die "Create HostPluginInfo: $DBI::errstr\n" if ( $dbh_Results->err() );
    $dbh_Results->do( 'CREATE TABLE IF NOT EXISTS PluginInfo '
          . '(PluginID CHAR(255), PluginName TEXT, PluginFamily TEXT, '
          . 'PRIMARY KEY (PluginID))' );
    die "Create PluginInfo: $DBI::errstr\n" if ( $dbh_Results->err() );
    $dbh_Results->do( 'CREATE TABLE IF NOT EXISTS PluginInfoDetails '
          . '(PluginID CHAR(255), Port INT(6), HostIP CHAR(255), xref TEXT, bid TEXT, plugin_output TEXT, vuln_publication_date DATE, cvss_vector DATE, cve DATE, description TEXT, cvss_base_score CHAR(255), '
          . 'solution TEXT, synopsis TEXT, see_also TEXT, patch_publication_date DATE, LocalScore INT(3), Comment TEXT, PRIMARY KEY (PluginID,Port,HostIP))'
    );
    die "Create PluginInfoDetails: $DBI::errstr\n"
      if ( $dbh_Results->err() );

    # Get the first non-empty line of the file containing the customer name
    my $CustomerName = q{};
    if ( open( CF, '<', './CustomerName.in' ) )
    {
        warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
          . " => Reading 'Customer.in'\n"
          if ($opt_v);
        while ( my $Line = <CF> )
        {

            # Remove CRLF and leading/trailing spaces
            chomp($Line);

            $Line =~ s/^\s*//o;
            $Line =~ s/\s*$//o;
            if ( length($Line) )
            {
                $CustomerName = "$Line";
                last;
            } ## end if ( length($Line) )
        } ## end while ( my $Line = <CF> )
        close(CF);
    } else
    {

        #TBD#
        # Ask for the customer name
    } ## end else [ if ( open( CF, '<', './CustomerName.in'...))]

    # Read in the NI.iprange file
    my $IPRanges = q{};
    if ( open( CF, '<', './NI.iprange' ) )
    {
        warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
          . " => Reading 'NI.iprange'\n"
          if ($opt_v);
        while ( my $Line = <CF> )
        {

            # Remove CRLF and leading/trailing spaces
            chomp($Line);
            $Line =~ s/^\s*//o;
            $Line =~ s/\s*$//o;
            $IPRanges .= ",$Line" if ( length($Line) );
        } ## end while ( my $Line = <CF> )
        close(CF);
        $IPRanges =~ s/^,//;
    } else
    {

        #TBD#
        # Ask for the IP addresses/ranges
    } ## end else [ if ( open( CF, '<', './NI.iprange'...))]

    # Read in the xml file
    my $tpp       = XML::TreePP->new();
    my $tree      = $tpp->parsefile($opt_f);
    my @host_data = ();
    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . " => Reading xml file '$opt_f'\n"
      if ($opt_v);
    my @report_data;

    if (
        ref( $tree->{NessusClientData_v2}->{Report}->{ReportHost} ) eq
        "HASH" )
    {
        push @report_data,
          $tree->{NessusClientData_v2}->{Report}->{ReportHost};
    } elsif (
        ref( $tree->{NessusClientData_v2}->{Report}->{ReportHost} ) eq
        "ARRAY" )
    {
        @report_data =
          @{ $tree->{NessusClientData_v2}->{Report}->{ReportHost} };
    } ## end elsif ( ref( $tree->{NessusClientData_v2...}))
    foreach my $hostproperties (@report_data)
    {
        my %hash;
        $hash{file} = $opt_f;
        $hash{name} = $hostproperties->{-name};
        my @host;
        if ( ref( $hostproperties->{HostProperties}->{tag} ) eq "HASH" )
        {
            push @host, $hostproperties->{HostProperties}->{tag};
        } elsif (
            ref( $hostproperties->{HostProperties}->{tag} ) eq "ARRAY" )
        {
            @host = @{ $hostproperties->{HostProperties}->{tag} };
        } ## end elsif ( ref( $hostproperties...))
        $hash{host_report} = $hostproperties->{ReportItem};
        foreach my $host (@host)
        {
            $hash{ $host->{-name} } = $host->{"#text"};
        }    # end - foreach my $host_data (@host_data)
        $hostproperties = \%hash;
    }    # end foreach my $hostproperties (@host_data)
    push @host_data, @report_data;

    # Save host and plugin info
    my $sth_HostInfo = $dbh_Results->prepare(
        'INSERT INTO HostInfo VALUES (?, ?, ?, ?, ?, ?, ?)');
    my $sth_HostPluginInfo = $dbh_Results->prepare(
        'INSERT INTO HostPluginInfo VALUES (?, ?, ?, ?, ?, ?)');
    my $sth_PluginInfo =
      $dbh_Results->prepare('REPLACE INTO PluginInfo VALUES (?, ?, ?)');
    my $sth_PluginInfoDetails = $dbh_Results->prepare(
        'REPLACE INTO PluginInfoDetails VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    );

    my $GlobalScanStart = q{};
    foreach my $host (@host_data)
    {
        my %HostInfo = ();
        foreach my $etype (qw{name fqdn HOST_START HOST_END})
        {
            $HostInfo{$etype} =
              ( defined( $host->{"$etype"} ) ) ? $host->{"$etype"} : q{};
        } ## end foreach my $etype (qw{name fqdn HOST_START HOST_END}...)
        $HostInfo{operating_system} =
          ( defined( $host->{'operating-system'} ) )
          ? $host->{'operating-system'}
          : q{};

        $sth_HostInfo->execute(
            "$HostInfo{name}",             "$HostInfo{fqdn}",
            "$HostInfo{operating_system}", "$HostInfo{HOST_START}",
            "$HostInfo{HOST_END}",         0,
            0
        );
        die "Populate HostInfo: $DBI::errstr\n" if ( $dbh_Results->err() );

        # Skip dead hosts
        next unless ( ref( $host->{host_report} ) eq "ARRAY" );

        foreach my $entry ( @{ $host->{host_report} } )
        {

            # Skip invalid plugin IDs
            next if ( $entry->{'-pluginID'} < 0 );

            my %PluginInfo = ();

            # Setup default values for the plugin infos
            $PluginInfo{'PluginID'}    = $entry->{-pluginID};
            $PluginInfo{'Name'}        = $entry->{-pluginName};
            $PluginInfo{'Family'}      = $entry->{-pluginFamily};
            $PluginInfo{'Protocol'}    = $entry->{-protocol};
            $PluginInfo{'Port'}        = $entry->{-port};
            $PluginInfo{'ServiceName'} = $entry->{-svc_name};
            foreach my $etype (@ExtraPluginFields)
            {
                $PluginInfo{"$etype"} = q{};
            } ## end foreach my $etype (@ExtraPluginFields...)

            if ( $entry->{'-pluginID'} == 0 )
            {

                # Make up a new plugin ID for ID "0"
                $PluginInfo{'PluginID'} =
                  $entry->{-port} . '_' . $entry->{-protocol};

                # Make up some synopsis
                $PluginInfo{synopsis} =
                  "Something answered on port $entry->{-port}/$entry->{-protocol} during the port scan";
            } elsif ( $entry->{'-pluginID'} > 0 )
            {

                # Get detailed infos (if present)
                foreach my $etype (@ExtraPluginFields)
                {
                    if ( $entry->{$etype} )
                    {
                        if ( ref( $entry->{$etype} ) eq "ARRAY" )
                        {

                            # Combine several values into one
                            $PluginInfo{"$etype"} =
                              join( $ValueSep, @{ $entry->{$etype} } );
                        } else
                        {
                            $PluginInfo{"$etype"} = "$entry->{$etype}";
                        } ## end else [ if ( ref( $entry->{$etype...}))]
                    } ## end if ( $entry->{$etype} ...)
                } ## end foreach my $etype (@ExtraPluginFields...)
            } ## end elsif ( $entry->{'-pluginID'...})

            # Populate the database tables
            $sth_HostPluginInfo->execute(
                "$HostInfo{name}",          "$PluginInfo{PluginID}",
                "$PluginInfo{Protocol}",    "$PluginInfo{Port}",
                "$PluginInfo{ServiceName}", q{}
            );
            die "Populate HostPluginInfo: $DBI::errstr\n"
              if ( $dbh_Results->err() );

            $sth_PluginInfo->execute( "$PluginInfo{PluginID}",
                "$PluginInfo{Name}", "$PluginInfo{Family}" );
            die "Populate PluginInfo: $DBI::errstr\n"
              if ( $dbh_Results->err() );

            $sth_PluginInfoDetails->execute(
                "$PluginInfo{PluginID}",
                "$PluginInfo{Port}",
                "$HostInfo{name}",
                "$PluginInfo{xref}",
                "$PluginInfo{bid}",
                "$PluginInfo{plugin_output}",
                "$PluginInfo{vuln_publication_date}",
                "$PluginInfo{cvss_vector}",
                "$PluginInfo{cve}",
                "$PluginInfo{description}",
                "$PluginInfo{cvss_base_score}",
                "$PluginInfo{solution}",
                "$PluginInfo{synopsis}",
                "$PluginInfo{see_also}",
                "$PluginInfo{patch_publication_date}",
                $UnknownScore,
                q{}
            );
            die "Populate PluginInfoDetails: $DBI::errstr\n"
              if ( $dbh_Results->err() );

            unless ( length($GlobalScanStart) )
            {
                if ( $PluginInfo{PluginID} =~ /19506/o )
                {

                    # Save the global scan start as show in this plugin
                    if ( $PluginInfo{plugin_output} =~
                        /Scan\s+Start\s+Date\s*:\s*(2\d\d\d\/\d{1,2}\/\d{1,2}\s+\d{1,2}:\d{1,2})/mo
                      )
                    {
                        $GlobalScanStart = time2str( str2time("$1") );
                    } ## end if ( $PluginInfo{plugin_output...})
                } ## end if ( $PluginInfo{PluginID...})
            } ## end unless ( length($GlobalScanStart...))
        } ## end foreach my $entry ( @{ $host...})
    } ## end foreach my $host (@host_data...)

    # Fill in any scan start fields that are empty
    #    $GlobalScanStart =q{};
    $dbh_Results->do(
        "UPDATE HostInfo SET ScanStart='$GlobalScanStart' WHERE ScanStart = ''"
    );

    # Save general infos
    my $sth = $dbh_Results->prepare(
        'INSERT INTO GeneralInfo VALUES (?, ?, ?, ?, ?, ?)');
    $sth->execute( "$tree->{NessusClientData_v2}->{Policy}->{policyName}",
        "$opt_f", "$CustomerName", 'i', "$IPRanges", 0 );
    die "Save GeneralInfo: $DBI::errstr\n" if ( $dbh_Results->err() );

    # Close the database
    $dbh_Results->disconnect;
    $dbh_Results = undef;
} ## end sub ReadInput

#--------------------------------------------------------------------
# Create local copy of master scores
#--------------------------------------------------------------------
sub ReadMasterScores()
{

    # Merge scores from master database to results database
    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . " => Merging scores from master database\n"
      if ($opt_v);

    # Connect to the results database
    ConnectResultsDB();

    # Get some general infos
    my $sth = $dbh_Results->prepare(
        'SELECT PhaseDone,PolicyName FROM GeneralInfo');
    $sth->execute();
    die "Get finished phases: $DBI::errstr\n" if ( $dbh_Results->err() );
    my ( $PhaseDone, $PolicyName ) = $sth->fetchrow_array();
    die "ERROR: Input phase not complete\n" unless ( $PhaseDone =~ /i/ );
    $sth->finish;

    my $MasterScores_Insert = q{};
  SW_MASTERSCORES_TYPE:
    {
        if ( $DBMS{'dbtype'} =~ /^sqlite3$/i )
        {

            $dbh_MasterScores = DBI->connect(
                "dbi:SQLite:dbname=$DBMS{'sqlite3_dbname'}",
                q{}, q{},
                {
                    RaiseError => 0,
                    PrintError => 1,
                    AutoCommit => 1
                }
            );
            die "$DBI::errstr\n" if ( $dbh_MasterScores->err() );

 # Speedups as per http://search.cpan.org/dist/DBD-SQLite/lib/DBD/SQLite.pm
            $dbh_MasterScores->do('PRAGMA synchronous = 0');
            if ($opt_d)
            {

                # Debugging
                $dbh_MasterScores->trace(3);
                $dbh_MasterScores->do('PRAGMA vdbe_trace = ON');
                $dbh_MasterScores->do('PRAGMA parser_trace = ON');
            } ## end if ($opt_d)
            $MasterScores_Insert =
              'INSERT INTO PluginScores VALUES (?, ?, ?, ?, ?)';
            last SW_MASTERSCORES_TYPE;
        } ## end if ( $DBMS{'dbtype'} =~...)

        if ( $DBMS{'dbtype'} =~ /^mysql$/i )
        {
            # Connect to the master score database (mysql version)
            $dbh_MasterScores = DBI->connect(
                "DBI:mysql:database=$DBMS{'mysql_dbname'};host=$DBMS{'mysql_dbserver'};port=$DBMS{'mysql_dbport'}",
                "$DBMS{'mysql_dbuserid'}",
                "$DBMS{'mysql_dbpassword'}",
                {
                    RaiseError => 0,
                    PrintError => 1,
                    AutoCommit => 1
                }
              )
              or die
              "ERROR: Can not connect to master score database ($DBI::errstr)\n";
            $MasterScores_Insert =
              'INSERT IGNORE INTO PluginScores VALUES (?, ?, ?, ?, ?)';
            last SW_MASTERSCORES_TYPE;
        } ## end if ( $DBMS{'dbtype'} =~...)
    } ## end SW_MASTERSCORES_TYPE:

    # Get the score for each plugin from the master and
    #  update the results database accordingly
    $sth = $dbh_Results->prepare(
        'SELECT PluginID FROM PluginInfo ORDER BY PluginID ASC');
    $sth->execute();
    die "Get all detected plugins: $DBI::errstr\n"
      if ( $dbh_Results->err() );
    my @DetectedPlugins = ();
    while ( my ($PluginID) = $sth->fetchrow_array )
    {
        push( @DetectedPlugins, "$PluginID" );
    } ## end while ( my ($PluginID) = ...)
    $sth->finish;

    $sth = $dbh_MasterScores->prepare(
        'SELECT PolicyScore,PolicyComment FROM PluginScores WHERE PluginID = ? AND PolicyName = ?'
    );
    $sth_update = $dbh_Results->prepare(
        'UPDATE PluginInfoDetails SET LocalScore = ?, Comment = ? WHERE PluginID = ?'
    );
    foreach my $PluginID (@DetectedPlugins)
    {
        $sth->execute( $PluginID, $PolicyName );
        die
          "Get master plugin score, and comment for '$PluginID/$PolicyName': $DBI::errstr\n"
          if ( $dbh_Results->err() );
        while ( my ( $PolicyScore, $PolicyComment ) =
            $sth->fetchrow_array )
        {

            # Skip any unknown scores
            next if ( $PolicyScore == $UnknownScore );

            # Update the results database
            $sth_update->execute( $PolicyScore, $PolicyComment,
                $PluginID );
            die "Update result score for '$PluginID': $DBI::errstr\n"
              if ( $dbh_Results->err() );
        } ## end while ( my ( $PolicyScore...))
    } ## end foreach my $PluginID (@DetectedPlugins...)
    $sth->finish;
    $sth_update->finish;

    # Find all plugins with and unknown score
    $sth = $dbh_Results->prepare(
        'SELECT PluginID,synopsis,description FROM PluginInfoDetails WHERE LocalScore = ? ORDER BY PluginID ASC'
    );
    $sth->execute($UnknownScore);
    die "Get unknown plugins: $DBI::errstr\n" if ( $dbh_Results->err() );
    my @UnknownPlugins = ();
    $sth_update = $dbh_MasterScores->prepare("$MasterScores_Insert");
    while ( my ( $PluginID, $Synopsis, $Description ) =
        $sth->fetchrow_array )
    {

        # Remember some values for display
        push( @UnknownPlugins,
            "\n===> Unknown score for plugin ID $PluginID:\n$Synopsis\n$Description\n"
        );

        # Update the master score database
        $sth_update->execute( $PluginID, 0, $PolicyName, $UnknownScore,
            q{} );
    } ## end while ( my ( $PluginID, $Synopsis...))
    $sth->finish;

    # Close the master score database
    $dbh_MasterScores->disconnect;
    $dbh_MasterScores = undef;

    # List unknown plugins
    my $NoUnknownPlugins = scalar @UnknownPlugins;
    if ($NoUnknownPlugins)
    {
        die "Found $NoUnknownPlugins unknown plugin(s):\n "
          . join( "\n ", @UnknownPlugins ) . "\n";
    } ## end if ($NoUnknownPlugins)

    # Update the phase
    $sth = $dbh_Results->prepare('SELECT PhaseDone From GeneralInfo');
    $sth->execute();
    die "Get phase in 'm': $DBI::errstr\n"
      if ( $dbh_Results->err() );
    ($PhaseDone) = $sth->fetchrow_array;
    $sth->finish;
    unless ( $PhaseDone =~ /m/ )
    {
        $PhaseDone .= 'm';
        $sth_update =
          $dbh_Results->prepare('UPDATE GeneralInfo SET PhaseDone = ?');
        $sth_update->execute($PhaseDone);
        die "Updating phase 'm': $DBI::errstr\n"
          if ( $dbh_Results->err() );
        $sth_update->finish;
    } ## end unless ( $PhaseDone =~ /m/...)

    # Close the results database
    $dbh_Results->disconnect;
    $dbh_Results = undef;

} ## end sub ReadMasterScores

#--------------------------------------------------------------------
# Replace template placeholders in strings
# (adapt if you have more than the default ones - see above)
#--------------------------------------------------------------------
sub ReplaceTemplateValues($)
{
    my ($String_Ref) = @_;

    $$String_Ref =~ s/%%%COMPANY_NAME%%%/$TemplateValues{'company_name'}/g;
    $$String_Ref =~
      s/%%%COMPANY_EMAIL%%%/$TemplateValues{'company_email'}/g;
    $$String_Ref =~ s/%%%COMPANY_URL%%%/$TemplateValues{'company_url'}/g;
    $$String_Ref =~ s/%%%CISSP%%%/$TemplateValues{'cissp'}/g;
} ## end sub ReplaceTemplateValues($)

#--------------------------------------------------------------------
# Score all findings
#--------------------------------------------------------------------
sub ScoreFindings()
{
    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . " => Score all findings\n"
      if ($opt_v);

    # Connect to the results database
    ConnectResultsDB();

    # Get the plugins found per host
    my $sth = $dbh_Results->prepare(
        'SELECT HostPluginInfo.HostIP,HostPluginInfo.PluginID FROM HostPluginInfo LEFT JOIN HostInfo ON HostPluginInfo.HostIP = HostInfo.HostIP'
    );
    $sth->execute();
    die "Get plugins for hosts: $DBI::errstr\n" if ( $dbh_Results->err() );
    my %HostPluginList = ();
    my %NoPlugins      = ();
    while ( my ( $HostIP, $PluginID ) = $sth->fetchrow_array )
    {

        # Collect the plugin IDs
        $HostPluginList{"$HostIP"} .= "$PluginID,";
        $NoPlugins{"$HostIP"}++;

    } ## end while ( my ( $HostIP, $PluginID...))
    $sth->finish;

    my $sth_update_HighestScore = $dbh_Results->prepare(
        'UPDATE HostInfo SET HighestScore = ? WHERE HostIP = ?');
    my $sth_update_SumCritOrMedScore = $dbh_Results->prepare(
        'UPDATE HostInfo SET SumCritOrMedScore = ? WHERE HostIP = ?');
    for my $HostIP ( keys %HostPluginList )
    {

        # Get the total score of all medium and high plugins
        #  AND the highest score of ALL plugins for this host
        my $SumCritOrMedScore = -1;
        my $HostMaxScore      = -1;

        my $PluginList = $HostPluginList{"$HostIP"};
        $PluginList =~ s/,$//;
        my @Plugins = split( /,/, $PluginList );
        my $PluginBatch = 0;
        my $BatchSize =
          ( $NoPlugins{"$HostIP"} > 150 ) ? 150 : $NoPlugins{"$HostIP"};

        # Check plugins in batches of $BatchSize
        while ( $PluginBatch < $NoPlugins{"$HostIP"} )
        {
            warn "DEBUG: Current PluginBatch = $PluginBatch (total: "
              . $NoPlugins{"$HostIP"} . ")\n"
              if ($opt_d);
            $BatchSize =
              ( ( $NoPlugins{"$HostIP"} - $PluginBatch ) > 150 )
              ? 150
              : $NoPlugins{"$HostIP"} - $PluginBatch;
            my $Placeholders = '?,' x $BatchSize;
            $Placeholders =~ s/,$//;
            $sth = $dbh_Results->prepare(
                "SELECT MAX(LocalScore) FROM PluginInfoDetails WHERE PluginID IN ($Placeholders)"
            );
            my @PluginBatch = splice( @Plugins, 0, $BatchSize );
            $sth->execute(@PluginBatch);
            die "Get max score for '$HostIP': $DBI::errstr\n"
              if ( $dbh_Results->err() );
            my ($ThisBatch_HostMaxScore) = $sth->fetchrow_array;
            $HostMaxScore = $ThisBatch_HostMaxScore
              if ( $ThisBatch_HostMaxScore > $HostMaxScore );

            warn
              "DEBUG: Current HostMaxScore for '$HostIP' = $HostMaxScore\n"
              if ($opt_d);

            $sth = $dbh_Results->prepare(
                "SELECT SUM(LocalScore) FROM PluginInfoDetails WHERE PluginID IN ($Placeholders) AND (LocalScore > 19)"
            );
            $sth->execute(@PluginBatch);
            die
              "Get sum of medium and high score for '$HostIP': $DBI::errstr\n"
              if ( $dbh_Results->err() );
            ($SumCritOrMedScore) = $sth->fetchrow_array;

            warn
              "DEBUG: Current SumCritOrMedScore for '$HostIP' = $SumCritOrMedScore\n"
              if ($opt_d);

            if ( ( $PluginBatch + $BatchSize ) > $NoPlugins{"$HostIP"} )
            {

                # We are done
                last;
            } else
            {

                # Check the next batch of $BatchSize
                $PluginBatch += $BatchSize;
            } ## end else [ if ( ( $PluginBatch + ...))]
        } ## end while ( $PluginBatch < $NoPlugins...)

        # Remember the total score of all medium and high plugins
        $sth_update_SumCritOrMedScore->execute( $SumCritOrMedScore,
            $HostIP )
          if ($SumCritOrMedScore);

        # Remember the high score
        $sth_update_HighestScore->execute( $HostMaxScore, $HostIP )
          if ($HostMaxScore);
    } ## end for my $HostIP ( keys %HostPluginList...)
    $sth->finish;

    # Determine overall score
    # 39 if (at least one host with HighestScore >= 30)
    #    or (at least 75% or all hosts with HighestScore >= 20)
    # 29 if (at least one host with HighestScore >= 20)
    # 19 if (at least one host with HighestScore >= 10)
    #  9 in all other cases
    my $OverallScore = 9;
    $sth = $dbh_Results->prepare(
        'SELECT COUNT(*) FROM HostInfo WHERE HighestScore >= 30');
    $sth->execute();
    die "Get number of hosts with 'high' score: $DBI::errstr\n"
      if ( $dbh_Results->err() );
    my ($HighCount) = $sth->fetchrow_array;
    $sth->finish;
    if ( $HighCount > 0 )
    {

        # At least one 'high' HighestScore
        $OverallScore = 39;
    } else
    {
        $sth->finish;
        $sth = $dbh_Results->prepare(
            'SELECT COUNT(*) FROM HostInfo WHERE HighestScore >= 20');
        $sth->execute();
        die "Get number of hosts with 'medium' score: $DBI::errstr\n"
          if ( $dbh_Results->err() );
        my ($MediumCount) = $sth->fetchrow_array;
        $sth->finish;

        if ( $MediumCount > 0 )
        {
            $sth = $dbh_Results->prepare('SELECT COUNT(*) FROM HostInfo');
            $sth->execute();
            die "Get number of total hosts: $DBI::errstr\n"
              if ( $dbh_Results->err() );
            my ($TotalHosts) = $sth->fetchrow_array;

            # At least one host with 'medium' HighestScore
            $OverallScore = 29;
            if (    ( $TotalHosts > 1 )
                and ( $MediumCount > ( 0.75 * $TotalHosts ) ) )
            {

                # More than 75% of all hosts have 'medium' HighestScores
                $OverallScore = 39;
            } ## end if ( ( $TotalHosts > 1...))
            $sth->finish;
        } else
        {
            $sth = $dbh_Results->prepare(
                'SELECT COUNT(*) FROM HostInfo WHERE HighestScore >= 10');
            $sth->execute();
            die "Get number of hosts with 'low' score: $DBI::errstr\n"
              if ( $dbh_Results->err() );
            my ($LowCount) = $sth->fetchrow_array;
            $sth->finish;
            if ( $LowCount > 0 )
            {

                # At least one host with 'low' HighestScore
                $OverallScore = 19;
            } ## end if ( $LowCount > 0 )
        } ## end else [ if ( $MediumCount > 0 ...)]
    } ## end else [ if ( $HighCount > 0 ) ]

    # Update the overall score
    $sth_update =
      $dbh_Results->prepare('UPDATE GeneralInfo SET OverallScore = ?');
    $sth_update->execute($OverallScore);
    die "Updating overall score: $DBI::errstr\n"
      if ( $dbh_Results->err() );
    $sth_update->finish;

    # Update the phase
    $sth = $dbh_Results->prepare('SELECT PhaseDone From GeneralInfo');
    $sth->execute();
    die "Get phase in 's': $DBI::errstr\n"
      if ( $dbh_Results->err() );
    my ($PhaseDone) = $sth->fetchrow_array;
    $sth->finish;
    unless ( $PhaseDone =~ /s/ )
    {
        $PhaseDone .= 's';
        $sth_update =
          $dbh_Results->prepare('UPDATE GeneralInfo SET PhaseDone = ?');
        $sth_update->execute($PhaseDone);
        die "Updating phase 'm': $DBI::errstr\n"
          if ( $dbh_Results->err() );
        $sth_update->finish;
    } ## end unless ( $PhaseDone =~ /s/...)

    # Close the results database
    $dbh_Results->disconnect;
    $dbh_Results = undef;
} ## end sub ScoreFindings

#--------------------------------------------------------------------
# Create a consistent HTML header across all documents
#--------------------------------------------------------------------
sub CreateHTMLHeader($$)
{
    my ( $Title, $PolicyName ) = @_;

    my $HTMLHeader =
      "<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n"
      . "<html>\n"
      . "<head>\n"
      . "<title>$Title</title>\n"
      . "<link rev='made' href='mailto:%%%COMPANY_EMAIL%%%'>\n"
      . "<meta name='copyright' content='(c) CopyRight 2013 B-LUC Consulting and Thomas Bullinger'>\n"
      . "<meta name='description' content='$Title'>\n"
      . "<meta http-equiv='Content-Type' content='text/html;charset=utf-8'>\n"
      . "<style type='text/css'>\n"
      . "<!--/* <![CDATA[ */\n"
      . "<!--\n"
      . "div,title,span,p,.p,h1,h2,h3,ul,li,a,td,th,select,input,textarea{\n"
      . "  font-family:'Trebuchet MS',Verdana,Arial,Helvetica,Swiss,Futura,sans-serif;font-size:11px;color:black;\n"
      . "}\n"
      . "pre {font-family:'Trebuchet MS',Verdana,Arial,Helvetica,Swiss,Futura,sans-serif;font-size:9px;color:black;}\n"
      . "td.doctitle {font-family:Palatino,Georgia,Times,serif;font-size:44px;font-variant:small-caps;letter-spacing:-1px;text-align:center;}\n"
      . "th.title {font-family: Palatino,Georgia,Times,serif;font-size: 16px;font-variant:small-caps;}\n"
      . "td.risk {font-family: Georgia,Times,serif;font-size:16px;}\n"
      . "a:link,a:visited {color:#0000FF;text-decoration:none;font-variant:inherit;}\n"
      . "a:hover{color: dodgerblue;text-decoration: underline;}\n"
      . "-->\n"
      . "/* ]]> */-->\n"
      . "</style>\n"
      . "<script type='text/javascript'>//<![CDATA[\n"
      . "  function SetWindowWidth() {\n"
      . "    var myWidth = 0, myHeight = 0, myHorz = 0;\n"
      . "    if( typeof( window.innerWidth ) == 'number' ) {\n"
      . "      //Non-IE\n"
      . "      myWidth = window.innerWidth;\n"
      . "      myHeight = window.innerHeight;\n"
      . "    } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {\n"
      . "      //IE 6+ in 'standards compliant mode'\n"
      . "      myWidth = document.documentElement.clientWidth;\n"
      . "      myHeight = document.documentElement.clientHeight;\n"
      . "    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {\n"
      . "      //IE 4 compatible\n"
      . "      myWidth = document.body.clientWidth;\n"
      . "      myHeight = document.body.clientHeight;\n"
      . "    }\n"
      . "    if (myWidth < 870) {\n"
      . "      myHorz = 870 - myWidth;\n"
      . "      window.resizeBy(myHorz, 0);\n"
      . "      window.alert ('Increased the browser window by '+myHorz+' pixels for better view');\n"
      . "    }\n" . "  }\n"
      . "//]]></script>\n"
      . "</head>\n\n"
      . "<body bgcolor='#F0FFFF' onload='SetWindowWidth()'>\n"
      . "<center><table align='center' cellspacing='0' cellpadding='0' width='800'>\n"
      . "<tr><td class='doctitle' colspan='2'>$PolicyName</td></tr>\n"
      . "<tr><td><font color='Silver'>Version "
      . PROG_VERSION
      . "</font></td><td align='right'><img src='$LogoFile{small}' alt='%%%COMPANY_NAME%%%'></td></tr>\n"
      . "<tr bgcolor='black'><td colspan='2'>&nbsp;</td></tr>\n"
      . "</table></center>\n"
      . "<center><table align='center' cellspacing='0' cellpadding='0' width='800'>\n"
      . "<tr><th class='title'>$Title<br><span style='font-variant:normal'>created "
      . POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . "</span></th></tr>\n";
    $HTMLHeader .=
      "<tr><td><font color='DarkSlateGray'><b>Note:</b> Only plugins/hosts with at least noteworthy (medium) scores are shown</font></td></tr>\n"
      if ( $Title eq 'Technical summary' );
    $HTMLHeader .= "<tr bgcolor='#585858'><td"
      . (
        ( $Title eq 'Technical Summary' )
        ? ' style="color:white; text-align:center">Plugins in order of severity and number of hosts'
        : '>&nbsp;'
      )
      . "</td></tr>\n"
      . "</table></center>\n";

    # Replace placeholders in the HTML header
    ReplaceTemplateValues( \$HTMLHeader );
    return ("$HTMLHeader");
} ## end sub CreateHTMLHeader($$)

#--------------------------------------------------------------------
# Create new zipfile
#--------------------------------------------------------------------
sub CreateZipFile($)
{
    my ($PolicyName) = @_;

    # First create an "index.html" file
    if ( open( SD, '>', 'index.html' ) )
    {
        my $HTMLContents = CreateHTMLHeader( 'Index', "$PolicyName" );
        $HTMLContents .= <<HCT;
<h3>The documents of interest are:</h3>
<ul>
 <li><a href="ExecutiveReport.pdf">A printable executive report. suitable for distribution</a>
 <li><a href="TechSummary.html">A summary of the technical findings</a> (this is the most useful document for the technical staff)
</ul>
<p><p>
<h3>Additional files are:</h3>
<ul>
 <li><a href="Remediation.txt">A editable document to keep track of remediation efforts</a>
 <li><a href="TechDetails.html">A searchable html version of the technical details</a>
 <li><a href="ExecSummary.html">A subset of the executive report in html format</a>
 <li><a href="index.html">This file</a>
</ul>
$HTMLFooter
HCT
        # Replace placeholders in the HTML contents
        ReplaceTemplateValues( \$HTMLContents );

        print SD "$HTMLContents";
        close(SD);
    } ## end if ( open( SD, '>', 'index.html'...))

    # Create the actual zip file
    my $zip = Archive::Zip->new();
    foreach my $ZipMember ( glob('*.html *.xml *.txt') )
    {
        my $file_member = $zip->addFile("$ZipMember");
        $file_member
          ->desiredCompressionLevel(COMPRESSION_LEVEL_BEST_COMPRESSION);
    } ## end foreach my $ZipMember ( glob...)
    foreach my $ZipMember ( glob('.*.jpg *.pdf') )
    {
        my $file_member = $zip->addFile("$ZipMember");
        $file_member->desiredCompressionLevel(COMPRESSION_LEVEL_NONE);
    } ## end foreach my $ZipMember ( glob...)
    die "ERROR: $!"
      unless ( $zip->writeToFileNamed('Customer.zip') == AZ_OK );
} ## end sub CreateZipFile($)

#--------------------------------------------------------------------
# Create the executive summary
#--------------------------------------------------------------------
sub CreateExecutiveSummary($)
{
    my ($PolicyName) = @_;

    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . " => Create executive summary\n"
      if ($opt_v);

    # Get the overall score and the number of scanned IP addresses
    my $HighestScore = 0;
    my $ScannedHosts = 0;
    $sth = $dbh_Results->prepare('SELECT OverallScore FROM GeneralInfo');
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($HighestScore) = $sth->fetchrow_array();

    # Get number of hosts per score range
    $sth = $dbh_Results->prepare('SELECT COUNT(HostIP) FROM HostInfo');
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($ScannedHosts) = $sth->fetchrow_array();

    my $UnknownHosts = 0;
    $sth = $dbh_Results->prepare(
        'SELECT COUNT(HostIP) FROM HostInfo WHERE HighestScore >= 99');
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($UnknownHosts) = $sth->fetchrow_array();

    my $HighHosts = 0;
    $sth = $dbh_Results->prepare(
        'SELECT COUNT(HostIP) FROM HostInfo WHERE HighestScore >= 30 AND HighestScore < 40'
    );
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($HighHosts) = $sth->fetchrow_array();

    my $MediumHosts = 0;
    $sth = $dbh_Results->prepare(
        'SELECT COUNT(HostIP) FROM HostInfo WHERE HighestScore >= 20 AND HighestScore < 30'
    );
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($MediumHosts) = $sth->fetchrow_array();

    my $LowHosts = 0;
    $sth = $dbh_Results->prepare(
        'SELECT COUNT(HostIP) FROM HostInfo WHERE HighestScore >= 10 AND HighestScore < 20'
    );
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($LowHosts) = $sth->fetchrow_array();

    my $NoneHosts = 0;
    $sth = $dbh_Results->prepare(
        'SELECT COUNT(HostIP) FROM HostInfo WHERE HighestScore < 10');
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($NoneHosts) = $sth->fetchrow_array();

    # Collect all hosts with high risk level
    my %HostHigh = ();
    $sth = $dbh_Results->prepare(
        'SELECT HostIP FROM HostInfo WHERE HighestScore >= 30 AND HighestScore < 40'
    );
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    while ( my $rowref = $sth->fetchrow_arrayref )
    {
        my $HostIP = $$rowref[0];
        $HostHigh{"$HostIP"}++;
    } ## end while ( my $rowref = $sth...)

    # Collect all hosts with medium risk level
    my %HostMedium = ();
    $sth = $dbh_Results->prepare(
        'SELECT HostIP FROM HostInfo WHERE HighestScore >= 20 AND HighestScore < 30'
    );
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    while ( my $rowref = $sth->fetchrow_arrayref )
    {
        my $HostIP = $$rowref[0];
        $HostMedium{"$HostIP"}++;
    } ## end while ( my $rowref = $sth...)

    my $ResultHosts = $HighHosts + $MediumHosts + $LowHosts;

    # Collect all plugins with high risk level
    my %PluginHigh = ();
    $sth = $dbh_Results->prepare(
        'SELECT PluginID FROM PluginInfoDetails WHERE LocalScore >= 30 AND LocalScore < 40'
    );
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    while ( my $rowref = $sth->fetchrow_arrayref )
    {
        my $PluginID = $$rowref[0];
        $PluginHigh{"$PluginID"}++;
    } ## end while ( my $rowref = $sth...)

    # Collect all plugins with medium risk level
    my %PluginMedium = ();
    $sth = $dbh_Results->prepare(
        'SELECT PluginID FROM PluginInfoDetails WHERE LocalScore >= 20 AND LocalScore < 30'
    );
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    while ( my $rowref = $sth->fetchrow_arrayref )
    {
        my $PluginID = $$rowref[0];
        $PluginMedium{"$PluginID"}++;
    } ## end while ( my $rowref = $sth...)

    # Get the start and stop of the scan
    $sth = $dbh_Results->prepare(
        'SELECT MIN(ScanStart),MAX(ScanEnd) FROM HostInfo');
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    my ( $ScanStart, $ScanEnd ) = $sth->fetchrow_array();

    if ( open( SD, '>', 'ExecSummary.html' ) )
    {

        # Create graph for the "hosts per score"
        my $mygraph = GD::Graph::hbars->new( 610, 260 );
        $mygraph->set(
            x_label => 'Level',
            y_label => 'Hosts',
            title   => 'Hosts per risk level',

            # Separate the bars with 4 pixels
            bar_spacing => 2,

            # Show values to the right of each bar
            show_values => 1,

            # Show some nice colours
            dclrs      => [qw(lred lorange lgreen lblue)],
            cycle_clrs => 1,

            # Use a fixed color for the background
            bgcolor     => 'lgray',
            transparent => 0
        );

        # Show the %%%COMPANY_NAME%%% logo if present
        $mygraph->set( logo => "$LogoFile{'small'}" )
          if ( -f "$LogoFile{'small'}" );

        # Use a nicer Truetype font in the graph if present
        if ( -f '/usr/share/fonts/truetype/ttf-dejavu/DejaVuSerif.ttf' )
        {
            my $GDFont =
              '/usr/share/fonts/truetype/ttf-dejavu/DejaVuSerif.ttf';
            if (
                -f '/usr/share/fonts/truetype/ttf-dejavu/DejaVuSerif-Bold.ttf'
              )
            {
                $mygraph->set_title_font(
                    '/usr/share/fonts/truetype/ttf-dejavu/DejaVuSerif-Bold.ttf',
                    11
                );
            } else
            {
                $mygraph->set_title_font( "$GDFont", 11 );
            } ## end else [ if ( -f ...)]
            $mygraph->set_x_label_font( "$GDFont", 9 );
            $mygraph->set_x_axis_font( "$GDFont", 9 );
            $mygraph->set_y_label_font( "$GDFont", 9 );
            $mygraph->set_y_axis_font( "$GDFont", 9 );
            if (
                -f '/usr/share/fonts/truetype/ttf-dejavu/DejaVuSerif-Italic.ttf'
              )
            {
                $mygraph->set_values_font(
                    '/usr/share/fonts/truetype/ttf-dejavu/DejaVuSerif-Italic.ttf',
                    10
                );
            } else
            {
                $mygraph->set_values_font( "$GDFont", 10 );
            } ## end else [ if ( -f ...)]
        } ## end if ( -f ...)
        my @data = (
            [ 'High',     'Medium',     'Low',     'None' ],
            [ $HighHosts, $MediumHosts, $LowHosts, $NoneHosts ]
        );
        my $myimage = $mygraph->plot( \@data );
        if ( open( PICTURE, '>', '.HostsPerLevel.jpg' ) )
        {

            # Make sure we are writing to a binary stream
            binmode PICTURE;

            # Convert the image to JPEG and print it to the file PICTURE
            print PICTURE $myimage->jpeg;
            close PICTURE;
        } ## end if ( open( PICTURE, '>'...))

        my $RiskLevel = "<font color='White'>none</font>";
        if ( $HighestScore >= 40 )
        {
            $RiskLevel =
              "<font color='White'>undeterminable (new or unknown findings)</font>";
        } elsif ( $HighestScore >= 30 )
        {
            $RiskLevel = "<font color='Red'>HIGH RISK</font>";
        } elsif ( $HighestScore >= 20 )
        {
            $RiskLevel = "<font color='#ff8040'>medium RISK</font>";
        } elsif ( $HighestScore >= 10 )
        {
            $RiskLevel = "<font color='LawnGreen'>low risk</font>";
        } ## end elsif ( $HighestScore >= ...)

        my $HTMLContents =
          CreateHTMLHeader( 'Executive Summary', "$PolicyName" );

        # Show some marketing fluff
        $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr>
  <td><b><i>Growing rate of attacks and threats:</i></b></td>
 </tr>
 <tr>
  <td>Internet attacks and vulnerabilities are coming at us faster than ever before and from a wide variety of sources.
      The rate of change for Internet attacks has far outpaced a traditional, casual patch management process.
      Frequent periodic testing and patching is essential.  Security must be a part of of the operational process.
  </td>
 </tr>
 <tr bgcolor='#c0c0c0'>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td><b><i>Regulatory compliance:</i></b></td>
 </tr>
 <tr>
  <td>GLBA, FFIEC, NCUA, SEC, HIPAA, SOX, PCI, Government Regulations (like FISMA) is forcing the need for more frequent
   &#8217;test and patch&#8217; strategies and tactics.
   Organizations may not have the time or information to address this problem.
   They do need &#8217;official, third party&#8217; professional security assistance to validate compliance.
   Regulatory pressure and Best Security Practices call for Annual Security Audits and periodic
    Vulnerability Assessments for the purpose of identifying and remediation of risks and security &#8217;gaps&#8217;
    that may exist in the Organization.
  </td>
 </tr>
 <tr bgcolor='#c0c0c0'>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td><b><i>Types of assessments:</i></b></td>
 </tr>
 <tr>
  <td>
   <ul>
    <li><i>External Vulnerability Assessment</i> simulates a &#8217;cracker&#8217; trying to penetrate your firewall and DMZ
    <li><i>Internal Vulnerability Assessment</i> simulates a &#8217;disgruntled employee&#8217; and attempts to discover vulnerabilities inside your firewall and on your LAN
    <li><a href='http://www.cisecurity.org/'><i>CIS</i> (Center for Internet Security)</a> Benchmarks provide a score from 0-100, grading your servers and databases for security administrative competence against the &#8217;best industry practices&#8217;
    <li><i>Social Engineering</i> attempts to gain access and vital knowledge by communicating with employees and business partners
    <li><i>Wireless Vulnerability Assessment</i> uses &#8217;drive-by and walk-by&#8217; attempts to gain access to private  wireless networks and assets, using modern wireless technology
    <li><i>Database and Web Application Assessment</i> determines security vulnerabilities at the data and software levels of your I/T infrastructure
   </ul>
  </td>
 </tr>
</table></center>\n

<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr bgcolor='#c0c0c0'>
  <td colspan='2' class='risk'>
      Using the risk assessment algorithm as explained in <a href='#Methodology'>Methodology</a> below,
      <a href='%%%COMPANY_URL%%%' target='_blank'>%%%COMPANY_NAME%%%</a>
      concluded that as of $ScanStart the overall risk of '$PolicyName' is $RiskLevel.
  </td>
 </tr>
 <tr bgcolor='#c0c0c0'>
  <td colspan='2'>Additional comments and annotations may be found in the attached
      <a href='Comments.html'>comments file</a> (if applicable).
  </td>
 </tr>
 <tr bgcolor='#c0c0c0'>
  <td colspan='2' class='risk'>&nbsp;</td>
 </tr>
HCT
        $HTMLContents .=
            "<tr><td colspan='2'><font color='DarkSlateGray'>"
          . "<b>Note</b>: Overall score: "
          . int( $HighestScore / 10 ) * 10
          . "</font></td></tr>\n"
          if ( length($HighestScore) );

        $HTMLContents .= <<HCT;
 <tr>
  <td colspan='2'>&nbsp;</td>
 </tr>
 <tr>
  <td colspan='2'><img src='.HostsPerLevel.jpg' alt='Hosts per level'></td>
 </tr>
 <tr>
  <td colspan='2'>&nbsp;</td>
 </tr>
 <tr>
  <td colspan='2'>A total of $ScannedHosts host(s) were scanned, of which $ResultHosts host(s) reported results:</td>
 </tr>
 <tr>
  <td colspan='2'>&nbsp;&nbsp;&nbsp;$HighHosts host(s) were rated critical (high)</td>
 </tr>
 <tr>
  <td colspan='2'>&nbsp;&nbsp;&nbsp;$MediumHosts host(s) were rated noteworthy (medium)</td>
 </tr>
 <tr>
  <td colspan='2'>&nbsp;&nbsp;&nbsp;$LowHosts host(s) were rated informational (low)</td>
 </tr>
 <tr>
  <td colspan='2'>&nbsp;&nbsp;&nbsp;$NoneHosts host(s) had no significant findings</td>
 </tr>
HCT
        $HTMLContents .=
          "<tr><td colspan='2'>&nbsp;&nbsp;&nbsp;$UnknownHosts host(s) had new or unknown findings</td></tr>\n"
          if ($UnknownHosts);

        $HTMLContents .=
          "<tr><td colspan='2'>The vulnerability scan started on '$ScanStart' and ended on "
          . "'"
          . time2str( str2time("$ScanEnd") )
          . "'</td></tr>\n"
          . "<tr bgcolor='#c0c0c0'><td colspan='2'>&nbsp;</td></tr>\n"
          . "</table></center>\n";

        $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr>
  <th colspan='2'>Top 10 host(s) with at least noteworthy (medium) findings</th>
 </tr>
HCT
        my $Top10 = 0;
        $sth = $dbh_Results->prepare(
            'SELECT Count(PluginID) FROM PluginInfoDetails WHERE HostIP = ? AND LocalScore >= ? AND LocalScore < ?'
        );

        my %HostHM = ( %HostHigh, %HostMedium );
        foreach my $HostIP (
            sort { $HostHM{$b} <=> $HostHM{$a} }
            keys %HostHM
          )
        {
            $sth->execute( $HostIP, 30, 40 );
            my ($PluginCount) = $sth->fetchrow_array;
            next unless ($PluginCount);
            $HTMLContents .= <<HCT;
 <tr>
  <td>&nbsp;&nbsp;$HostIP</td>
  <td>$PluginCount critical (high) findings</td>
 </tr>
HCT
            $Top10++;
            last if ( $Top10 >= 10 );
        } ## end foreach my $HostIP ( sort {...})
        if ( $Top10 < 10 )
        {
            foreach my $HostIP (
                sort { $HostHM{$b} <=> $HostHM{$a} }
                keys %HostHM
              )
            {
                $sth->execute( $HostIP, 20, 30 );
                my ($PluginCount) = $sth->fetchrow_array;
                next unless ($PluginCount);
                $HTMLContents .= <<HCT;
 <tr>
  <td>&nbsp;&nbsp;$HostIP</td>
  <td>$PluginCount noteworthy (medium) findings</td>
 </tr>
HCT
                $Top10++;
                last if ( $Top10 >= 10 );
            } ## end foreach my $HostIP ( sort {...})
        } ## end if ( $Top10 < 10 )

        $HTMLContents .= <<HCT;
 <tr bgcolor='#c0c0c0'>
  <td colspan='2'>&nbsp;</td>
 </tr>
 <tr>
  <th colspan='2'>Top 10 plugin(s) with at least noteworthy (medium) findings</th>
 </tr>
HCT
        $Top10 = 0;
        $sth   = $dbh_Results->prepare(
            'SELECT COUNT(HostIP) FROM HostPluginInfo WHERE PluginID = ?');
        foreach my $PluginID (
            sort { $PluginHigh{$b} <=> $PluginHigh{$a} }
            keys %PluginHigh
          )
        {
            $sth->execute($PluginID);
            my ($HostCount) = $sth->fetchrow_array();
            $HTMLContents .= <<HCT;
 <tr>
  <td>&nbsp;&nbsp;$PluginID</td>
  <td>$HostCount host(s) with critical (high) findings</td>
 </tr>
HCT
            $Top10++;
            last if ( $Top10 >= 10 );
        } ## end foreach my $PluginID ( sort...)
        if ( $Top10 < 10 )
        {
            foreach my $PluginID (
                sort { $PluginMedium{$b} <=> $PluginMedium{$a} }
                keys %PluginMedium
              )
            {
                $sth->execute($PluginID);
                my ($HostCount) = $sth->fetchrow_array();
                $HTMLContents .= <<HCT;
 <tr>
  <td>&nbsp;&nbsp;$PluginID</td>
  <td>$HostCount host(s) with noteworthy (medium) findings</td>
 </tr>
HCT
                $Top10++;
                last if ( $Top10 >= 10 );
            } ## end foreach my $PluginID ( sort...)
        } ## end if ( $Top10 < 10 )
        $HTMLContents .= <<HCT;
 <tr bgcolor='#c0c0c0'>
  <td colspan='2'>&nbsp;</td>
 </tr>
</table></center>

<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr>
  <td><a href='TechSummary.html' target='_blank'>Technical Summary</a></td>
 </tr>
 <tr>
  <td><a href='TechDetails.html' target='_blank'>Technical Details</a></td>
 </tr>
 <tr>
  <td><a href='Comments.html' target='_blank'>Additional Comments</a></td>
 </tr>
 <tr bgcolor='Black'>
  <td>&nbsp;</td>
 </tr>
</table></center>
HCT
        # Show some explanations for the methodology used
        $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr>
  <td><b><i><a name='Methodology'>Methodology</a>:</i></b></td>
 </tr>
 <tr>
  <td>External Network Vulnerability Assessments are conducted from a dedicated
      Vulnerability Testing appliance located in our Secure Data Center against
      public-facing devices located at the client's location.
      These non-disruptive methods include, but are not be limited to:
      port scans, OS and application fingerprinting, patch level checks, etc.
      The database of individual tests is updated regularly and currently over
      24,000 distinct tests are performed on each device.
  </td>
 </tr>
 <tr>
  <td>All External Vulnerabilities Assessments conducted by %%%COMPANY_NAME%%%
      are run under the assumption that anyone on the Internet will try to attack
      publicly  available servers and other network infrastructure &#8211; regardless of
      geographic or logical location.
  </td>
 </tr>
 <tr>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td>An Internal Network Vulnerability Assessment is conducted from a portable
      Vulnerability Testing appliance, temporarily located inside the client's network
      and performing the same checks as the External test.
  </td>
 </tr>
 <tr><
  <td>All Internal Vulnerabilities Assessments conducted by %%%COMPANY_NAME%%% are run under the
     assumption that a potential attacker has already gained access to the internal
     network(s) of a customer, be it via wireless access points, a laptop which was
     brought in, access to an unlocked workstation, exploit of an externally accessible
     device or similar.
  </td>
 </tr>
 <tr>
  <td>&nbsp;
  </td>
 </tr>
 <tr>
  <td><b><u>NOTE</u></b>:
  </td>
 </tr>
 <tr>
  <td>%%%COMPANY_NAME%%% does NOT attempt to exploit any vulnerabilities discovered during
      the course of Internal/External Vulnerability testing. All targets tested  are
      considered to be of equally critical importance. As such, verification of false
      positives, the content and sensitivity of data discovered, and the  criticality
      of applications and devices in relation to the target environment must be
      determined by the client in relation to said client's most recent risk assessment.
  </td>
 </tr>
 <tr>
  <td>&nbsp;
  </td>
 </tr>
 <tr>
  <td>Upon completion of testing, %%%COMPANY_NAME%%% examines the raw data and generates
      a report that identifies open ports, down-level operating systems and vulnerable
      applications on a per-device basis, classifying each vulnerability using a
      High-Medium-Low Vulnerability Rating Scale:
  </td>
 </tr>
 <tr>
  <td>To determine a host's risk, the vulnerability scanner uses port scans and port
      specific tests which are implemented as <i>plugins</i>.  Each of the plugins
      produces a finding.
      %%%COMPANY_NAME%%% has assigned a score to each plugin based on the context of
      an external or internal scan.
  </td>
 </tr>
 <tr>
  <td>The risk for an individual host is determined by the highest score assigned
  to any of the findings for that host.
  </td>
 </tr>
 <tr>
  <td>&nbsp;</td>
 </tr>
</table></center>

<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr>
  <td colspan='2'><u>Scores and associated risk</u>:</td>
 </tr>
 <tr>
  <td>30 &#8211; 39</td>
  <td><font color='Red'>$RiskText[30]</font></td>
 </tr>
 <tr>
  <td>20 &#8211; 29</td>
  <td><font color='#ff8040'>$RiskText[20]</font></td>
 </tr>
 <tr>
  <td>10 &#8211; 19</td>
  <td><font color='LawnGreen'>$RiskText[10]</font></td>
 </tr>
 <tr>
  <td>&nbsp;0 &#8211; &nbsp;9</td>
  <td>$RiskText[0]</td>
 </tr>
 <tr>
  <td colspan='2'>&nbsp;</td>
 </tr>
 <tr>
  <td colspan='2'>The overall risk is determined by the highest score for any of the hosts.
      If there are no high risk hosts, but 75% or greater of all hosts are medium,
      the overall risk rating will be high.
  </td>
 </tr>
 <tr>
  <td colspan='2'>&nbsp;</td>
 </tr>
 <tr>
  <td colspan='2'><u>Examples</u> (all assume 60 hosts have been scanned):</td>
 </tr>
 <tr>
  <td>At least one host show a <font color='Red'>high</font> risk:</td>
  <td>overall risk is <font color='Red'>$RiskText[30]</font></td>
 </tr>
 <tr>
  <td>47 of the hosts show <font color='#ff8040'>medium</font> risk, none shows <font color='Red'>high</font>:</td>
  <td>overall risk is <font color='Red'>$RiskText[30]</font> (75% rule!)</td>
 </tr>
 <tr>
  <td>At least one host shows <font color='#ff8040'>medium</font> risk, none shows <font color='Red'>high</font>:</td>
  <td>overall risk is <font color='#ff8040'>$RiskText[20]</font></td>
 </tr>
 <tr>
  <td>None of the hosts show <font color='#ff8040'>medium</font> or <font color='Red'>high</font> risk:</td>
  <td>overall risk is <font color='LawnGreen'>$RiskText[10]</font></td>
 </tr>
</table></center>

<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td>%%%COMPANY_NAME%%% presents the details and recommendations in several reports;
      an Executive Summary for Senior Management and both a Technical Summary and
      a Detailed Technical Document for the Technical Staff.
  </td>
 </tr>
</table></center>

<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr bgcolor='Black'>
  <td>&nbsp;</td>
 </tr>
</table></center>
$HTMLFooter
HCT

        # Replace placeholders in the HTML contents and footer
        ReplaceTemplateValues( \$HTMLContents );

        print SD "$HTMLContents";
        close(SD);
    } ## end if ( open( SD, '>', 'ExecSummary.html'...))
} ## end sub CreateExecutiveSummary($)

#--------------------------------------------------------------------
# Collect all summary data for a host
#--------------------------------------------------------------------
sub GetHostSummary ($)
{
    my ($HostID) = @_;

    # Get the plugins for this host and their scores for this host
    my $sth_plugin = $dbh_Results->prepare(
        'SELECT DISTINCT HostPluginInfo.PluginID,PluginInfoDetails.LocalScore FROM HostPluginInfo'
          . ' LEFT JOIN PluginInfoDetails ON PluginInfoDetails.PluginID = HostPluginInfo.PluginID'
          . ' WHERE HostPluginInfo.HostIP = ? AND PluginInfoDetails.LocalScore > 10'
          . ' ORDER BY PluginInfoDetails.LocalScore,PluginInfoDetails.Port DESC'
    );
    my @PluginList    = ();
    my @PluginScore   = ();
    my %PluginCounter = ();
    $sth_plugin->bind_param( 1, $HostID, SQL_CHAR );
    $sth_plugin->execute();

    while ( my $rowref = $sth_plugin->fetchrow_arrayref )
    {
        push( @PluginList,  $$rowref[0] );
        push( @PluginScore, $$rowref[1] );
    } ## end while ( my $rowref = $sth_plugin...)

    # Get the score for this host
    my $sth_score = $dbh_Results->prepare(
        'SELECT HighestScore,SumCritOrMedScore FROM HostInfo WHERE HostIP = ?'
    );
    $sth_score->bind_param( 1, $HostID, SQL_CHAR );
    $sth_score->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    my ( $HighestScore, $SumCritOrMedScore ) =
      $sth_score->fetchrow_array();

    my $Summary = <<EST;
<tr bgcolor='#c0c0c0'>
 <td colspan='2'>&nbsp;<td>
</tr>
<tr>
 <th colspan='2'>Host: <a name='H_$HostID'><font size='larger'>$HostID</font></a></th>
</tr>
<tr>
 <td valign='top'>Risk level:</td>
 <td><font color='$RiskColor[$HighestScore]'>$RiskText[$HighestScore]</font>
     (highest host score: $HighestScore, total host score: $SumCritOrMedScore)
 </td>
</tr>
<tr>
 <td valign='top'>Plugin(s):</td>
 <td>
EST

    my $PIndex = 0;

    my $sth_plugin_count = $dbh_Results->prepare(
        'SELECT COUNT(*) FROM HostPluginInfo WHERE HostIP = ? AND PluginID = ?'
    );
    foreach my $Score (@PluginScore)
    {
      SW_SCORE:
        {
            if ( $Score < 30 )
            {
                $Score =
                  "<font color='$RiskColor[$Score]'>medium ($Score)</font>";
                last SW_SCORE;
            } ## end if ( $Score < 30 )
            if ( $Score < 40 )
            {
                $Score =
                  "<font color='$RiskColor[$Score]'>high ($Score)</font>";
                last SW_SCORE;
            } ## end if ( $Score < 40 )
            $Score =
              "<font color='$RiskColor[$Score]'>undetermined/new ($Score)</font>";
        } ## end SW_SCORE:

        my $PluginID = $PluginList[$PIndex];

        # Get the number of plugin occurrences for this host
        $sth_plugin_count->bind_param( 1, $HostID,   SQL_CHAR );
        $sth_plugin_count->bind_param( 2, $PluginID, SQL_CHAR );
        $sth_plugin_count->execute();
        die "$DBI::errstr\n" if ( $dbh_Results->err() );
        my ($PluginCount) = $sth_plugin_count->fetchrow_array();
        $Summary .=
          "<a href='#P_$PluginID'>$PluginID</a> (score: $Score, found on $PluginCount port(s))<br>";
        $PIndex++;
    } ## end foreach my $Score (@PluginScore...)
    $Summary .= <<EST;
</td>
</tr>
EST
    # Replace placeholders in the Summary
    ReplaceTemplateValues( \$Summary );

    return ("$Summary");
} ## end sub GetHostSummary ($)

#--------------------------------------------------------------------
# Collect all summary data for a plugin
#--------------------------------------------------------------------
sub GetPluginSummary ($$$)
{
    my ( $PolicyName, $PluginID, $Score ) = @_;

    my $sth_plugin = $dbh_Results->prepare(
            'SELECT DISTINCT HostPluginInfo.HostIP FROM HostPluginInfo'
          . ' LEFT JOIN PluginInfoDetails ON PluginInfoDetails.PluginID = HostPluginInfo.PluginID'
          . ' WHERE HostPluginInfo.PluginID = ? AND PluginInfoDetails.LocalScore >= ?'
    );
    $sth_plugin->bind_param( 1, $PluginID, SQL_CHAR );
    $sth_plugin->bind_param( 2, $Score,    SQL_INTEGER );
    $sth_plugin->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    my @Hosts = ();
    while ( my $rowref = $sth_plugin->fetchrow_arrayref )
    {
        push( @Hosts, $$rowref[0] );
    } ## end while ( my $rowref = $sth_plugin...)

    my $SynopsisRef  = q{};
    my $SynopsisText = 'unknown';
    $sth_plugin = $dbh_Results->prepare(
        'SELECT synopsis FROM PluginInfoDetails WHERE PluginID = ?');
    $sth_plugin->execute($PluginID);
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($SynopsisRef) = $sth_plugin->fetchrow_arrayref();

    if ( ref($SynopsisRef) eq "ARRAY" )
    {
        $SynopsisText = join( "", @$SynopsisRef )
          if ( scalar @$SynopsisRef );
    } else
    {
        $SynopsisText = $SynopsisRef if ( length($SynopsisRef) );
    } ## end else [ if ( ref($SynopsisRef)...)]

    my $DescriptionRef  = q{};
    my $DescriptionText = 'unknown';
    $sth_plugin = $dbh_Results->prepare(
        'SELECT description FROM PluginInfoDetails WHERE PluginID = ?');
    $sth_plugin->execute($PluginID);
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($DescriptionRef) = $sth_plugin->fetchrow_arrayref();
    if ( ref($DescriptionRef) eq "ARRAY" )
    {
        $DescriptionText = join( "", @$DescriptionRef )
          if ( scalar @$DescriptionRef );
    } else
    {
        $DescriptionText = $DescriptionRef if ( length($DescriptionRef) );
    } ## end else [ if ( ref($DescriptionRef...))]

    my $SolutionRef  = q{};
    my $SolutionText = 'unknown';
    $sth_plugin = $dbh_Results->prepare(
        'SELECT solution FROM PluginInfoDetails WHERE PluginID = ?');
    $sth_plugin->execute($PluginID);
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($SolutionRef) = $sth_plugin->fetchrow_arrayref();
    if ( ref($SolutionRef) eq "ARRAY" )
    {
        $SolutionText = join( "", @$SolutionRef )
          if ( scalar @$SolutionRef );
    } else
    {
        $SolutionText = $SolutionRef if ( length($SolutionRef) );
    } ## end else [ if ( ref($SolutionRef)...)]

    my $CommentRef  = q{};
    my $CommentText = 'n/a';
    $sth_plugin = $dbh_Results->prepare(
        'SELECT Comment FROM PluginInfoDetails WHERE PluginID = ?');
    $sth_plugin->execute($PluginID);
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($CommentRef) = $sth_plugin->fetchrow_arrayref();

    if ( ref($CommentRef) eq "ARRAY" )
    {
        $CommentText = join( "", @$CommentRef )
          if ( scalar @$CommentRef );
    } else
    {
        $CommentText = $CommentRef if ( length($CommentRef) );
    } ## end else [ if ( ref($CommentRef) ...)]

    my $sth_host_count = $dbh_Results->prepare(
        'SELECT COUNT(*) FROM HostPluginInfo WHERE HostIP = ? AND PluginID = ?'
    );
    my $Summary = <<EST;
<tr bgcolor='#c0c0c0'>
 <td colspan='2'>&nbsp;<td>
</tr>
<tr>
 <th colspan='2'>Plugin: <a name='P_$PluginID'><font size='larger'>$PluginID</font></a></th>
</tr>
<tr>
 <td valign='top'>&#8217;$PolicyName&#8217; risk:</td>
 <td><font color='$RiskColor[$Score]'>$RiskText[$Score]</font> ($Score)</td>
</tr>
<tr>
 <td valign='top'>Synopis/Description:</td>
 <td><pre>
EST
    $Summary .=
        wrap( q{}, q{}, $SynopsisText )
      . "</pre>\n<pre>"
      . wrap( q{}, q{}, $DescriptionText )
      . "</pre></td>\n</tr>\n"
      . "<tr>\n <td valign='top'>Solution:</td>\n <td><pre>"
      . wrap( q{}, q{}, $SolutionText )
      . "</pre></td>\n</tr>\n"
      . "<tr>\n <td valign='top'>Auditor's comment:</td>\n <td><pre>"
      . wrap( q{}, q{}, $CommentText )
      . "</pre></td>\n</tr>\n"
      . "<tr>\n <td valign='top'>Host(s):</td>\n<td>";

    foreach my $HostID ( sort @Hosts )
    {

        # Get the number of host occurrences for this plugin
        $sth_host_count->bind_param( 1, $HostID,   SQL_CHAR );
        $sth_host_count->bind_param( 2, $PluginID, SQL_CHAR );
        $sth_host_count->execute();
        die "$DBI::errstr\n" if ( $dbh_Results->err() );
        my ($HostCount) = $sth_host_count->fetchrow_array();
        $Summary .=
          "<a href='#H_$HostID'>$HostID</a> (found on $HostCount port(s) on this host)<br>";
    } ## end foreach my $HostID ( sort @Hosts...)
    $Summary .= "</td>\n</tr>\n";

    # Replace placeholders in the Summary
    ReplaceTemplateValues( \$Summary );

    return ("$Summary");
} ## end sub GetPluginSummary ($$$)

#--------------------------------------------------------------------
# Create the technical summary
#--------------------------------------------------------------------
sub CreateTechnicalSummary($)
{
    my ($PolicyName) = @_;

    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . " => Create technical summary\n"
      if ($opt_v);

    if ( open( SD, '>', 'TechSummary.html' ) )
    {
        my $HTMLContents =
          CreateHTMLHeader( 'Technical Summary', "$PolicyName" );

        # Show list sorted by plugin in descending order of score
        my $sth = $dbh_Results->prepare(
            'SELECT COUNT(PluginID) FROM PluginInfoDetails WHERE LocalScore >= 20 AND LocalScore < 40 ORDER BY LocalScore'
        );
        $sth->execute();
        die "$DBI::errstr\n" if ( $dbh_Results->err() );
        my ($TotalPlugins) = $sth->fetchrow_arrayref();

        if ($TotalPlugins)
        {

            # Show all plugins with high risk level
            my $sth = $dbh_Results->prepare(
                'SELECT DISTINCT PluginID FROM PluginInfoDetails WHERE LocalScore >= 30 AND LocalScore < 40 ORDER BY PluginID'
            );
            $sth->execute();
            die "$DBI::errstr\n" if ( $dbh_Results->err() );
            my $sth_pc = $dbh_Results->prepare(
                'SELECT COUNT(PluginID) FROM PluginInfoDetails WHERE PluginID = ?'
            );
            my %HPlugins = ();

            while ( my $rowref = $sth->fetchrow_arrayref )
            {
                my $PluginID = $$rowref[0];
                $sth_pc->bind_param( 1, $PluginID, SQL_CHAR );
                $sth_pc->execute();
                die "$DBI::errstr\n" if ( $dbh_Results->err() );
                my $rowref_pc = $sth_pc->fetchrow_arrayref();
                $HPlugins{"$PluginID"} = $$rowref_pc[0];
            } ## end while ( my $rowref = $sth...)
            $HTMLContents .=
              "<center><table align='center' cellspacing='0' cellpadding='1' width='800'>\n";
            my @Plugins =
              sort { $HPlugins{$b} cmp $HPlugins{$a} } keys %HPlugins;
            foreach my $PluginID (@Plugins)
            {
                $HTMLContents .=
                  GetPluginSummary( "$PolicyName", $PluginID, 30 );
            } ## end foreach my $PluginID (@Plugins...)
            $HTMLContents .= "</table></center>";

            # Show all plugins with medium risk level
            $sth = $dbh_Results->prepare(
                'SELECT DISTINCT PluginID FROM PluginInfoDetails WHERE LocalScore >= 20 AND LocalScore < 30 ORDER BY PluginID'
            );
            $sth->execute();
            die "$DBI::errstr\n" if ( $dbh_Results->err() );
            %HPlugins = ();
            while ( my $rowref = $sth->fetchrow_arrayref )
            {
                my $PluginID = $$rowref[0];
                $sth_pc->bind_param( 1, $PluginID, SQL_CHAR );
                $sth_pc->execute();
                die "$DBI::errstr\n" if ( $dbh_Results->err() );
                my $rowref_pc = $sth_pc->fetchrow_arrayref();
                $HPlugins{"$PluginID"} = $$rowref_pc[0];
            } ## end while ( my $rowref = $sth...)
            $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='1' width='800'>
HCT
            @Plugins =
              sort { $HPlugins{$b} cmp $HPlugins{$a} } keys %HPlugins;
            foreach my $PluginID (@Plugins)
            {
                $HTMLContents .=
                  GetPluginSummary( "$PolicyName", $PluginID, 20 );
            } ## end foreach my $PluginID (@Plugins...)

            # Write a divider
            $HTMLContents .= <<HCT;
 <tr bgcolor='#585858'>
  <td style='color:white; text-align:center' colspan='2'>Hosts sorted by total score</td>
 </tr>
</table></center>
HCT
        } ## end if ($TotalPlugins)

        # Show list sorted by host in descending order of total score
        $sth = $dbh_Results->prepare(
            'SELECT DISTINCT HostIP,HighestScore FROM HostInfo ORDER BY HighestScore,SumCritOrMedScore DESC'
        );
        $sth->execute();
        die "$DBI::errstr\n" if ( $dbh_Results->err() );
        my @Hosts        = ();
        my %HighestScore = ();
        while ( my $rowref = $sth->fetchrow_arrayref )
        {
            my $HostID = "$$rowref[0]";
            push( @Hosts, "$HostID" );
            $HighestScore{"$HostID"} = $$rowref[1];
        } ## end while ( my $rowref = $sth...)

        if ( scalar @Hosts )
        {

            # Show all hosts with high score
            $HTMLContents .=
              "<center><table align='center' cellspacing='0' cellpadding='0' width='800'>\n";
            foreach my $HostID (@Hosts)
            {
                $HTMLContents .= GetHostSummary($HostID)
                  if ( ( $HighestScore{"$HostID"} >= 30 )
                    && ( $HighestScore{"$HostID"} < 40 ) );
            } ## end foreach my $HostID (@Hosts)
            $HTMLContents .= "</table></center>\n";

            # Show all hosts with medium score
            $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='1' width='800'>
HCT
            foreach my $HostID (@Hosts)
            {
                $HTMLContents .= GetHostSummary($HostID)
                  if ( ( $HighestScore{"$HostID"} >= 20 )
                    && ( $HighestScore{"$HostID"} < 30 ) );
            } ## end foreach my $HostID (@Hosts)
            $HTMLContents .= <<HCT;
</table></center>
HCT
        } ## end if ( scalar @Hosts )

        $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr bgcolor='#c0c0c0'>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td><a href='ExecSummary.html' target='_blank'>Executive Summary</a></td>
 </tr>
 <tr>
  <td><a href='TechDetails.html' target='_blank'>Technical Details</a></td>
 </tr>
 <tr>
  <td><a href='Comments.html' target='_blank'>Additional Comments</a></td>
 </tr>
 <tr bgcolor='Black'>
  <td>&nbsp;</td>
 </tr>
</table></center>
$HTMLFooter
HCT
        # Replace placeholders in the HTML contents and footer
        ReplaceTemplateValues( \$HTMLContents );

        print SD "$HTMLContents";

        close(SD);
    } ## end if ( open( SD, '>', 'TechSummary.html'...))
} ## end sub CreateTechnicalSummary($)

#--------------------------------------------------------------------
# Get a web reference for a TCP/UDP port
#--------------------------------------------------------------------
sub GetHREF($)
{
    my ($Protocol) = @_;

    $Protocol =~ s/\s*[\w|\-]+\s*\((\d+).*/$1/;

    # Either point to a port number specific or a generic WikiPedia page
    return (
        ( $Protocol =~ /^\d+$/ )
        ? "http://www.iss.net/security_center/advice/Exploits/Ports/$Protocol/default.htm"
        : 'http://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers'
    );
} ## end sub GetHREF($)

#--------------------------------------------------------------------
# Collect all data for a host
#--------------------------------------------------------------------
sub GetHostDetail($)
{
    my ($HostID) = @_;

    # Get the list of plugins and their scores for this host
    my $sth_plugin = $dbh_Results->prepare(
        'SELECT DISTINCT HostPluginInfo.PluginID,PluginInfoDetails.LocalScore FROM HostPluginInfo'
          . ' LEFT JOIN PluginInfoDetails ON PluginInfoDetails.PluginID = HostPluginInfo.PluginID'
          . ' WHERE HostPluginInfo.HostIP = ? ORDER BY PluginInfoDetails.LocalScore DESC'
    );
    $sth_plugin->execute($HostID);
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    my @PluginList  = ();
    my @PluginScore = ();
    $sth_plugin->bind_param( 1, $HostID, SQL_CHAR );
    $sth_plugin->execute();

    my $PIndex = 0;
    while ( my $rowref = $sth_plugin->fetchrow_arrayref )
    {
        $PluginList[$PIndex]  = "$$rowref[0]";
        $PluginScore[$PIndex] = $$rowref[1];
        $PIndex++;
    } ## end while ( my $rowref = $sth_plugin...)

    # Get the score for this host
    my $sth_score = $dbh_Results->prepare(
        'SELECT HighestScore FROM HostInfo WHERE HostIP = ?');
    $sth_score->bind_param( 1, $HostID, SQL_CHAR );
    $sth_score->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    my ($HighestScore) = $sth_score->fetchrow_array();

    # Get the start and end times for the scan for this host
    my $sth_ts = $dbh_Results->prepare(
        'SELECT ScanStart, ScanEnd FROM HostInfo WHERE HostIP = ?');
    $sth_ts->bind_param( 1, $HostID, SQL_CHAR );
    $sth_ts->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    my ( $ScanStart, $ScanEnd ) = $sth_ts->fetchrow_array();

    # Prepare some more queries
    my $sth_port_protocol = $dbh_Results->prepare(
        'SELECT DISTINCT Port,Protocol FROM HostPluginInfo WHERE HostIP = ? AND PluginID = ? ORDER BY Protocol'
    );
    die "$DBI::errstr\n" if ( $dbh_Results->err() );

    my @StandardPluginFields = qw (PluginID PluginName PluginFamily);
    my $sth_result_standard =
      $dbh_Results->prepare( 'SELECT '
          . join( ",", @StandardPluginFields )
          . ' FROM PluginInfo WHERE PluginID = ?' );

    #    $sth_result_standard->bind_param( 1, $PluginID, SQL_CHAR );
    #    $sth_result_standard->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );

    my $sth_result_extra =
      $dbh_Results->prepare( 'SELECT '
          . join( ",", @ExtraPluginFields )
          . ' FROM PluginInfoDetails WHERE PluginID = ? AND Port = ? AND HostIP = ?'
      );
    die "$DBI::errstr\n" if ( $dbh_Results->err() );

    my $ScanEnd_String = time2str( str2time("$ScanEnd") );
    my $Detail         = <<EDT;
 <tr bgcolor='#c0c0c0'>
  <td colspan='2'>&nbsp;<td>
 </tr>
 <tr>
  <th colspan='2'>Host: <a name='H_$HostID'><font size='larger'>$HostID</font></a></th>
 </tr>
 <tr>
  <td style='width:15%' valign='top'>Host score:</td>
  <td>$HighestScore</td>
 </tr>
 <tr>
  <td style='width:15%' valign='top'>Risk level:</td>
  <td><font color='$RiskColor[$HighestScore]'>$RiskText[$HighestScore] ($HighestScore)</font></td>
 </tr>
 <tr>
  <td style='width:15%' valign='top'>Scan start:</td>
  <td>$ScanStart</td>
 </tr>
 <tr>
  <td style='width:15%' valign='top'>Scan end:</td>
  <td>$ScanEnd_String</td>
 </tr>
 <tr>
  <td style='width:15%' valign='top'>Plugin(s):</td>
  <td>&nbsp;</td>
 </tr>
 <tr><td colspan='2'>
EDT

    $PIndex = 0;
    my %HPPDone = ();
    while ( $PIndex < scalar @PluginList )
    {

        my $PluginID = $PluginList[$PIndex];
        my $Score    = $PluginScore[$PIndex];
        if ( defined $Score )
        {
          SW_SCORE:
            {
                if ( $Score < 10 )
                {
                    $Score =
                      "<font color='$RiskColor[$Score]'>none ($Score)</font>";
                    last SW_SCORE;
                } ## end if ( $Score < 10 )
                if ( $Score < 20 )
                {
                    $Score =
                      "<font color='$RiskColor[$Score]'>low ($Score)</font>";
                    last SW_SCORE;
                } ## end if ( $Score < 20 )
                if ( $Score < 30 )
                {
                    $Score =
                      "<font color='$RiskColor[$Score]'>medium ($Score)</font>";
                    last SW_SCORE;
                } ## end if ( $Score < 30 )
                if ( $Score < 40 )
                {
                    $Score =
                      "<font color='$RiskColor[$Score]'>high ($Score)</font>";
                    last SW_SCORE;
                } ## end if ( $Score < 40 )
                $Score =
                  "<font color='$RiskColor[$Score]'>undetermined/new ($Score)</font>";
            } ## end SW_SCORE:
        } else
        {
            $Score =
              "<font color='$RiskColor[$Score]'>undetermined/new</font>";
        } ## end else [ if ( defined $Score ) ]

        # Get all protocols for this host and pluginID
        my @Protocols = ();
        my @Ports     = ();
        $sth_port_protocol->bind_param( 1, $HostID,   SQL_CHAR );
        $sth_port_protocol->bind_param( 2, $PluginID, SQL_CHAR );
        $sth_port_protocol->execute();

        my $PortIndex = 0;
        while ( my $rowref = $sth_port_protocol->fetchrow_arrayref )
        {
            $Ports[$PortIndex]     = $$rowref[0];
            $Protocols[$PortIndex] = $$rowref[1];
            $PortIndex++;
        } ## end while ( my $rowref = $sth_port_protocol...)

        $PortIndex = 0;
        while ( $PortIndex < scalar @Ports )
        {
            my $Protocol = $Protocols[$PortIndex];

            # Get the port for this
            my $Port = $Ports[$PortIndex];
            $PortIndex++;

            next
              if (
                defined $HPPDone{ "$HostID"
                      . "$PluginID"
                      . "$Protocol"
                      . "$Port" } );

            # Prepare the link for the protocol
            my $HREF = GetHREF($Protocol);

            # Get the actual results for this host
            $sth_result_standard->bind_param( 1, $PluginID, SQL_CHAR );
            $sth_result_standard->execute();
            die "$DBI::errstr\n" if ( $dbh_Results->err() );

            my %FieldContents = ();
            while ( my $hashref = $sth_result_standard->fetchrow_hashref )
            {

                foreach my $FieldName (@StandardPluginFields)
                {

                    # Convert each field contents into a string
                    my $FieldText = q{};
                    if ( defined $hashref->{$FieldName} )
                    {
                        if ( ref( $hashref->{$FieldName} ) eq "ARRAY" )
                        {
                            $FieldText =
                              join( "", @{ $hashref->{$FieldName} } );
                        } else
                        {
                            $FieldText = $hashref->{$FieldName};
                        } ## end else [ if ( ref( $hashref->{$FieldName...}))]
                    } ## end if ( defined $hashref->...)
                    $FieldContents{$FieldName} =
                      length("$FieldText") ? "$FieldText" : 'n/a';

                    # Replace artificial delimiter with ","
                    $FieldContents{"$FieldName"} =~ s/$ValueSep/, /g;
                } ## end foreach my $FieldName (@StandardPluginFields...)
            } ## end while ( my $hashref = $sth_result_standard...)

            $sth_result_extra->bind_param( 1, $PluginID, SQL_CHAR );
            $sth_result_extra->bind_param( 2, $Port,     SQL_CHAR );
            $sth_result_extra->bind_param( 3, $HostID,   SQL_CHAR );
            $sth_result_extra->execute();
            die "$DBI::errstr\n" if ( $dbh_Results->err() );

            while ( my $hashref = $sth_result_extra->fetchrow_hashref )
            {
                foreach my $FieldName (@ExtraPluginFields)
                {

                    # Convert each field contents into a string
                    my $FieldText = q{};
                    if ( defined $hashref->{$FieldName} )
                    {
                        if ( ref( $hashref->{$FieldName} ) eq "ARRAY" )
                        {
                            $FieldText =
                              join( "", @{ $hashref->{$FieldName} } );
                        } else
                        {
                            $FieldText = $hashref->{$FieldName};
                        } ## end else [ if ( ref( $hashref->{$FieldName...}))]
                    } ## end if ( defined $hashref->...)
                    $FieldContents{$FieldName} =
                      length("$FieldText") ? "$FieldText" : 'n/a';

                    # Replace artificial delimiter with ","
                    $FieldContents{"$FieldName"} =~ s/$ValueSep/, /g;
                } ## end foreach my $FieldName (@ExtraPluginFields...)
            } ## end while ( my $hashref = $sth_result_extra...)

            $Detail .= <<EDT;
<center><table cellspacing='0' cellpadding='1' width='800' border='1' style='table-layout:fixed'>
 <tr>
  <td style='width:20%;border-top:solid 1px;border-bottom:dotted 1px;border-right:dotted 1px;' valign='top'><i>Plugin ID:</i></td>
  <td style='width:80%;border-top:solid 1px;border-bottom:dotted 1px;border-left:dotted 1px;'><b>$PluginID</b> (Host: $HostID)</td>
 </tr>
 <tr>
  <td style='width:20%;border-top:dotted 1px;border-bottom:dotted 1px;border-right:dotted 1px;' valign='top'><i>%%%COMPANY_NAME%%% score:</i></td>
  <td style='width:80%;border-top:dotted 1px;border-bottom:dotted 1px;border-left:dotted 1px;'>$Score</td>
 </tr>
 <tr>
  <td style='width:20%;border-top:dotted 1px;border-bottom:dotted 1px;border-right:dotted 1px;' valign='top'><i>Protocol/Port:</i></td>
  <td style='width:80%;border-top:dotted 1px;border-bottom:dotted 1px;border-left:dotted 1px;'><a href='$HREF' target='_blank'>$Protocol</a>/$Port</td>
 </tr>
 <tr>
  <td style='width:20%;border-top:dotted 1px;border-bottom:dotted 1px;border-right:dotted 1px;' valign='top'><i>Plugin name:</i></td>
  <td style='width:80%;border-top:dotted 1px;border-bottom:dotted 1px;border-left:dotted 1px;'>$FieldContents{'PluginName'}</td>
 </tr>
 <tr>
  <td style='width:20%;border-top:dotted 1px;border-bottom:dotted 1px;border-right:dotted 1px;' valign='top'><i>Plugin family:</i></td>
  <td style='width:80%;border-top:dotted 1px;border-bottom:dotted 1px;border-left:dotted 1px;'>$FieldContents{'PluginFamily'}</td>
 </tr>
 <tr>
  <td style='width:20%;border-top:dotted 1px;border-bottom:dotted 1px;border-right:dotted 1px;' valign='top'><i>Synopsis:</i></td>
  <td style='width:80%;border-top:dotted 1px;border-bottom:dotted 1px;border-left:dotted 1px;'><pre>$FieldContents{'synopsis'}</pre></td>
 </tr>
 <tr>
  <td style='width:20%;border-top:dotted 1px;border-bottom:dotted 1px;border-right:dotted 1px;' valign='top'><i>Solution:</i></td>
  <td style='width:80%;border-top:dotted 1px;border-bottom:dotted 1px;border-left:dotted 1px;'><pre>$FieldContents{'solution'}</pre></td>
 </tr>
 <tr>
  <td style='width:20%;border-top:dotted 1px;border-bottom:dotted 1px;border-right:dotted 1px;' valign='top'><i>Auditor's comment:</i></td>
  <td style='width:80%;border-top:dotted 1px;border-bottom:dotted 1px;border-left:dotted 1px;'><pre>$FieldContents{'Comment'}</pre></td>
 </tr>
 <tr>
  <td style='width:20%;border-top:dotted 1px;border-bottom:dotted 1px;border-right:dotted 1px;' valign='top'><i>See also:</i></td>
  <td style='width:80%;border-top:dotted 1px;border-bottom:dotted 1px;border-left:dotted 1px;'><pre>$FieldContents{'see_also'}</pre></td>
 </tr>
 <tr>
  <td style='width:20%;border-top:dotted 1px;border-bottom:dotted 1px;border-right:dotted 1px;' valign='top'><i>Plugin output:</i></td>
  <td style='width:80%;border-top:dotted 1px;border-bottom:dotted 1px;border-left:dotted 1px;'><pre>$FieldContents{'plugin_output'}</pre></td>
 </tr>
EDT

            my $AddlDetails = q{};
            foreach
              my $FieldName ( @StandardPluginFields, @ExtraPluginFields )
            {
                next
                  if ( $FieldName =~
                    /Plugin(?:ID|Name|Family)|synopsis|solution|Comment|see_also|plugin_output/o
                  );

                # Convert field names into human readable strings
                #  and HTTP links where appropriate
                my $HumanReadableFN = "$FieldName";
                my $HREF            = q{};
              SWITCH_FN:
                {
                    if ( "$FieldName" =~ /description/o )
                    {
                        $HumanReadableFN = 'Description';
                        last SWITCH_FN;
                    } ## end if ( "$FieldName" =~ /description/o...)
                    if ( "$FieldName" =~ /xref/o )
                    {
                        $HumanReadableFN = 'Cross reference';

                        foreach my $XREF (
                            split( /,/, $FieldContents{"$FieldName"} ) )
                        {
                            if ( $XREF =~ /OSVDB:(\d+)/o )
                            {
                                $HREF .=
                                  "<a href='http://osvdb.org/show/osvdb/$1' target='_blank'>http://osvdb.org/show/osvdb/$1</a>, ";
                            } ## end if ( $XREF =~ /OSVDB:(\d+)/o...)
                            if ( $XREF =~ /CWE:(\d+)/o )
                            {
                                $HREF .=
                                  "<a href='http://cwe.mitre.org/data/definitions/$1.html' target='_blank'>http://cwe.mitre.org/data/definitions/$1.html</a>, ";
                            } ## end if ( $XREF =~ /CWE:(\d+)/o...)
                        } ## end foreach my $XREF ( split( /,/...))
                        last SWITCH_FN;
                    } ## end if ( "$FieldName" =~ /xref/o...)
                    if ( "$FieldName" =~ /bid/o )
                    {
                        $HumanReadableFN = 'Bugtraq ID';
                        last SWITCH_FN;
                    } ## end if ( "$FieldName" =~ /bid/o...)
                    if ( "$FieldName" =~ /vuln_publication_date/o )
                    {
                        $HumanReadableFN =
                          'Vulnerability publication date';
                        last SWITCH_FN;
                    } ## end if ( "$FieldName" =~ /vuln_publication_date/o...)
                    if ( "$FieldName" =~ /cvss_vector/o )
                    {
                        $HumanReadableFN =
                          'Common Vulnerability Scoring System Vector';
                        $HREF =
                          "<a href='http://www.first.org/cvss/cvss-guide.html' target='_blank'>http://www.first.org/cvss/cvss-guide.html</a>"
                          unless (
                            $FieldContents{"$FieldName"} =~ /n\/a/o );
                        last SWITCH_FN;
                    } ## end if ( "$FieldName" =~ /cvss_vector/o...)
                    if ( "$FieldName" =~ /cvss_base_score/o )
                    {
                        $HumanReadableFN =
                          'Common Vulnerability Scoring System Base Score';
                        $HREF =
                          "<a href='http://www.first.org/cvss/cvss-guide.html' target='_blank'>http://www.first.org/cvss/cvss-guide.html</a>"
                          unless (
                            $FieldContents{"$FieldName"} =~ /n\/a/o );
                        last SWITCH_FN;
                    } ## end if ( "$FieldName" =~ /cvss_base_score/o...)
                    if ( "$FieldName" =~ /cve/o )
                    {
                        $HumanReadableFN =
                          'Common Vulnerability and Exposures (CVE)';
                        foreach my $CVE (
                            split( /,/, $FieldContents{"$FieldName"} ) )
                        {
                            if ( $CVE =~ /(CVE-\d+-\d+)/o )
                            {
                                $HREF .=
                                  "<a href='https://cve.mitre.org/cgi-bin/cvename.cgi?name=$1' target='_blank'>https://cve.mitre.org/cgi-bin/cvename.cgi?name=$1</a>, ";
                            } ## end if ( $CVE =~ /(CVE-\d+-\d+)/o...)
                        } ## end foreach my $CVE ( split( /,/...))
                        last SWITCH_FN;
                    } ## end if ( "$FieldName" =~ /cve/o...)
                    if ( "$FieldName" =~ /patch_publication_date/o )
                    {
                        $HumanReadableFN = 'Patch publication date';
                        last SWITCH_FN;
                    } ## end if ( "$FieldName" =~ ...)
                } ## end SWITCH_FN:
                $HREF =~ s/,\s*$//;

                $AddlDetails .= "<u>$HumanReadableFN</u><br>"
                  . $FieldContents{"$FieldName"};
                $AddlDetails .= " ($HREF)" if ( length($HREF) );
                $AddlDetails .= "\n";
            } ## end foreach my $FieldName ( @StandardPluginFields...)
            $Detail .=
              "<tr><td style='width:20%;border-top:dotted 1px;border-bottom:solid 1px;border-right:dotted 1px;' valign='top'><i>Additional details:</i></td>"
              . " <td style='width:80%;border-top:dotted 1px;border-bottom:solid 1px;border-left:dotted 1px;'><pre>$AddlDetails</pre></td></tr>\n</table>\n"
              if ( length($AddlDetails) );

            $HPPDone{ "$HostID" . "$PluginID" . "$Protocol" . "$Port" }++;
        } ## end while ( $PortIndex < scalar...)
        $PIndex++;
    } ## end while ( $PIndex < scalar ...)
    $Detail .= "</td></tr>\n";

    # Replace placeholders in the details
    ReplaceTemplateValues( \$Detail );

    return ("$Detail");
} ## end sub GetHostDetail($)

#--------------------------------------------------------------------
# Create the detailed report
#--------------------------------------------------------------------
sub CreateDetailedReport($)
{
    my ($PolicyName) = @_;

    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . " => Create detailed report\n"
      if ($opt_v);

    my $sth =
      $dbh_Results->prepare('SELECT DISTINCT HostIP FROM HostInfo');
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    my @Hosts = ();
    while ( my $rowref = $sth->fetchrow_arrayref )
    {
        push( @Hosts, $$rowref[0] );
    } ## end while ( my $rowref = $sth...)

    if ( open( SD, '>', 'TechDetails.html' ) )
    {
        my $HTMLContents =
          CreateHTMLHeader( 'Technical Details', "$PolicyName" );

        if ( scalar @Hosts )
        {
            my $sth_score = $dbh_Results->prepare(
                'SELECT HighestScore FROM HostInfo WHERE HostIP = ?');

            # See: http://www.perlmonks.org/?node_id=22432
            my @SortedByIP =
              map substr( $_, 4 ) => sort
              map pack( 'C4' => /(\d+)\.(\d+)\.(\d+)\.(\d+)/ )
              . $_ => @Hosts;

            # Show all hosts with just their total and highest score
            $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
HCT
            foreach my $HostID (@SortedByIP)
            {

                $sth_score->bind_param( 1, $HostID, SQL_CHAR );
                $sth_score->execute();
                die "$DBI::errstr\n" if ( $dbh_Results->err() );
                my ($HighestScore) = $sth_score->fetchrow_array();

                $HTMLContents .= <<HCT;
 <tr>
  <td><a href='#H_$HostID'>$HostID</a></td>
  <td>Host score: $HighestScore</td>
  <td>Risk level: <font color='$RiskColor[$HighestScore]'>$RiskText[$HighestScore] ($HighestScore)</font></td>
 </tr>
HCT
            } ## end foreach my $HostID (@SortedByIP...)
            $HTMLContents .= <<HCT;
</table></center>
HCT

            # Show all hosts with all possible details
            $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
HCT
            foreach my $HostID (@SortedByIP)
            {
                $HTMLContents .= GetHostDetail($HostID);
            } ## end foreach my $HostID (@SortedByIP...)
            $HTMLContents .= <<HCT;
</table></center>
HCT
        } ## end if ( scalar @Hosts )

        $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr bgcolor='#c0c0c0'><td>&nbsp;</td>
 </tr>
 <tr>
  <td><a href='ExecSummary.html' target='_blank'>Executive Summary</a></td>
 </tr>
 <tr>
  <td><a href='TechSummary.html' target='_blank'>Technical Summary</a></td>
 </tr>
 <tr>
  <td><a href='Comments.html' target='_blank'>Additional Comments</a></td>
 </tr>
 <tr bgcolor='Black'>
  <td>&nbsp;</td>
 </tr>
</table></center>
$HTMLFooter
HCT

        # Replace placeholders in the HTML contents and footer
        ReplaceTemplateValues( \$HTMLContents );

        print SD "$HTMLContents";

        close(SD);
    } ## end if ( open( SD, '>', 'TechDetails.html'...))
} ## end sub CreateDetailedReport($)

#--------------------------------------------------------------------
# Create an empty comments file
#--------------------------------------------------------------------
sub CreateComments($)
{
    my ($PolicyName) = @_;

    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . " => Create comments file\n"
      if ($opt_v);

    # Read in the comments file if it is not empty
    if ( -s "$CommentsFile" )
    {
        if ( open( CF, '<', "$CommentsFile" ) )
        {
            while (<CF>)
            {
                chomp;
                push( @Comments, "$_" );
            } ## end while (<CF>)
            close(CF);
        } ## end if ( open( CF, '<', "$CommentsFile"...))
    } ## end if ( -s "$CommentsFile"...)

    # Create the comments file
    if ( open( SD, '>', 'Comments.html' ) )
    {
        my $HTMLContents =
          CreateHTMLHeader( 'Additional Comments', "$PolicyName" );

        my $Comments_String = join( "\n", @Comments );
        $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr>
  <td><pre>$Comments_String</pre></td>
 </tr>
</table></center>

<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr bgcolor='#c0c0c0'><td>&nbsp;</td>
 </tr>
 <tr>
  <td><a href='ExecSummary.html' target='_blank'>Executive Summary</a></td>
 </tr>
 <tr>
  <td><a href='TechDetails.html' target='_blank'>Technical Details</a></td>
 </tr>
 <tr>
  <td><a href='TechSummary.html' target='_blank'>Technical Summary</a></td>
 </tr>
 <tr bgcolor='Black'>
  <td>&nbsp;</td>
 </tr>
</table></center>
$HTMLFooter
HCT

        # Replace placeholders in the HTML contents and footer
        ReplaceTemplateValues( \$HTMLContents );

        print SD "$HTMLContents";

        close(SD);
    } ## end if ( open( SD, '>', 'Comments.html'...))
} ## end sub CreateComments($)

#--------------------------------------------------------------------
# Create the cover page
#--------------------------------------------------------------------
sub CreateCoverPage($)
{
    my ($PolicyName) = @_;

    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . " => Create cover page\n"
      if ($opt_v);

    if ( open( SD, '>', 'CoverPage.html' ) )
    {
        my $HTMLContents = CreateHTMLHeader( 'Cover Page', "$PolicyName" );

        # Get the end of the scan
        $sth = $dbh_Results->prepare('SELECT MAX(ScanEnd) FROM HostInfo');
        $sth->execute();
        die "$DBI::errstr\n" if ( $dbh_Results->err() );
        my ($ScanEnd) = $sth->fetchrow_array();

        # Get the customer name and scanned IP addresses
        $sth = $dbh_Results->prepare(
            'SELECT CustomerName,ScannedIPs FROM GeneralInfo');
        $sth->execute();
        die "$DBI::errstr\n" if ( $dbh_Results->err() );
        my ( $CustomerName, $ScannedHosts ) = $sth->fetchrow_array();

        # The actual cover page
        my $Now = scalar localtime();
        $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr>
  <td height='50'>&nbsp;</td>
 </tr>
 <tr>
  <td align='center'><img src='$LogoFile{large}' alt='%%%COMPANY_NAME%%%'></td>
 </tr>
 <tr>
  <td height='50'>&nbsp;</td>
 </tr>
 <tr>
  <td align='center' height='30'><font size='5'>$CustomerName</font></td>
 </tr>
 <tr>
  <td align='center' height='30'><font size='4'><i>$PolicyName</i></font></td>
 </tr>
 <tr>
  <td align='center'>$Now</td>
 </tr>
 <tr>
  <td height='50'>&nbsp;</td>
 </tr>
</table></center>
<!-- NEW PAGE -->
HCT

        # The list of IP addresses, intended audience etc.
        my $ScanEnd_String = time2str( str2time("$ScanEnd") );
        $HTMLContents .= <<HCT;
<center><table align='center' cellspacing='0' cellpadding='0' width='800'>
 <tr>
  <td><h2>Background</h2></td>
 </tr>
 <tr>
  <td>$CustomerName contracted with %%%COMPANY_NAME%%%, Inc. to perform '$PolicyName'.</td>
 </tr>
 <tr>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td>This was done using %%%COMPANY_NAME%%%'s Vulnerability Testing tool and performed
      under the guidance of a certified security professional %%%CISSP%%%. It ended on
      $ScanEnd_String
  </td>
 </tr>
 <tr>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td><h2>Audience</h2></td>
 </tr>
 <tr>
  <td>
   <ul>
    <li>CIO, CSO
    <li>CEO, board of directors
    <li>Corporate auditor
   </ul>
  </td>
 </tr>
 <tr>
  <td>&nbsp;</td>
 </tr>
 <tr>
  <td><h2>Target Environment</h2></td>
 </tr>
 <tr>
  <td>%%%COMPANY_NAME%%% tried to find all services of all devices using the IP addresses below:<br>
   <ul>
HCT
        $HTMLContents .=
          '    <li>' . join( "\n<li>", split( /,/, $ScannedHosts ) );
        $HTMLContents .= <<HCT;

   </ul>
  </td>
 </tr>
</table></center>
$HTMLFooter
HCT
        # Replace placeholders in the HTML contents and footer
        ReplaceTemplateValues( \$HTMLContents );

        print SD "$HTMLContents";
        close(SD);
    } ## end if ( open( SD, '>', 'CoverPage.html'...))
} ## end sub CreateCoverPage($)

#--------------------------------------------------------------------
# Create new logos
#--------------------------------------------------------------------
sub CreateLogos()
{
    foreach my $LogoSize ( 'small', 'large' )
    {

        # Deal with specific logos as per the external config file
        if ( $ExternalConfig->exists( 'LogoFile', "$LogoSize" ) )
        {
            my $LogoFile =
              trim( $ExternalConfig->val( 'LogoFile', "$LogoSize" ) );
            if ( -s "$LogoFile" )
            {
                if ( open( IN, '<', "$LogoFile" ) )
                {
                    binmode(IN);

                    if ( open( OUT, '>', $LogoFile{"$LogoSize"} ) )
                    {
                        binmode(OUT);

                        # Copy in chunks of max. 64KB
                        my $blksize = ( stat IN )[11] || 65536;
                        my ( $buf, $len, $written, $offset );
                        while ( $len = sysread IN, $buf, $blksize )
                        {
                            if ( !defined $len )
                            {
                                next if $! =~ /^Interrupted/o;  # ^Z and fg
                                die "System read error: $!\n";
                            } ## end if ( !defined $len )
                            my $offset = 0;

                            while ($len)
                            {    # Handle partial writes.
                                defined( $written = syswrite OUT,
                                    $buf, $len, $offset )
                                  or die "System write error: $!\n";
                                $len -= $written;
                                $offset += $written;
                            } ## end while ($len)
                        } ## end while ( $len = sysread IN...)

                        close(OUT);
                    } ## end if ( open( OUT, '>', $LogoFile...))

                    close(IN);
                } ## end if ( open( IN, '<', "$LogoFile"...))

                # The logo file is present -> process the next one
                next;
            } else
            {
                warn "WARNING: Specific logo file '$LogoFile' not found\n";
            } ## end else [ if ( -s "$LogoFile" ) ]
        } ## end if ( $ExternalConfig->exists...)

        # The logo file is not yet present
        #  read it from the "DATA" section
        #  at the bottom of the this script
        if ( open( PIC, '> ', $LogoFile{"$LogoSize"} ) )
        {
            warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
              . " => Writing "
              . $LogoFile{"$LogoSize"} . "\n"
              if ($opt_v);
            binmode PIC;
            my $HaveData = 0;
            my $InUU     = 0;

            # Always start from the top of the "DATA" section
            seek( DATA, 0, 0 );
            while (<DATA>)
            {
                if ($InUU)
                {
                    last if (/^end logo_$LogoSize/);
                    $HaveData++;
                    my $block = unpack( "u", $_ );
                    print PIC $block;
                } else
                {
                    $InUU++ if (/^begin logo_$LogoSize/);
                } ## end else [ if ($InUU) ]
            } ## end while (<DATA>)
            close(PIC);

            warn "WARNING: No data to create '"
              . $LogoFile{"$LogoSize"} . "'\n"
              unless ($HaveData);
        } else
        {
            warn "WARNING: Can not create '"
              . $LogoFile{"$LogoSize"} . "'\n";
        } ## end else [ if ( open( PIC, '> ', ...))]
    } ## end foreach my $LogoSize ( 'small'...)
} ## end sub CreateLogos

#--------------------------------------------------------------------
# Create the output files
#--------------------------------------------------------------------
sub CreateOutput()
{
    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . " => Creating output files\n"
      if ($opt_v);

    # Create the logos
    CreateLogos();

    # Assign risk level description to scores
    for ( 0 .. 9 )
    {
        $RiskText[$_]  = 'none';
        $RiskColor[$_] = 'Black';
    } ## end for ( 0 .. 9 )
    for ( 10 .. 19 )
    {
        $RiskText[$_]  = 'low';
        $RiskColor[$_] = 'LawnGreen';
    } ## end for ( 10 .. 19 )
    for ( 20 .. 29 )
    {
        $RiskText[$_]  = 'noteworthy (medium)';
        $RiskColor[$_] = '#ff8040';
    } ## end for ( 20 .. 29 )
    for ( 30 .. 39 )
    {
        $RiskText[$_]  = 'critical (high)';
        $RiskColor[$_] = 'Red';
    } ## end for ( 30 .. 39 )
    $RiskText[99]  = 'unknown or new';
    $RiskColor[99] = 'Black';

    # Get the policy name for this scan
    ConnectResultsDB();
    my $PolicyName = 0;
    $sth = $dbh_Results->prepare('SELECT PolicyName FROM GeneralInfo');
    $sth->execute();
    die "$DBI::errstr\n" if ( $dbh_Results->err() );
    ($PolicyName) = $sth->fetchrow_array();

    # Create comments
    CreateComments("$PolicyName");

    # Create an executive summary
    CreateExecutiveSummary("$PolicyName");

    # Create a technical summary
    CreateTechnicalSummary("$PolicyName");
    if ( -s 'TechSummary.html' )
    {
        if ( ( -x '/bin/lynx' ) || ( -x '/usr/bin/lynx' ) )
        {
            if ( ( -x '/bin/sed' ) || ( -x '/usr/bin/sed' ) )
            {
                system(
                    "lynx -dump TechSummary.html | sed '/Hosts sorted by total score/,\$d' > Remediation.txt"
                );
            } else
            {
                warn "Please install 'sed' to create 'Remediation.txt'\n";
            } ## end else [ if ( ( -x '/bin/sed' )...)]
        } else
        {
            warn "Please install 'lynx' to create 'Remediation.txt'\n";
        } ## end else [ if ( ( -x '/bin/lynx' ...))]
    } ## end if ( -s 'TechSummary.html'...)

    # Create technical details
    CreateDetailedReport("$PolicyName");

    # Create the cover page
    CreateCoverPage("$PolicyName");

    if ( -x '/usr/bin/htmldoc' )
    {
        warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
          . " => Creating PDF file\n"
          if ($opt_v);

        # Create a PDF combining the cover page, executive summary
        #  and comments (if present)
        system( '/usr/bin/htmldoc --webpage --quiet -t pdf '
              . '--size letter --right 1in --left 1in '
              . '--header ... --footer ... --browserwidth 800 '
              . ( ( -s 'CoverPage.html' )   ? 'CoverPage.html '   : q{} )
              . ( ( -s 'ExecSummary.html' ) ? 'ExecSummary.html ' : q{} )
              . ( ( -s 'Comments.html' )    ? 'Comments.html '    : q{} )
              . '> ExecutiveReport.pdf' );
        warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
          . " => Done\n"
          if ($opt_v);
    } else
    {
        warn "Please install 'htmldoc' to create 'ExecutiveReport.pdf'\n";
    } ## end else [ if ( -x '/usr/bin/htmldoc'...)]

    # Create a zip files with all the necessary files
    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . " => Creating zip file\n"
      if ($opt_v);
    CreateZipFile("$PolicyName");
    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime ) . " => Done\n"
      if ($opt_v);

    # Update the phase
    $sth = $dbh_Results->prepare('SELECT PhaseDone From GeneralInfo');
    $sth->execute();
    die "Get phase in 'o': $DBI::errstr\n"
      if ( $dbh_Results->err() );
    ($PhaseDone) = $sth->fetchrow_array;
    $sth->finish;
    unless ( $PhaseDone =~ /o/ )
    {
        $PhaseDone .= 'o';
        $sth_update =
          $dbh_Results->prepare('UPDATE GeneralInfo SET PhaseDone = ?');
        $sth_update->execute($PhaseDone);
        die "Updating phase 'o': $DBI::errstr\n"
          if ( $dbh_Results->err() );
        $sth_update->finish;
    } ## end unless ( $PhaseDone =~ /o/...)

    # Close the results database
    $dbh_Results->disconnect;
    $dbh_Results = undef;

} ## end sub CreateOutput

#--------------------------------------------------------------------
# UUencode logos
#--------------------------------------------------------------------
sub UUEncodeLogos()
{
    warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
      . " => Creating uuencoded data\n"
      if ($opt_v);

    # UUencode a picture file to be used as a logo later
    foreach my $LogoSize ( 'small', 'large' )
    {

        # Pick the right the logo to uuencode
        my $JPGName = ( $LogoSize eq 'small' ) ? "$opt_u" : "$opt_U";
        next unless ( -s "$JPGName" );

        my ( $uuencoded_data, $line );

        open( PIC, '<', "$JPGName" )
          or die "ERROR: Picture '$JPGName' not found\n";

        # Process the file
        binmode(PIC);
        while ( read( PIC, $line, 45 ) )
        {
            $uuencoded_data .= pack( "u", $line );
        } ## end while ( read( PIC, $line,...))
        close(PIC);

        if ( length("$uuencoded_data") )
        {

            # Read in the current perl script
            open( PS, '<', "$0" ) or die "ERROR: Can not read '$0'\n";
            warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
              . " => Reading $0\n"
              if ($opt_v);

            my $Orig = q{};
            my $InUU = 0;
            while (<PS>)
            {
                if ($InUU)
                {
                    if (/^end logo_$LogoSize/)
                    {
                        $Orig .= "$_";
                        $InUU = 0;
                    } ## end if (/^end logo_$LogoSize/...)
                } else
                {
                    $Orig .= "$_";
                    if (/^begin logo_$LogoSize/)
                    {
                        $Orig .= "$uuencoded_data";
                        $InUU = 1;
                    } ## end if (/^begin logo_$LogoSize/...)
                } ## end else [ if ($InUU) ]
            } ## end while (<PS>)
            close(PS);

            # Write the new script
            open( PS, '>', "new-$ProgName" )
              || die "ERROR: Can not write new 'new-$ProgName'\n";
            warn POSIX::strftime( "%Y-%m-%d %H:%M:%S", localtime )
              . " => Updating $0\n"
              if ($opt_v);
            print PS "$Orig\n";
            close(PS);
        } else
        {
            warn "WARNING: No data to create '$LogoFile{$LogoSize}'\n";
        } ## end else [ if ( length("$uuencoded_data"...))]
    } ## end foreach my $LogoSize ( 'small'...)
} ## end sub UUEncodeLogos

#--------------------------------------------------------------------
# Show usage
#--------------------------------------------------------------------
sub ShowUsage()
{
    print "Usage: $ProgName [options]\n",
      "       -c conf    Specify configuration file [default=$opt_c]\n",
      "       -f path    Specify '.xml' file [default=none]\n",
      "       -p action  Specify what to do\n",
      "          i = read in .xml, CustomerName.in and NI.iprange files\n",
      "          m = merge scores with master score database\n",
      "          s = score all findings based on consolidated scores\n",
      "          o = output final documents\n",
      "          NOTE: You can combine the actions, eg. 'imso'\n",
      "       -u path    UUencode the small logo [default=none]\n",
      "       -U path    UUencode the large logo [default=none]\n",
      "          NOTE: The builtin logos are the ones for B-LUC Consulting\n",
      "       -v         Show progress messages [default=no]\n",
      "       -h         Show this help [default=no]\n",
      "       -d         Show some debug info on STDERR [default=no]\n\n";

    exit 0;
} ## end sub ShowUsage

#--------------------------------------------------------------------
# Main function
#--------------------------------------------------------------------
# Speedup STDOUT
$!++;

# Get program options
getopts('c:df:hm:p:u:U:v') || ShowUsage();
ShowUsage() if ($opt_h);

if ( ($opt_u) || ($opt_U) )
{
    UUEncodeLogos();
    exit 0;
} ## end if ( ($opt_u) || ($opt_U...))

# Sanity checks for configuration file
unless ( -s "$opt_c" )
{
    warn "ERROR: Configuration file '$opt_c' not found\n";
    ShowUsage();
} ## end unless ( -s "$opt_c" )

# Read global ini file into memory
$ExternalConfig = Config::IniFiles->new(
    -file    => "$opt_c",
    -default => 'default settings'
) or die "ERROR: Can not read configuration file '$opt_c'\n";

unless ( length($opt_p) )
{
    warn "ERROR: No action specified\n";
    ShowUsage();
} ## end unless ( length($opt_p) )

# Sanity checks for input file
unless ( length($opt_f) )
{
    warn "ERROR: No '.xml'. file specified\n";
    ShowUsage();
} ## end unless ( length($opt_f) )
die "ERROR: Specified '.xml' file '$opt_f' not found\n"
  unless ( -f "$opt_f" );

# Create an empty comments if necessary
my ( $filename, $directories, $suffix ) = fileparse( $opt_f, qr/\.[^.]*/ );
$CommentsFile = "$directories" . "$filename" . '.comments';
system("touch '$CommentsFile'");

# Sanity checks for master score database
# Determine the database type for the master scores
if ( $ExternalConfig->exists( 'MasterScores', 'dbtype' ) )
{
    $DBMS{'dbtype'} =
      trim( $ExternalConfig->val( 'MasterScores', 'dbtype' ) );
} else
{
    die "ERROR: No database type specified in configuration file.\n";
} ## end else [ if ( $ExternalConfig->exists...)]
SW_DBTYPE:
{
    if ( $DBMS{'dbtype'} =~ /^sqlite3$/ )
    {
        unless (
            $ExternalConfig->exists( 'MasterScores', 'sqlite3_dbname' ) )
        {
            warn
              "ERROR: No name for the master score database specified\n";
            ShowUsage();
        } else
        {
            $DBMS{'sqlite3_dbname'} = trim(
                $ExternalConfig->val( 'MasterScores', 'sqlite3_dbname' ) );
            unless ( length( $DBMS{'sqlite3_dbname'} ) )
            {
                warn
                  "ERROR: No name for the master score database specified\n";
                ShowUsage();
            } ## end unless ( length( $DBMS{'sqlite3_dbname'...}))
        } ## end else
        last SW_DBTYPE;
    } ## end if ( $DBMS{'dbtype'} =~...)

    if ( $DBMS{'dbtype'} =~ /^mysql$/ )
    {
        unless (
            $ExternalConfig->exists( 'MasterScores', 'mysql_dbname' ) )
        {
            warn
              "ERROR: No name for the master score database specified\n";
            ShowUsage();
        } else
        {
            $DBMS{'mysql_dbname'} = trim(
                $ExternalConfig->val( 'MasterScores', 'mysql_dbname' ) );
            unless ( length( $DBMS{'mysql_dbname'} ) )
            {
                warn
                  "ERROR: No name for the master score database specified\n";
                ShowUsage();
            } ## end unless ( length( $DBMS{'mysql_dbname'...}))
        } ## end else
        unless (
            $ExternalConfig->exists( 'MasterScores', 'mysql_dbserver' ) )
        {
            warn
              "ERROR: No server for the master score database specified\n";
            ShowUsage();
        } else
        {
            $DBMS{'mysql_dbserver'} = trim(
                $ExternalConfig->val( 'MasterScores', 'mysql_dbserver' ) );
            unless ( length( $DBMS{'mysql_dbserver'} ) )
            {
                warn
                  "ERROR: No server for the master score database specified\n";
                ShowUsage();
            } ## end unless ( length( $DBMS{'mysql_dbserver'...}))
        } ## end else
        unless (
            $ExternalConfig->exists( 'MasterScores', 'mysql_dbport' ) )
        {
            warn
              "ERROR: No port for the master score database specified\n";
            ShowUsage();
        } else
        {
            $DBMS{'mysql_dbport'} = trim(
                $ExternalConfig->val( 'MasterScores', 'mysql_dbport' ) ) +
              0;
            unless ( length( $DBMS{'mysql_dbport'} ) )
            {
                warn
                  "ERROR: No port for the master score database specified\n";
                ShowUsage();
            } ## end unless ( length( $DBMS{'mysql_dbport'...}))
        } ## end else
        unless (
            $ExternalConfig->exists( 'MasterScores', 'mysql_dbuserid' ) )
        {
            warn
              "ERROR: No user id for the master score database specified\n";
            ShowUsage();
        } else
        {
            $DBMS{'mysql_dbuserid'} = trim(
                $ExternalConfig->val( 'MasterScores', 'mysql_dbuserid' ) );
            unless ( length( $DBMS{'mysql_dbuserid'} ) )
            {
                warn
                  "ERROR: No user ID for the master score database specified\n";
                ShowUsage();
            } ## end unless ( length( $DBMS{'mysql_dbuserid'...}))
        } ## end else
        unless (
            $ExternalConfig->exists( 'MasterScores', 'mysql_dbpassword' ) )
        {
            warn
              "ERROR: No password for the master score database specified\n";
            ShowUsage();
        } else
        {
            $DBMS{'mysql_dbpassword'} = trim(
                $ExternalConfig->val( 'MasterScores', 'mysql_dbpassword' )
            );
            unless ( length( $DBMS{'mysql_dbpassword'} ) )
            {
                warn
                  "ERROR: No user ID for the master score database specified\n";
                ShowUsage();
            } ## end unless ( length( $DBMS{'mysql_dbpassword'...}))
        } ## end else
        last SW_DBTYPE;

        die "ERROR: Unknown database type '"
          . $DBMS{'dbtype'}
          . "' specified.\n";
    } ## end if ( $DBMS{'dbtype'} =~...)
} ## end SW_DBTYPE:

# Deal with specific logos
if ( $ExternalConfig->exists( 'LogoFile', 'small' ) )
{
    my $LogoFile = trim( $ExternalConfig->val( 'LogoFile', 'small' ) );
} ## end if ( $ExternalConfig->exists...)

# Populate the values for placeholders
foreach my $PH ( keys %TemplateValues )
{
    $TemplateValues{"$PH"} =
      trim( $ExternalConfig->val( 'Placeholders', "$PH" ) )
      if ( $ExternalConfig->exists( 'Placeholders', "$PH" ) );
} ## end foreach my $PH ( keys %TemplateValues...)

# Make sure that the actions are in lower case
$opt_p =~ tr/A-Z/a-z/;

if ( $opt_p =~ /i/ )
{

    # Input mode specified
    ReadInput();
} ## end if ( $opt_p =~ /i/ )

if ( $opt_p =~ /m/ )
{

    # Extract master score database
    ReadMasterScores();
} ## end if ( $opt_p =~ /m/ )

if ( $opt_p =~ /s/ )
{

    # Score all findings
    ScoreFindings();
} ## end if ( $opt_p =~ /s/ )

if ( $opt_p =~ /o/ )
{

    # Create the output files
    CreateOutput();
} ## end if ( $opt_p =~ /o/ )

# We are done
exit 0;
__END__
# Ensure that the logo is between the lines "begin logo_small"
#  and "end logo_small", e.g by calling this script like this:
# nbe2.pl -u <path of jpg file>
# Recommended max. size:  80x20 pixels
begin logo_small
M_]C_X``02D9)1@`!`0$`2`!(``#_VP!#``4#!`0$`P4$!`0%!04&!PP(!P<'
M!P\+"PD,$0\2$A$/$1$3%AP7$Q0:%1$1&"$8&AT='Q\?$Q<B)"(>)!P>'Q[_
MVP!#`04%!0<&!PX("`X>%!$4'AX>'AX>'AX>'AX>'AX>'AX>'AX>'AX>'AX>
M'AX>'AX>'AX>'AX>'AX>'AX>'AX>'A[_P``1"``4`"@#`2(``A$!`Q$!_\0`
M&P`!``("`P````````````````,%`0($!@?_Q``K$``!`@4#!`$#!0``````
M```!`@,`!!$2(043,2)!46$&%(&1(S)Q@J'_Q``;`0`!!`,`````````````
M`````00&!P(#!?_$`",1``$$`0,$`P````````````$``@,1!!(AT05A@9$3
M47'_V@`,`P$``A$#$0`_`/3)][4DZG+-RC`7+E)+RC3[4_!']@<T(B"9F=6"
MW]J440EP!``&4ANO)\KZ/5*F@S%QJ&GZJ[(3")-A]J86TH,N*940E1&#P>_H
M_>*U_2?EES@E&PRUU;86EQQ0Z>DDE)S=6OHBG&:.AQ)G`7'[!_;X4OAR('`6
M6"MMSYOA2:2J?4A8G\.I4I-$I`00%$)4#[2`H^+J=C'.BK=TKY>XJTMI2T:$
MA*5I4/U*D`A'&WBO-1X)B$:5\TEY>ZS><Z;QMJ)4;+2K]F`%!*J`<78)P5=T
MN=Y)JNU'A#A%(2?E8.UJZA&9*2U,23!F92:+VVG<*F2#=3.!4#/@G^3"&+L.
M=IHL/HIF98P2`X'RNRB66J85,?73@#@%6@[T#`&!VX'?S3DU,RCC3^[]?.K-
M:VK=JG@]J>_\%:PA%ZL>[2-U!"T+02*@\XX-1U"CCBEE!?)2*DFT>`*X`X%!
MV$2+E7%M-(^OG4EM%EP=RKVK&3[_`#6$(SUN^T:0MI=I<M<H34RZ-LIM=<N'
3-:YS7M"$(YV<]VH;K?"!17__V0``
`
end logo_small
#
# Recommended max. size: 400x120 pixels
begin logo_large
M_]C_X``02D9)1@`!`0$`2`!(``#_VP!#``4#!`0$`P4$!`0%!04&!PP(!P<'
M!P\+"PD,$0\2$A$/$1$3%AP7$Q0:%1$1&"$8&AT='Q\?$Q<B)"(>)!P>'Q[_
MVP!#`04%!0<&!PX("`X>%!$4'AX>'AX>'AX>'AX>'AX>'AX>'AX>'AX>'AX>
M'AX>'AX>'AX>'AX>'AX>'AX>'AX>'A[_P``1"`!0`*`#`2(``A$!`Q$!_\0`
M&P```@,!`0$```````````````4$!@<#`0+_Q``]$``!`P,#`@((!0$%"0``
M```!`@,$``41!A(A$S$B00<4%E%A9*/A(S)Q@9$5)#,T0H(U4E5SE*&BL='_
MQ``<`0`"`P$!`0$``````````````P$"!`4&!PC_Q``T$0`!`P$&`P8$!@,`
M```````!`@,1``0%$B$Q0091D1,4(E)3T3)A@:$50F)Q\/$6L>'_V@`,`P$`
M`A$#$0`_`-=HHKG))3&<4'`T0@G>1D)X[X\Z_/($F*].!)BNE%9]8]1ZBN&E
M;C?URX3*(BE=-M<8X=VI!P3NXSG''G5NTM=%7FP1+FM@L*?02I'N()&1\#C(
M^!K?:[N=LH)608.$QL8F*WVN[7K*"5D&#A,;&)BF=%%4M-\O4GTA2-/Q9,<1
M64=1;A8W*2-H.._O4!FDV:RKM&+"0,(DSR%)LUD7:<>$@81)GD*NE%*M6SGK
M9IV9/8=2VXPWN25(W`GL!C/F<"E,&ZW;V#=OLV2PF0J,7VTI8\*0,D#OSN&/
MTS5FK$MQH.@B"K#OKTJS5B<=:#H(@JP[ZGZ5:Z*H]OU;,AZ+3J*]*;=5(441
MXS;>PJ(41WR>,`GMV%-+4C4]PM2)TFY1X3[R`MMAN,%(0#R`HJ.2>V<$4UV[
M7&9+B@`#AG/,C6,IRW,13G;L<9DN*``.&<\R-8RG+<Q%62BH-A=G/V>*]<FD
MM2UM@NH2D@)/NP>U3JPK1@44G:L"T8%%)VHHHHJM4HHHHHHHHHHHHHHHHHHH
MI%K^;ZAHZY/!6%*9+2><'*_#Q_.?VI[46?;K?/V>OP8LKIYV=9E*]N>^,CCL
M/XI]E<0V\E:Q(!!Z5HLKB&WD+<$@$'I66W.VP;;Z+H$ER4ZB<Z4O--%Y2FW2
MI6>6CE/"3WQY"KS$5J5ZSVUR`W:8I5%076GVE^%6.R0D@`?#RIG'LEECNI=C
MVBWLN)[+1&0DC]P*85T[9>H?2!$G$I4J^>P'(?R*Z=LO8/I`PR<2E2KY[`<A
M_(I3:QJ-,K-U=M)C[3_AD.!>?+\Q(Q50]&#C<R^:BU"\M*4+=VH6HX"4E14<
M^X8":T"5&CRXZH\IAI]E>-S;B`I)P<C(/'<5`]G=/_\``K7_`-(W_P#*4S;F
MTM.H4(*X&0&0!D[[TIBW-I9=0H05P,@,@#)WWJJ^DVZB=Z/TRH:%F/)E);W'
MC*$E6%?H2@8_45V])+J(6B8UDAGJ.RBU&80C\RDIP<@#RX`_>KB8<0P_4C%8
M]5V[.CTQLV^[;VQ4:%9+1"?#\6VQ670,)6EL;DCW`^0_2F,7BPT&QA,(45`<
MYB).T1R,TUB\6&@V,)A"BH#G,1)VB.1FL^])UJ<@:=TXT4$Q88Z4@C)&XA'/
M[[5?S6@W:[0[;:Q.6L.(6`&$((R\H_E2GWY^]37FFWFE-/-H<;4,*2M.01\0
M:@,6"R,/I>:M4-#B#E!#(\!SGP^[]J6Y;V[0RVV^#X"HY;XC/TSWSI;EO;M#
M+;;X/@*CEOB,_3/?.F0[<T445RJY%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M<9LJ/"BN2I;R&66QE2UG`%=JSGTKNNS;]8[!O*8\AU)7@XW%2P@9_3G^:W7=
M8Q;+0&B8&9)^0$FM]VV,6RT!HF!F2?D!)JSIU5!7!5<&H=Q=@I!/K*8QV%([
MJ`/BQ\<4ZB/M2HK4IA6]IY"7&U8(RDC(.#R.*\<;CHB%A:4(CA&PI[)"<8Q^
MF*IMLGR;[J>=;XTMR#9;0GI$,+VJ<4,I&5=]OA5V]PJZ+.W:$*4V,(3F23.6
M@&FI/T_:F(L[=I0M38PA.9),Y:`::D_3]JO%%9I"U3='?1W/N*Y[J),22&F7
MTMHR\#C`4%)(_P`V3C!X'[^W"XZ@1!TPA-T?1+GNH\&!XTG!*E\9[JX';;CC
M-:OP)X**5*`S(WV&([:16G\!>"BE2@,R-]AB.VD5I5%4?6UWDP]21X,N;*MM
ML>C$-2F/)\G@K/\`NC';XYKYU++OD-S3MJ;O#BILQ8;D*:0@)4D$97^7(.#Y
M$=NU(;NEQ8;.(#&"1KH`2<XC*,^65);NEQ8;.(#&"1KH`2<XC*,QME5ZI&=1
ML^U*-/&#*3(4DKZBMFS8`3NX43SC'(!I/"NLVY^E!^%'EN"WV]@]1M)\*UX`
M.?B"K_QJ)IM2+AK35%[<<4VS&08R'$GE(`P5)^.$9_>G-7:&TK4]GX`K?(J,
M)'WFFM78&TK4]GX`H:Y%1`2/O-7I,B.J2J*EYLOI3N4V%`J2/(D>5+[%?H-Y
ME3X\1+X5!=Z3I<1@$Y(RGGMX3WP:HV@([J=.WC5#T^7&=<6XX5(*#O"4DY.Y
M)R<D_P`5,MEUNS7HREW^7<743''%.-.);;YY"$I*2G&-WPSCSIKMSI;*VTJD
MA24`YCQ'7*#(UWIKMSI;*VTJDA24`YCQ'7*#(UWK0J*S"Y734*=,V!Q%UD-S
MI\A*6T@#+H/=2N/>4``<8Y.2>'6J+K+]MH%E<GJMEM6R777DJ""X1N\.\]AP
M!Q[S\*0;F<"@,0/Q<]$Z[9SM2#<K@4!B!^([Z(UVSG:KK15)]'DBZ7*;<)<B
MX37[8P\IN#U,`.IRKDG&5$#;_/PJ[5@MEE-E=+1()'+^=:P6RRFRNEHD$CE_
MK]^=%%%%9JRT44[C6#K1FGO6]N]`5CIYQD9]]=/9SYSZ7WKT2.$[W6D*2UD?
MU)]ZYRKVLB3!7]C[4@I%JS3;%^$9T2%Q)D1>^.^A.[:<@\CS&0#W%7SV<^<^
ME]Z/9SYSZ7WI['"]^,+#C;4$?J1[TQB_[.PX'&W((^1]JH4FU7BX/11<;E%$
M5AU#JV8\=2>NI)R`HE1P,@'%0/9*5'5=F;9=$1HUU65/;V"IQO.<A!W`<Y(Y
M'%:9[.?.?2^]'LY\Y]+[UI1<-_H$):`'*41K.D\_;2M*.*6D"$N`#EARUG2-
M9]M*S2Y:*C/Z>@V*+*5'AQW@\\2G<MX\YR<C&<G]...*[WO3#EPU)`NK5Q,5
MJ(R6@TAK)&<@E)SP<*]W&!6B>SGSGTOO1[.?.?2^]2+CXB!G!YMT?FUWWJ1Q
M6D&>U\VWFUVWK,[AI*7+:FP7+OU;=+>#H;?:+CD?!!PVHJXXXY'`]_.>USTH
MN1J&VW.+<3&;@QN@EOI!9QA0R"3C.%=R#C`K1O9SYSZ7WH]G/G/I?>@7)Q"-
M&^8U1N(.^X%2.+$#1WF/AYB#MN!6;Z>TE_2+W-G)GJ6Q(<"TLA&"`,[059R0
M,_O@9J/9M%O0+1<8"KNMSUL.[5):VA)6G;N4,^(X\N`,G]:U#V<^<^E]Z/9S
MYSZ7WJ57+Q$HDEO6)S1^73?^]ZE7%J5$DNZQ/A\NFW][UF,?1SK6CGK#_5%%
MQ;>Q+@;PA`W[R-N><G@DGMCMVHFZ,=DZ8CV,W982A:"XYTN"A`("$IS@<G.2
M2<_L!IWLY\Y]+[T>SGSGTOO4_@W$85B[//%BU1KSUJ?\N`5B[7/%B^'?GI_R
ML[ONEUW"\6J9&G^IM6].U#26@K'Q3DX!P!Y'L*K"8LB_:JN#UJN:[8[;4".E
MN9A[>!P3M5^5/A!)\63SYUM?LY\Y]+[U$F:(MDQT.RVXDAP=E.Q$J(_<FG66
MYK[:3A6S,"!F@Q)DR"<YSUTFG63BVS-)PK7,"`<,Q)DR",YSUTFJ/Z/KQ.O%
MG=<GMM!R.^I@.M?D="<<C[<59*>MZ80VV&VY24(2,!*6<`?]Z^O9SYSZ7WKG
MVGA6]775+19\(.V).7WKG6F_+`ZZI:#A!V@Y?:D%%/\`V<^<^E]ZYR;!T8SK
MWK>[8@JQT\9P,^^LR^$[W0DJ4UD/U)]Z2F]K(HP%_8^U-FI"(EB1*<"BAF*'
M%!/<@)R<?Q2"V>D"Q2[>[,?;G0$-,L2%(>8WJZ+S740Y^$5@)P%@DD;=AS@8
M)=,/0'K0W%D2&"A<<-N(+H!P4X([Y%5AWT?>CQV$W#>B)=;;1TTJ7<WU.=/I
MAOI[RYN*`D8"2=HR2`"2:^NV2V,"SMCM$Z#<<J\DXRKM%2DZ\J=6W5]FN"[T
MF.9I%FDHC25&&[A:U-H<3TL)_$R'$XVY)R"`0I),B7J2T1M(R]5&27;7%B.R
MW7&TDJ#;:2I8V]PH;5`I.""""`:4S-(Z+EHEMO(RW,<9=?0BY/(2IQI+:6UX
M2X`%A++8W#!(3@D@G+&%:],0].+TXRW#-I6VZTN*X[U$+0X5%:5;B20=RL@^
M^M'?;/ZB>HI995Y3TKQW55MAN(9NZ'[2\XO:AN2$J*DX1^(2VI02V%.)05*(
M&[CS&8$OTB:58MB;@W+DRV52(T<"-#=<62^Z&VU!(3DH*CC<,C(*1E6$GQ>D
MM&./L2'0IV0PX7$/NW1Y;I)V9"EEPJ6G\-'A42..W>O5:3T48:(@8:2TVRPP
MC9.<2I"&7NNUA07D%+GB"@<Y\Z.^L>HGJ*GL3Y3TJ=<]8Z:MLB4Q.NK3*XB2
MI\E"BE&`DJ&X#!4`M*BD'("@2`#FFMMG1[C#1+BJ<+2\XZC2FU#!Q@I4`H'X
M$"JU(TAHA]^6\\RA:YC72DYN#N'1M2DJ4-^"LA"<K_,<<GDTZ>1;WKO'N2[J
MZ%1VUMML(F%#)W8RI2`0%JX`!5G;SMP223OMG]1/45!85LD]*5Z[UW9-&/0&
MKRF7_;D/J94RV%)RT@**221@JR`G/!)`)&17.=Z0]+Q+1+N2I;[R(C/6=;8C
MK<64A02K;M&%[%$)7M)"%<*(/%3-3672NI6NE?&HDUOU=Z/M6^0.FZ$A8P%#
MOM3SW&.,5!9T?H9EBX1VHS#;-Q:6S):3.<"%)6<K(3OPA14-Q4G"MQ)SDDT=
M]L_J)ZBI#)@2D]*82]8Z8AON,3;U%B.M%(=1():4WN;ZH*@H#:-G))P!YXKF
MWK;33DB+&1<'/6)*UH0R8KH<24%&_>DIRV$]1LDK``"T$\*!*V5H7T?S+D;C
M.@QYLI3/1<<E37'NJCIEO"PM9"\H)!*LD\9Y`IE:+%I6U7!NXP]GKJ$/(]9>
MG+>=6'BSU-ZUK)62([(!424I;2D$`8H[ZQZB>HH[$^4]*D:1U/:M40WI-K<<
M4&'EL/(6GEM:%J24D@D9\.<9R`4Y`S4R_P!S;L]I>N+L:3)0UMRW'0%+.5!.
M0"0,#.220``3Y4BCZ8TK#C.Q[;(=MC;[R7I`AW%QHO*"RYXE!61E1Y(()2`D
MDI\-/9Z[=-AN17IP2VX,*+,M3*Q^BT*"D_J"*.^V?U$]152RN<DGI2N-K/3[
MCS3#TYJ.\ZX4!"G4.!/C4A)6MM2D-[U)4E.Y0*E`I`W`@=&M8Z;<9ZPN02V6
M77T*6RX@.--I"U.)RD;D;5`A0R%>1-+FM':&:F-341F?66R273.<*G<K4Y^*
M2O\`%\2UG"]V-RO>:^G=):,=MJ+<X5KC(0IMM*KJ^2VVI`0IM"NIE+92`"@$
M)X'&11WVS^HGJ*MV)\IZ5VD:[TXBR7B[QI#\QBT1WWY(:84G(9!WI2I82@D8
M'^8`;DDD`@UWN6L+);;Q<+9/<DQU6^W&Y2GUQ7.BVP-V5;\8.`D\#W>\$5Q=
MT[I%S3$_3)2VFU7#J>M,(G.(+@</C&\+"@%=B`0,$CL:]N6G]+7!9<EN!UPP
M%6]:ESEJ#K!!!0ZE2BEW\RO[P*Y)-'?;/ZB>HH[%7E/2GD.?#F2)L>,^EQV"
M^(\E(!_#<+:'`D_Z'$*_U5[<O]G2?^2O_P!&E&F;79-/FY&%<773<98E/*E3
M2\K>&6F0`I1*B-C*/S$G.>:8W"9$5`D)3*84HM*``<&2<&L]KMEG-G<':#0[
-CE5FF5AQ/A.HVK__V0``
`
end logo_large
