#!/bin/bash
###############################################################################
PROG_VERSION='2.0'
#
# (c) Copyright 2013 B-LUC Consulting Thomas Bullinger
#
# Authoritative repository:
# https://gitlab.com/bluc/IT-Security.git
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
###############################################################################

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Be gentle to disk I/O
ionice -c2 -n7 -p $$

# You must be root to run this script!
if [ $EUID -ne 0 ]
then
    echo "Only 'root' can run this script"
    exit 1
fi

# Adapt to your setup!!!
NESSUS_USER=''
NESSUS_PASS=''
if [ -z "$NESSUS_USER" -o -z "$NESSUS_PASS" ]
then
    cat << EOT
ERROR: Either the nessus user or password is missing.
       Please adapt the script accordingly and try again.
EOT
    exit 1
fi

# Define the editor to use (fallback to "vi")
EDITOR=${EDITOR:-vim}
[ -x $EDITOR ] || EDITOR='vi'

# Install the necessary debian packages
if [ -x /usr/bin/apt-get ]
then
    # Debian packages:
    apt-get install at nmap mailx ruby libyaml-ruby libzlib-ruby ruby-dev rubygems1.8 libhttpclient-ruby1.8

    # Get the necessary nessus ruby gems
    [ -s /usr/local/src/nessus-xmlrpc-0.5.0.gem ] || wget -t 5 http://rubyforge.org/frs/download.php/73906/nessus-xmlrpc-0.5.0.gem -O /usr/local/src/nessus-xmlrpc-0.5.0.gem

    # Build them if necessary
    GEM_DIR=/var/lib/gems/1.8/gems/nessus-xmlrpc-0.5.0
    cd /usr/local/src
    [ -s $GEM_DIR/bin/nessus-cli.rb ] || gem install nessus-xmlrpc-0.5.0.gem
                                        
    # Apply patches
    cd $GEM_DIR/bin

    # Create a new patched nessus-cli.rb file
    #   - Contains patch to show start and end times of a scan
    #   - Contains patch to allow multiple targets
    if [ -z "$(grep 'B-LUC Consulting' nessus-cli.rb)" ]
    then
        mv -f nessus-cli.rb nessus-cli.rb.ORIG
        cat << EOT > nessus-cli.rb
#!/usr/bin/env ruby
# = nessus-cli.rb:  Nessus command line interface for XML-RPC
# Author:: Vlatko Kosturjak
# 
# (C) Vlatko Kosturjak, Kost. Distributed under GPL and BSD (dual licensed).
# Patched by B-LUC Consulting with nessus-cli.rb.verbose.patch
#  - Also show start and end time for scan
# Patched by B-LUC Consulting with target-file.diff
#  - Allow for targets to be read from a file
#  https://rubyforge.org/tracker/download.php/9436/36492/28194/4929/target-file.diff

require 'nessus-xmlrpc'
require 'getoptlong'

verbose = 0
debug = 0
operation = ''
targets = ''
deletereport = false
user = ''
password = ''
scanname = ''
output = ''
output1 = ''
wait = ''
policy = ''
url = ''

def intro 
	\$stderr.print \$0 + ": Nessus command line interface for XML-RPC\\n"
	\$stderr.print "(C) Vlatko Kosturjak, Kost. Distributed under GPL.\\n"
	\$stderr.print "\\n"
end

intro

def give_help
	puts <<-EOF
--user <user>	user for login to Nessus server
--password <p>	password for login to Nessus server
--scan <name>	start scan with name
--target <ip>	specify list of targets, separated by comma
--policy <pol>	specify policy to use (name of policy)
--url <url>	url of Nessus server (default: localhost:8834)
--wait [t]	wait scan to finish (ask in regular periods of <t> for status)
--output <f>	output report XML to file <f>
--output1 <f>	output report XML v1 to file <f>
--reportdelete	delete report after finish or delete report by id (if alone)
--stop <id>	stop scan identified by <id>
--stop-all	stop all scans
--pause <id>	pause scan identified by <id>
--pause-all	pause all scans
--resume <id>	resume scan identified by <id>
--resume-all	resume all scans
--report <id>	download report identified by <id>
--list-scans	list scans
--list-policy	list policies
--status <id>	get status of scan by <id>
--verbose	be verbose
--debug		be even more verbose
--help		this help

Examples: 
#{\$0} --user john --password doe --scan scan-localhost --wait --output report.xml --target localhost
EOF
	exit 0
end

if ARGV.length < 1
	give_help
end

opt = GetoptLong.new(
	["--help", "-h", GetoptLong::NO_ARGUMENT],
	["--verbose", "-v", GetoptLong::OPTIONAL_ARGUMENT],
	["--target", "-t", GetoptLong::REQUIRED_ARGUMENT],
	["--user", "-u", GetoptLong::REQUIRED_ARGUMENT],
	["--password", "-p", GetoptLong::REQUIRED_ARGUMENT],
	["--policy", "-P", GetoptLong::REQUIRED_ARGUMENT],
	["--url", "-U", GetoptLong::REQUIRED_ARGUMENT],
	["--deletereport", "-D", GetoptLong::OPTIONAL_ARGUMENT],
	["--wait", "-w", GetoptLong::OPTIONAL_ARGUMENT],
	["--scan", "-s", GetoptLong::REQUIRED_ARGUMENT],
	["--list-scans", "-l", GetoptLong::NO_ARGUMENT],
	["--list-policy", "-L", GetoptLong::NO_ARGUMENT],
	["--status", "-W", GetoptLong::REQUIRED_ARGUMENT],
	["--stop", "-S", GetoptLong::REQUIRED_ARGUMENT],
	["--stop-all", "-a", GetoptLong::NO_ARGUMENT],
	["--pause", "-q", GetoptLong::REQUIRED_ARGUMENT],
	["--pause-all", "-Q", GetoptLong::NO_ARGUMENT],
	["--resume", "-e", GetoptLong::REQUIRED_ARGUMENT],
	["--resume-all", "-E", GetoptLong::NO_ARGUMENT],
	["--report", "-r", GetoptLong::REQUIRED_ARGUMENT],
	["--output", "-o", GetoptLong::REQUIRED_ARGUMENT],
	["--output1", "-1", GetoptLong::REQUIRED_ARGUMENT]
)

def give_error
	\$stderr.print "You used incompatible options, probably you mixed --scan with --stop"
	\$stderr.print "or something similar."
	exit 0
end

opt.each do |opt,arg|
	case opt
		when	'--help'
			give_help
		when	'--user'
			user = arg
		when	'--password'
			password = arg
		when 	'--stop'
			if operation == ''
				operation = "stop"
				scanname = arg
			else
				give_error
			end
		when 	'--pause'
			if operation == ''
				operation = "pause"
				scanname = arg
			else
				give_error
			end
		when 	'--resume'
			if operation == ''
				operation = "resume"
				scanname = arg
			else
				give_error
			end
		when 	'--stop-all'
			if operation == ''
				operation = "stop-all"
			else
				give_error
			end
		when 	'--pause-all'
			if operation == ''
				operation = "pause-all"
			else
				give_error
			end
		when 	'--resume-all'
			if operation == ''
				operation = "resume-all"
			else
				give_error
			end
		when 	'--report'
			if operation == ''
				operation = "report"
				scanname = arg
			else
				give_error
			end
		when 	'--scan'
			if operation == ''
				operation = "scan"
				scanname = arg
			else
				give_error
			end
		when	'--target'
			if arg[0..6] == 'file://'
				f = File.open(arg[7..-1], "r")
				f.each_line do |line|
					line=line.chomp
					line=line.strip
					unless line == '' or line == nil
						if targets == ''
							targets = line
						else
							targets = targets + "," + line
						end
					end
				end
				f.close
			else
				# if there's multiple target options, add comma
				if targets == ''
					targets = arg
					
				else
					targets = targets + "," + arg
				end
			end
		when	'--wait'
			if arg == ''
				wait = 15
			else
				wait = arg.to_i
			end
		when	'--reportdelete'
			if arg = ''
				deletereport=true
			else
				operation = "reportdelete"
				scanname = arg
			end

		when	'--output'
			output = arg
		when	'--output1'
			output1 = arg
		when	'--policy'
			policy = arg
		when	'--status'
			if operation == ''
				operation = "status"
				scanname = arg
			else
				give_error
			end
		when	'--url'
			url = arg
		when 	'--verbose'
			if arg == ''
				verbose += 1
			else
				verbose = arg.to_i
			end
		when 	'--debug'
			if arg == ''
				debug += 1
			else
				debug = arg.to_i
			end
		when	'--list-scans'
			if operation == ''
				operation = "list-scans"
				scanname = arg
			else
				give_error
			end
		when	'--list-policy'
			if operation == ''
				operation = "list-policy"
				scanname = arg
			else
				give_error
			end
	end
end

if (user == '') or (password == '')
	\$stderr.print "User and password is required to login to Nessus server"
	\$stderr.print "Try --help!"
	exit 1
end 

\$stderr.print "[i] Targets: " + targets +"\\n" if verbose > 0 
\$stderr.print "[i] Connecting to nessus server: " if verbose > 0 
n=NessusXMLRPC::NessusXMLRPC.new(url,user,password) 
if n.logged_in 
	\$stderr.print "OK!\\n" if verbose > 0
else
	\$stderr.print "[e] Error connecting/logging to the server!\\n" 
	exit 2
end

case operation
	when "scan"
		if policy == ''
			\$stderr.print "[w] Policy not defined, using first served from the server\\n"
			pid,name = n.policy_get_first
			\$stderr.print "[w] using policy id " + pid + " with name " + name + "\\n"
		else
			pid=n.policy_get_id(policy)
			if pid == ''
				\$stderr.print "[e] policy doesn't exit: " + policy + "\\n"
				exit 3
			end
		end	
		if targets == ''
			\$stderr.print "[w] Targets not defined, using localhost as target\\n"
			targets = '127.0.0.1'
		end
		\$stderr.print "[i] Initiating scan with targets: "+targets+': ' if verbose > 0
		uid=n.scan_new(pid,scanname,targets)
		\$stderr.print "done\\n" if verbose > 0
		time = Time.new
		timestr = time.strftime("%Y-%m-%d %H:%M:%S")
		\$stderr.print "Scan '"+scanname+" ("+uid+")' started at "+timestr+"\\n"
		unless wait == ''
			while not n.scan_finished(uid)
				\$stderr.print "[v] Sleeping for " + wait.to_s() + ": " if verbose > 1			
				sleep wait
				\$stderr.print "done\\n" if verbose > 1
				stat = n.scan_status(uid)
				print "\r" + stat if verbose > 0
			end	
		else
			puts uid
			exit 0
		end	
		outputfile=''
		unless output == ''
			\$stderr.print "[i] Output XML report to file: "+output if verbose > 0
			content=n.report_file_download(uid)	
			File.open(output, 'w') {|f| f.write(content) }	
			\$stderr.print ": done\\n" if verbose > 0
			outputfile = output
		end
		unless output1 == ''
			\$stderr.print "[i] Output XML1 report to file: "+output1 if verbose > 0
			content=n.report_file1_download(uid)	
			File.open(output1, 'w') {|f| f.write(content) }	
			\$stderr.print ": done\\n" if verbose > 0
			outputfile = output1
		end
		if deletereport
			\$stderr.print "[i] Deleting report: " if verbose > 0
			n.report_delete(uid)
			\$stderr.print "done\\n" if verbose > 0
		end
		time = Time.new
		timestr = time.strftime("%Y-%m-%d %H:%M:%S")
		\$stderr.print "Scan '"+uid+"' ended at "+timestr+" (output file is "+outputfile+")\\n"
	when "report"
		uid=scanname
		if (output == '') and (output1 == '') 
			\$stderr.print "[e] You want report, but specify filename with --output or output1\\n"
		end
		unless output == ''
			\$stderr.print "[i] Output XML report to file: "+output if verbose > 0
			content=n.report_file_download(uid)	
			File.open(output, 'w') {|f| f.write(content) }	
			\$stderr.print ": done\\n" if verbose > 0
		end
		unless output1 == ''
			\$stderr.print "[i] Output XML1 report to file: "+output1 if verbose > 0
			content=n.report1_file_download(uid)	
			File.open(output, 'w') {|f| f.write(content) }	
			\$stderr.print ": done\\n" if verbose > 0
		end
		if deletereport
			\$stderr.print "[i] Deleting report: " if verbose > 0
			n.report_delete(uid)
			\$stderr.print "done\\n" if verbose > 0
		end
	when "stop"
		\$stderr.print "[i] Stopping scan: " + scanname if verbose > 0
		n.scan_stop(scanname)
		\$stderr.print "done\\n" if verbose > 0
	when "stop-all"
		\$stderr.print "[i] Stopping all scans: " if verbose > 0	
		list=n.scan_stop_all
		\$stderr.print "done\\n" if verbose > 0
		if verbose > 1
			list.each {|uuid| puts "[v] Stop all: " + uuid }
		end
	when "pause"
		\$stderr.print "[i] Pausing scan: " + scanname if verbose > 0
		n.scan_pause(scanname)
		\$stderr.print "done\\n" if verbose > 0
	when "pause-all"
		\$stderr.print "[i] Pausing all scans: " if verbose > 0	
		list=n.scan_pause_all
		\$stderr.print "done\\n" if verbose > 0
		if verbose > 1
			list.each {|uuid| puts "[v] Pause all: " + uuid }
		end
	when "resume"
		\$stderr.print "[i] Resuming scan: " + scanname if verbose > 0
		n.scan_resume(scanname)
		\$stderr.print "done\\n" if verbose > 0
	when "resume-all"
		\$stderr.print "[i] Resuming all scans: " if verbose > 0	
		list=n.scan_resume_all
		\$stderr.print "done\\n" if verbose > 0
		if verbose > 1
			list.each {|uuid| puts "[v] Resume all: " + uuid }
		end
	when "reportdelete"
		\$stderr.print "[i] Deleting report: " + scanname if verbose > 0
		n.report_delete(scanname)
		\$stderr.print "done\\n" if verbose > 0
	when "status"
		puts "status: " + n.scan_status(scanname)	
	when "list-scans"
		list=n.scan_list_hash
		list.each {|scan| 
			puts "id="+scan['id']+":name="+scan['name']+":"+ \\
				"status="+scan['status']+":owner="+scan['owner']+":"+ \\
				"targets done/total="+scan['current']+"/"+scan['total']
		}
	when "list-policy"
		list=n.policy_list_names
		list.each {|policy| 
			puts policy 
		}
		
end

\$stderr.print "[v] End reached.\\n" if verbose > 1
EOT
    fi

    # Apply the patch file to collect all info for listing
    cd $GEM_DIR/lib
    if [ -z "$(grep 'B-LUC Consulting' nessus-xmlrpc.rb)" ]
    then
        mv -f nessus-xmlrpc.rb nessus-xmlrpc.rb.ORIG
        cat << EOT > nessus-xmlrpc.rb
#
# = nessus-xmlrpc.rb: communicate with Nessus(4.2+) over XML RPC interface
#
# Author:: Vlatko Kosturjak
#
# (C) Vlatko Kosturjak, Kost. Distributed under GPL and BSD license (dual).
# 
# == What is this library? 
# 
# This library is used for communication with Nessus over XML RPC interface. 
# You can start, stop, pause and resume scan. Watch progress and status of scan, 
# download report, etc.
#
# == Requirements
# 
# Required libraries are standard Ruby libraries: uri, net/https and rexml/document. 
#
# == Optional
# 
# Library is able to use nokogiri if available, but nokogiri is not required.
# 
# == Usage:
# 
#  require 'nessus-xmlrpc'
#  n=NessusXMLRPC::NessusXMLRPC.new('https://localhost:8834','user','pass');
#  if n.logged_in
# 	id,name = n.policy_get_first
# 	puts "using policy ID: " + id + " with name: " + name
# 	uid=n.scan_new(id,"textxmlrpc","127.0.0.1")
#	puts "status: " + n.scan_status(uid)
# 	while not n.scan_finished(uid)
# 		sleep 10
# 	end
#	content=n.report_file_download(uid)
# 	File.open('report.xml', 'w') {|f| f.write(content) }
#  end
# Patched by B-LUC Consulting with nessus-xmlrpc.rb.listall.patch
#  - Record owner, status and start time for a scan entry

require 'uri'
require 'net/https'
require 'rexml/document'

# NessusXMLRPC module
# 
# Usage:
# 
#  require 'nessus-xmlrpc'
#  n=NessusXMLRPC::NessusXMLRPC.new('https://localhost:8834','user','pass');
#  if n.logged_in
# 	id,name = n.policy_get_first
# 	uid=n.scan_new(id,"textxmlrpc","127.0.0.1")
#	puts "status: " + n.scan_status(uid)
#  end
#
# Check NessusXMLRPCrexml for description of methods implemented 
# (for both NessusXMLRPCnokogiri and NessusXMLRPCrexml).

module NessusXMLRPC 

# Class which uses standard REXML to parse nessus XML RPC replies. 
# It is adviseable to use NessusXMLRPC class, not this class directly.
# As NessusXMLRPC class will use nokogiri or rexml, depending on availability.
class NessusXMLRPCrexml
	# initialize object: try to connect to Nessus Scanner using URL, user and password
	#
	# Usage: 
	# 
	#  n=NessusXMLRPC::NessusXMLRPC.new('https://localhost:8834','user','pass');
	def initialize(url,user,password)
		if url == ''
			@nurl="https://localhost:8834/"
		else
			if url =~ /\\/\$/
				@nurl=url
			else
				@nurl=url + "/"
			end
		end
		@token=''
		login(user,password)
	end

	# checks if we're logged in correctly
	#
	# returns: true if logged in, false if not
	#
	# Usage: 
	# 
	#  n=NessusXMLRPC::NessusXMLRPC.new('https://localhost:8834','user','pass');
	#  if n.logged_in
	#	puts "Logged in"
	#  else
	#	puts "Error"
	#  end
	def logged_in
		if @token == ''
			return false
		else
			return true
		end
	end	

	# send standard Nessus XML request and check
	# 
	# returns: rexml/document root
	def nessus_request(uri, post_data) 
		body=nessus_http_request(uri, post_data)
		# puts response.body
		docxml = REXML::Document.new(body)
		begin 
		status = docxml.root.elements['status'].text
		rescue
			puts "[e] error in XML parsing"
		end
		if status == "OK"
			return docxml 
		else 
			return ''
		end
	end

	# send standard Nessus HTTP request and check
	#
	# returns: body of response
	def nessus_http_request(uri, post_data) 
		url = URI.parse(@nurl + uri) 
		request = Net::HTTP::Post.new( url.path )
		request.set_form_data( post_data )
		if not defined? @https	
			@https = Net::HTTP.new( url.host, url.port )
			@https.use_ssl = true
			@https.verify_mode = OpenSSL::SSL::VERIFY_NONE
		end
		# puts request
		begin
			response = @https.request( request )
		rescue 
			puts "[e] error connecting to server: "+ @nurl + " with URI: " + uri

			exit
		end
		# puts response.body
		return response.body
	end
	
	# login with user & password and sets object-wide @token, @name and @admin
	def login(user, password)
		post = { "login" => user, "password" => password }
		docxml=nessus_request('login', post)
		if docxml == '' 
			@token=''
		else
			@token = docxml.root.elements['contents'].elements['token'].text
			@name = docxml.root.elements['contents'].elements['user'].elements['name'].text
			@admin = docxml.root.elements['contents'].elements['user'].elements['admin'].text
			# puts "Got token:" + @token
		end
			
	end
	
	# initiate new scan with policy id, descriptive name and list of targets
	# 
	# returns: uuid of scan
	# 
	# Usage: 
	# 
	#  n=NessusXMLRPC::NessusXMLRPC.new('https://localhost:8834','user','pass');
	#  if n.logged_in
	# 	id,name = n.policy_get_first
	# 	puts "using policy ID: " + id + " with name: " + name
	# 	uid=n.scan_new(id,"textxmlrpc","127.0.0.1")
	#  end
	def scan_new(policy_id,scan_name,target)
		post= { "token" => @token, "policy_id" => policy_id, "scan_name" => scan_name, "target" => target } 
		docxml=nessus_request('scan/new', post)
		if docxml == '' 
			return ''
		else
			uuid=docxml.root.elements['contents'].elements['scan'].elements['uuid'].text
			return uuid
		end	
	end

	# get uids of scans
	#
	# returns: array of uids of active scans
	def scan_list_uids
		post= { "token" => @token } 
		docxml=nessus_request('scan/list', post)
		uuids=Array.new
		docxml.root.elements['contents'].elements['scans'].elements['scanList'].each_element('//scan') {|scan| uuids.push(scan.elements['uuid'].text) }
		return uuids
	end

	# get hash of active scan data
	# 
	# returns: array of hash of active scans
	def scan_list_hash
		post= { "token" => @token } 
		docxml=nessus_request('scan/list', post)
		scans=Array.new
		docxml.root.elements['contents'].elements['scans'].elements['scanList'].each_element('//scan') {|scan| 
#			scan.elements.each_with_index {|x, y| puts "#{x} => #{y}" }
			entry=Hash.new
			entry['id']=scan.elements['uuid'].text
			entry['name']=scan.elements['readableName'].text
			entry['current']=scan.elements['completion_current'].text;
			entry['total']=scan.elements['completion_total'].text;		
			entry['owner']=scan.elements['owner'].text
			entry['status']=scan.elements['status'].text
			entry['start_time']=scan.elements['start_time'].text
			scans.push(entry) 
		}
		return scans
	end

	# get policy by textname and return policyID
	# 
	# returns: policyID
	def policy_get_id(textname) 
		post= { "token" => @token } 
		docxml=nessus_request('policy/list', post)
		docxml.root.elements['contents'].elements['policies'].each_element('//policy') {|policy|
			if policy.elements['policyName'].text == textname
				return policy.elements['policyID'].text 
			end
		}
		return ''
	end	

	# get first policy from server and returns: policyID, policyName
	#
	# returns: policyID, policyName
	def policy_get_first
		post= { "token" => @token } 
		docxml=nessus_request('policy/list', post)
		docxml.root.elements['contents'].elements['policies'].each_element('//policy') {|policy|
				return policy.elements['policyID'].text, policy.elements['policyName'].text
		}
	end	

	# get list of policy IDs
	#
	# returns: array of all policy uids
	def policy_list_uids
		post= { "token" => @token } 
		docxml=nessus_request('policy/list', post)
		pids=Array.new
		docxml.root.elements['contents'].elements['policies'].each_element('//policy') { |policy| 
			pids.push(policy.elements['policyID'].text) }
		return pids
	end

	# stop scan identified by scan_uuid
	def scan_stop(uuid)
		post= { "token" => @token, "scan_uuid" => uuid } 
		docxml=nessus_request('scan/stop', post)
		return docxml
	end
	# stop all active scans 
	# 
	# Usage: 
	# 
	#  n=NessusXMLRPC::NessusXMLRPC.new('https://localhost:8834','user','pass');
	#  if n.logged_in
	#	n.scan_stop_all
	#  end
	def scan_stop_all
		b=scan_list_uids
		b.each {|uuid|
			scan_stop(uuid)
		}
		return b
	end
	# pause scan identified by scan_uuid
	def scan_pause(uuid)
		post= { "token" => @token, "scan_uuid" => uuid } 
		docxml=nessus_request('scan/pause', post)
		return docxml
	end
	# pause all active scans 
	# 
	# Usage: 
	# 
	#  n=NessusXMLRPC::NessusXMLRPC.new('https://localhost:8834','user','pass');
	#  if n.logged_in
	#	n.scan_pause_all
	#  end
	def scan_pause_all
		b=scan_list_uids
		b.each {|uuid|
			scan_pause(uuid)
		}
		return b
	end
	# remove scan identified by uuid
	def scan_resume(uuid)
		post= { "token" => @token, "scan_uuid" => uuid } 
		docxml=nessus_request('scan/resume', post)
		return docxml
	end
	# resume all active scans 
	# 
	# Usage: 
	# 
	#  n=NessusXMLRPC::NessusXMLRPC.new('https://localhost:8834','user','pass');
	#  if n.logged_in
	#	n.scan_resume_all
	#  end
	def scan_resume_all
		b=scan_list_uids
		b.each {|uuid|
			scan_resume(uuid)
		}
		return b
	end

	# check status of scan identified by uuid
	def scan_status(uuid)
		post= { "token" => @token, "report" => uuid } 
		docxml=nessus_request('report/list', post)
		docxml.root.elements['contents'].elements['reports'].each_element('//report') { |report|
			if report.elements['name'].text == uuid
				return (report.elements['status'].text)
			end
		}
		return ''
	end

	# check if scan is finished (completed to be exact) identified by uuid
	def scan_finished(uuid)
		status=scan_status(uuid)
		if status == "completed"
			return true
		else
			return false
		end
	end
	
	# get report by reportID and return XML file
	# 
	# returns: XML file of report (nessus v2 format)
	def report_file_download(report)
		post= { "token" => @token, "report" => report } 
		file=nessus_http_request('file/report/download', post)
		return file
	end

	# get report by reportID and return XML file (version 1)
	# 
	# returns: XML file of report (nessus v1 format)
	def report_file1_download(report)
		post= { "token" => @token, "report" => report, "v1" => "true" } 
		file=nessus_http_request('file/report/download', post)
		return file
	end
	
	# delete report by report ID
	def report_delete(id)
		post= { "token" => @token, "report" => id } 
		docxml=nessus_request('report/delete', post)
		return docxml
	end

	# get list of names of policies
	#
	# returns: array of names
	def policy_list_names
		post= { "token" => @token } 
		docxml=nessus_request('policy/list', post)
		list = Array.new
		docxml.root.elements['contents'].elements['policies'].each_element('//policy') {|policy|
				list.push policy.elements['policyName'].text
		}
		return list
	end

	# get hosts for particular report
	#
	# returns: array of hosts
	def report_hosts(report_id)
		post= { "token" => @token, "report" => report_id } 
		docxml=nessus_request('report/hosts', post)
		list = Array.new
		docxml.root.elements['contents'].elements['hostList'].each_element('//host') { |host| 
			list.push host.elements['hostname'].text
		}
		return list
	end

	# get host details for particular host identified by report id
	#
	# returns: severity, current, total
	def report_get_host(report_id,host)
		post= { "token" => @token, "report" => report_id } 
		docxml=nessus_request('report/hosts', post)
		docxml.root.elements['contents'].elements['hostList'].each_element('//host') { |host| 
			if host.elements['hostname'].text == host
				retval={}
				retval["severity"] = host.elements['severity'].text
				retval["current"] = host.elements['scanProgressCurrent'].text
				retval["total"] = host.elements['scanProgressTotal'].text
				return retval
			end
		}
	end
	#-- ToDo items
	def plugins_list
		post= { "token" => @token } 
		docxml=nessus_request('plugins/list', post)
		return docxml
	end
	def users_list
		post= { "token" => @token } 
		docxml=nessus_request('users/list', post)
		return docxml
	end
end # end of NessusXMLRPC::Class

# use nokogiri if available (it's faster!)
nokogiri=true
begin
	require 'nokogiri'
rescue LoadError
	nokogiri=false
end

# if found nokogiri
if nokogiri
# Class which uses nokogiri to parse nessus XML RPC replies. 
# It is adviseable to use NessusXMLRPC class, not this class directly.
# As NessusXMLRPC class will use nokogiri or rexml, depending on availability.
# 
# Documentation for this class documents only differences from NessusXMLRPCrexml.
# <b> So, check NessusXMLRPCrexml for method documentation </b>
class NessusXMLRPCnokogiri < NessusXMLRPCrexml
	# send standard Nessus XML request and check
	#
	# return: nokogiri XML file
	def nessus_request(uri, post_data) 
		body=nessus_http_request(uri, post_data)
		docxml = Nokogiri::XML.parse(body)
		begin 
		status = docxml.xpath("/reply/status").collect(&:text)[0]
		rescue
			puts "[e] error in XML parsing"
		end
		if status == "OK"
			return docxml 
		else 
			return ''
		end
	end

	def login(user, password)
		post = { "login" => user, "password" => password }
		docxml=nessus_request('login', post)
		if docxml == '' 
			@token=''
		else
			@token = docxml.xpath("/reply/contents/token").collect(&:text)[0]
			@name = docxml.xpath("/reply/contents/user/name").collect(&:text)[0]
			@admin = docxml.xpath("/reply/contents/user/admin").collect(&:text)[0]
		end
			
	end

	def scan_new(policy_id,scan_name,target)
		post= { "token" => @token, "policy_id" => policy_id, "scan_name" => scan_name, "target" => target } 
		docxml=nessus_request('scan/new', post)
		if docxml == '' 
			return ''
		else
			uuid=docxml.xpath("/reply/contents/scan/uuid").collect(&:text)[0]
			return uuid
		end	
	end

	def scan_status(uuid)
		post= { "token" => @token, "report" => uuid } 
		docxml=nessus_request('report/list', post)
		return docxml.xpath("/reply/contents/reports/report/name[text()='"+uuid+"']/../status").collect(&:text)[0]
	end

	def scan_list_uids
		post= { "token" => @token } 
		docxml=nessus_request('scan/list', post)
		return docxml.xpath("/reply/contents/scans/scanList/scan/uuid").collect(&:text)
	end

	def scan_list_hash
		post= { "token" => @token } 
		docxml=nessus_request('scan/list', post)
		items = docxml.xpath("/reply/contents/scans/scanList/scan")
		retval = items.collect do |item|
			tmpitem = {}
			[
				[:id, 'uuid'],
				[:name, 'readableName'],
				[:current, 'completion_current'],
				[:total, 'completion_total']
			].collect do |key, xpath|
			tmpitem[key] = item.at_xpath(xpath).content
			end
			tmpitem
		end
		return retval
	end

	def policy_get_id(textname) 
		post= { "token" => @token } 
		docxml=nessus_request('policy/list', post)
		return docxml.xpath("/reply/contents/policies/policy/policyName[text()='"+textname+"']/..policyID").collect(&:text)[0]
	end	

	def policy_list_uids
		post= { "token" => @token } 
		docxml=nessus_request('policy/list', post)
		return docxml.xpath("/reply/contents/policies/policy/policyID").collect(&:text)
	end

	def policy_get_first
		post= { "token" => @token } 
		docxml=nessus_request('policy/list', post)
		id=docxml.xpath("/reply/contents/policies/policy/policyID").collect(&:text)[0]
		name=docxml.xpath("/reply/contents/policies/policy/policyName").collect(&:text)[0]
		return id, name
	end	

	def policy_list_names
		post= { "token" => @token } 
		docxml=nessus_request('policy/list', post)
		return docxml.xpath("/reply/contents/policies/policy/policyName").collect(&:text)
	end

	def report_hosts(report_id)
		post= { "token" => @token, "report" => report_id } 
		docxml=nessus_request('report/hosts', post)
		return docxml.xpath("/reply/contents/hostList/host/hostname").collect(&:text)
	end

	def report_get_host(report_id,host)
		post= { "token" => @token, "report" => report_id } 
		docxml=nessus_request('report/hosts', post)
		items = docxml.xpath("/reply/contents/hostList/host/hostname[text()='"+host+"']")
		retval = items.collect do |item|
			tmpitem = {}
			[
				[:severity, 'severity'],
				[:current, 'scanProgressCurrent'],
				[:total, 'scanProgressTotal']
			].collect do |key, xpath|
			tmpitem[key] = item.at_xpath(xpath).content
			end
			tmpitem
		end
		return retval
	end
		
end # end of NessusXMLRPCnokogiri::Class
	# Main class which controls Nessus using XMLRPC. 
	# It is adviseable to use this NessusXMLRPC class, and not NessusXMLRPCnokogiri or NessusXMLRPCrexml,
	# As NessusXMLRPC class will use nokogiri or rexml, depending on availability. 
	# Of course, choosing nokogiri first because of speed.
	# 
	# Example:
	# 
	#  n=NessusXMLRPC::NessusXMLRPC.new('https://localhost:8834','user','pass');
	#  if n.logged_in
	# 	id,name = n.policy_get_first
	# 	uid=n.scan_new(id,"textxmlrpc","127.0.0.1")
	#	puts "status: " + n.scan_status(uid)
	#  end
	# 
	# Check NessusXMLRPCrexml for description of methods implemented 
	# (for both NessusXMLRPCnokogiri and NessusXMLRPCrexml).
	class NessusXMLRPC < NessusXMLRPCnokogiri
	end
else # nokogiri not found, use REXML
	class NessusXMLRPC < NessusXMLRPCrexml
	end
end # if nokogiri

end # of Module

EOT
    fi
fi

### Make sure there is a "scan" directory
SCANS_DIR=/var/scans
mkdir -p $SCANS_DIR

# Update nessus signatures?
#NOT YET#NOW=$(date +%s)
#NOT YET#NRC_AGE=0
#NOT YET#[ -f /opt/nessus/var/nessus/last_update.timestamp ] && NRC_AGE=$(date +%s -r /opt/nessus/var/nessus/last_update.timestamp)
#NOT YET#AGE=$(($NOW - $NRC_AGE))
#NOT YET#if [ $AGE -gt 172800 -a $AGE -lt 604800 ]
#NOT YET#then
#NOT YET#    NRC_AGE=0
#NOT YET#    [ -f /opt/nessus/lib/nessus/plugins/MD5 ] && NRC_AGE=$(date +%s -r /opt/nessus/lib/nessus/plugins/MD5)
#NOT YET#    AGE=$(($NOW - $NRC_AGE))
#NOT YET#    if [ $AGE -gt 172800 -a $AGE -lt 604800 ]
#NOT YET#    then
#NOT YET#        echo 'Please update nessus signatures first by typing in:'
#NOT YET#        echo '/opt/nessus/sbin/nessus-update-plugins'
#NOT YET#        exit 0
#NOT YET#    fi
#NOT YET#fi

### Determine if the scan will run on the local server or a remote scanner
SNS_Host=localhost
read -p 'Do you want to run the scan from this device (y/N) ? ' SNS_Local
if [ -z "$SNS_Local" ]
then
    SNS_Local='n'
else
    SNS_Local=${SNS_Local,,}
fi
[ "T${SNS_Local:0:1}" = 'Ty' ] || read -p 'What is the IP address of the remote agent ? ' SNS_Host

### Enter a name for the scan
read -p 'Enter the customer name for this scan [ex: customer.domain-ext] :' SNS_Name
[ -f *$SNS_Name ] && rm -f *$SNS_Name
mkdir -p $SCANS_DIR/$SNS_Name

if [ "T${SNS_Local:0:1}" = 'Ty' ]
then
    # Decide whether to create a scan file using network discovery tool or manually entered IP's
    cat << EOT
Do you want to:
(1) Have a list of test addresses created automatically using a network inventory tool
(2) manually enter the target addresses
EOT
    SNS_Target_Acquisition=0
    while [ $SNS_Target_Acquisition -lt 1 -o $SNS_Target_Acquisition -gt 2 ]
    do
        read -p 'Please enter either "1" or "2" to continue ' SNS_Target_Acquisition
        [ -z "$SNS_Target_Acquisition" ] && SNS_Target_Acquisition=0
    done
else
    # For remote execution always use the target file
    SNS_Target_Acquisition=2
fi

# By default force the user to enter the target addresses by hand
if [ $SNS_Target_Acquisition -eq 1 ]
then
    ### Run the inventory tool to create a target file
    rm -f /tmp/$$.*
    read -p "Enter the IP range(s) in an nmap acceptable format. (eg: x.x.x.x/x, x.x.x.x-x, etc.). Press any key to continue"
    $EDITOR /tmp/$$.input
    nmap -n -T4 -F -iL /tmp/$$.input -oG /tmp/$$.output
    awk '/ting ports/ {sub(/:/,"",$NF);print $NF}' /tmp/output | sort -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4 > $SCANS_DIR/$SNS_Name/NI.iprange
    rm -f /tmp/$$.*
fi
read -p 'A file will now open, enter the network range to be scanned. eg: x.x.x.x/x, x.x.x.y-z, or individual IP, one per line. Hit "enter" to continue'
$EDITOR $SCANS_DIR/$SNS_Name/NI.iprange

### Verify the SNS_addresses were entered correctly
while [ 1 ]
do
    cat $SCANS_DIR/$SNS_Name/NI.iprange
    read -p 'Does the above list look correct (Y/n) ? ' SNS_Confirm
    if [ -z "$SNS_Confirm" ]
    then
        SNS_Confirm='y'
    else
        SNS_Confirm=${SNS_Confirm,,}
    fi
    [ "T${SNS_Confirm:0:1}" = 'Ty' ] && break
    $EDITOR $SCANS_DIR/$SNS_Name/NI.iprange
done

### Enter the customer's legal name for the report title
read -p 'A file will now open, Enter the customer name as it is to be presented in the report title. Hit "enter" to continue '
$EDITOR $SCANS_DIR/$SNS_Name/CustomerName.in

### Verify the customer's legal name is correct
SNS_Confirm='n'
while [ 1 ]
do
    cat $SCANS_DIR/$SNS_Name/CustomerName.in
    read -p 'Does the above list look correct (Y/n) ? ' SNS_Confirm
    if [ -z "$SNS_Confirm" ]
    then
        SNS_Confirm='y'
    else
        SNS_Confirm=${SNS_Confirm,,}
    fi
    [ "T${SNS_Confirm:0:1}" = 'Ty' ] && break
    $EDITOR $SCANS_DIR/$SNS_Name/CustomerName.in
done

### Choose a scan type to run.
# Need to edit this section to display policies by running the command
#  "nessus-cli.rb --user $NESSUS_USER --password $NESSUS_PASS --list-policy"
#  inside of /usr/local/sbin/nessus-xmlrpc-0.3/bin
cat << EOT
(1) "Internal Network Scan"
(2) "External Network Scan"
(3) "Prepare for PCI DSS audits"
(4) "Web App Tests"
EOT
read -p 'What type of scan do you want to run: ' SNS_Policy
[ -z "$SNS_Policy" ] && SNS_Policy=0

while [ 1 ]
do
    case $SNS_Policy in
    1)  SNS_option='Internal Network Scan'
        break
        ;;
    2)  SNS_option="External Network Scan"
        break
        ;;
    3)  SNS_option="Prepare for PCI DSS audits"
        break
        ;;
    4)  SNS_option="Web App Tests"
        break
        ;;
    *)  read -p 'Please enter a valid choice to continue ' SNS_Policy
        if [ -z "$SNS_Policy" ]
        then
            SNS_Policy=0
            break
        fi
        ;;
     esac
done
if [ $SNS_Policy -lt 1 ]
then
    echo "ERROR: Invalid scheduling policy specified"
    exit
fi

### Ask who to notify when scan completes
read -p 'Who should I notify when the scan completes? (enter email address) ' Email_Notification

### Schedule the scan run (remove any existing command file first)
[ -f /$SNS_Name/SNS_At_CmdFile ] && rm -f /$SNS_Name/SNS_At_CmdFile

read -p 'Do you want to run the scan now, ie. in 1 minute (y/N) ?' SNS_Now
if [ -z "$SNS_Now" ]
then
    SNS_Now='n'
else
    SNS_Now=${SNS_Now,,}
fi
set $(date "+%F %R" -d'+1 minutes')
SNS_Date="$1"
SNS_Time="$2"
if [ "T${SNS_Now:0:1}" != 'Ty' ]
then
    read -p "Please enter a date you would like to run the scan. Format is YYYY-MM-DD [$SNS_Date]: " SNS_Date_in
    [ -z "$SNS_Date_in" ] || SNS_Date="$SNS_Date_in"
    read -p "Please enter the time you would like to start the scan. Format is HH:MM [$SNS_Time] : " SNS_Time_in
    [ -z "$SNS_Time_in" ] || SNS_Time="$SNS_Time_in"
fi
echo "Running '$SNS_Name' at $SNS_Date $SNS_Time"

cat << EOT > $SCANS_DIR/$SNS_Name/SNS_At_CmdFile
/var/lib/gems/1.8/bin/nessus-cli.rb --user $NESSUS_USER --password $NESSUS_PASS --scan $SCANS_DIR/$SNS_Name --policy "$SNS_option" --url https://$SNS_Host:8834 --wait --output $SCANS_DIR/$SNS_Name/$SNS_Name.nessus --target file://$SCANS_DIR/$SNS_Name/NI.iprange --verbose 2> $SCANS_DIR/$SNS_Name/scanlog.txt
date >> $SCANS_DIR/$SNS_Name/scanlog.txt
echo "=>  $SNS_Name scan complete" >> $SCANS_DIR/$SNS_Name/scanlog.txt
EOT
if [ -z "$Email_Notification" ]
then
    cat << EOT >> $SCANS_DIR/$SNS_Name/SNS_At_CmdFile
echo "Scan Status for $SNS_Name"
cat $SCANS_DIR/$SNS_Name/scanlog.txt
EOT
else
    cat << EOT >> $SCANS_DIR/$SNS_Name/SNS_At_CmdFile
mailx -a 'Importance: High' -a 'X-Priority: High' -s "Scan Status for $SNS_Name" $Email_Notification < $SCANS_DIR/$SNS_Name/scanlog.txt
EOT
fi
cat << EOT >> $SCANS_DIR/$SNS_Name/SNS_At_CmdFile
#:Name:$SNS_Name
#:Policy:$SNS_option
#:Notify:$Email_Notification
#:Host:$SNS_Host
#:Start:$SNS_Date $SNS_Time
EOT
[[ $- = *x* ]] && cat $SCANS_DIR/$SNS_Name/SNS_At_CmdFile

# Rename files left over from a previous run
[ -f $SCANS_DIR/$SNS_Name/scanlog.txt ] && mv -f $SCANS_DIR/$SNS_Name/scanlog.txt $SCANS_DIR/$SNS_Name/scanlog.txt.previous
[ -f $SCANS_DIR/$SNS_Name/$SNS_Name.nessus ] && mv -f $SCANS_DIR/$SNS_Name/$SNS_Name.nessus $SCANS_DIR/$SNS_Name/$SNS_Name.nessus.previous

# Finally schedule the test
at -f $SCANS_DIR/$SNS_Name/SNS_At_CmdFile $SNS_Time $SNS_Date

# We are done
exit 0
