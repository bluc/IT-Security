#!/bin/bash
################################################################
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG='-q'
[[ $- = *x* ]] && DEBUG='-v'

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)

# Required OS packages
[ -x /usr/bin/sshpass ] || apt install sshpass

# Required scripts from B-LUC GIT repository
for BGS in UpdateBlocklists.sh UpdateBlocklists-Fortigate.sh
do
    wget $DEBUG http://raw.githubusercontent.com/B-LUC/AdminTools/master/usr/local/sbin/$BGS -O /tmp/$BGS
    diff -wu /tmp/$BGS /usr/local/sbin/$BGS &> /dev/null
    [ $? -ne 0 ] && cat /tmp/$BGS > /usr/local/sbin/$BGS
done

# Get the credentials for the Fortigate
if [ -s /var/tmp/fortigate-credentials ]
then
    source /var/tmp/fortigate-credentials
else
    read -p 'IP address for Fortigate firewall ? ' FG_IP
    read -p 'User id for Fortigate firewall ? ' FG_USER
    read -p 'Password for Fortigate firewall ? ' FG_PASS
fi
echo '# Credentials for Fortigate firewall' > /var/tmp/fortigate-credentials
chown root: /var/tmp/fortigate-credentials
chmod 600 /var/tmp/fortigate-credentials
cat << EOT >> /var/tmp/fortigate-credentials
FG_IP=$FG_IP
FG_USER=$FG_USER
FG_PASS=$FG_PASS
EOT

# Get a copy of the current firewall config
BKP_FILE=$(tempfile)
SSHPASS="$FG_PASS" sshpass -e scp -o StrictHostKeyChecking=no $DEBUG \
  -o PreferredAuthentications=password,publickey,keyboard-interactive \
  ${FG_USER}@${FG_IP}:sys_config $BKP_FILE
if [ -s $BKP_FILE ]
then
    chmod 600 $BKP_FILE
    # Update the Fortigate firewall with blocklists
    (cd /usr/local/sbin; ./UpdateBlocklists-Fortigate.sh)
    if [ -s /tmp/Fortigate.block-script ]
    then
        # Create the update script
        UPDATE_SCRIPT=$(tempfile)
        echo 'config firewall addrgrp' > $UPDATE_SCRIPT
        egrep 'edit.*BAD_(HOST|NET)S_' $BKP_FILE | sed 's/edit/delete/g' >> $UPDATE_SCRIPT
        echo 'end' >> $UPDATE_SCRIPT
        echo 'config firewall address' >> $UPDATE_SCRIPT
        egrep 'edit.*BAD_(HOST|NET)_' $BKP_FILE | sed 's/edit/delete/g' >> $UPDATE_SCRIPT
        echo 'end' >> $UPDATE_SCRIPT
        grep -v 'delete' /tmp/Fortigate.block-script >> $UPDATE_SCRIPT

        # Apply the changes
        SSHPASS=$FG_PASS sshpass -e ssh -o StrictHostKeyChecking=no $DEBUG \
          -o PreferredAuthentications=password,publickey,keyboard-interactive \
          ${FG_USER}@${FG_IP} < $UPDATE_SCRIPT &> /dev/null
         if [ $? -ne 0 ]
         then
             echo "ERROR: Couldn't update Fortigate configuration"
        fi

        # Remove the script and the config file
        rm -f ${UPDATE_SCRIPT}* $BKP_FILE
    else
        echo "ERROR: Couldn't get current Fortigate configuration"
    fi
fi

# We are done
exit 0
