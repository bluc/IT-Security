IT-Security
===========

Over the years, I conducted many IT Security related tasks ranging from
checking systems and networks for vulnerabilities, auditing configuratuions,
policies and procedures to designing new or updated secure systems and
networks and even managing security related assets.

This repository contains a number of scripts which I have written to make my
life easier.

1. FirewallConfigReview.sh

Using the open-source version of "nipper", this tool audits existing
firewall configurations.  It is basically just a "fancy" shell around the
nipper tool.  "nipper" itself is not hosted here, but is available like
this:

'svn checkout http://nipper-ng.googlecode.com/svn/trunk/ nipper-ng-read-only'

2. CreateVAPackage.pl

This script uses the (xml) output of Nessus 4.x (and higher) to create a set
of documents which an auditor can use to present any vulnerabilities found
by "nessus" to a number of audiences:

- An executive summary for "C" level executives
- A technical summary for IT Directors to help them prioritize
   a remediation plan
- A technical detail documents for the IT Security professional
   to help him/her to find descriptions of each vulnerability,
   any background information (eg. CVE, CVSS, ...) and also
   references to the actual fix (eg. Knowledgebase).
- A comments file to list any comments that the system or network
   owner or maintainer might have (eg. "System has since been
   taken offline").

All the output documents are automatically packaged in a ".zip" file for
convenient sharing via email, FTP, file sharing sites, or similar.

The script uses a configuration file (a template of which is also included
in the repository) which contains specific configurations to adapt the tool
to the intended target environment.

3. StartNessusScan.sh

This script manages a "nessus" scan on the local or a remote nessus server.  It can
schedule a "nessus" scan, automatically retrieves the results once the scan has
finished (eg. to use it as input for the "CreatePackage.pl" script), and optionally
notify someone via email about the presence of new results.

4. sea

A package that implements a Security Engineering Assessment system using spoofed emails.

License
=======

All tools are licensed under the Apache License, Version 2.0
(the "License"); you may not use these files except in compliance
with the License. You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

