#!/bin/bash
###############################################################################
PROG_VERSION='2.0'
#
# (c) Copyright 2013 B-LUC Consulting Thomas Bullinger
#
# Authoritative repository:
# https://gitlab.com/bluc/IT-Security.git
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
###############################################################################

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/home/va/bin

# Be gentle to disk I/O
ionice -c2 -n7 -p $$

# The nipper tool
if [ -x /usr/bin/nipper ]
then
    cat << EOT
ERROR: The tool 'nipper' is not installed!

Please download it via 'svn checkout http://nipper-ng.googlecode.com/svn/trunk/ nipper-ng-read-only'
 or request it from consult@btoy1.net

Once you have the source tool, install it as described in the 'INSTALL' file.
EOT
    exit 0
fi

# The work directory
VA_HOME=$(getent passwd va | cut -d: -f6)
if [ -z "$VA_HOME" ]
then
    echo "No home directory for account 'va' found"
    exit 0
fi
WORKDIR=$VA_HOME/fw-reports
mkdir -p $WORKDIR

# You must be root to run this script!
[ $EUID -eq 0 ] || exit 0

# Determine which type of device were are testing
cat << EOT
(1) Cisco Router
(2) PIX
(3) ASA
(4) Cisco FWSM
(5) Sonicwall
(6) Checkpoint FW-1
(7) Juniper Netscreen
EOT
read -p 'Which firewall type are you reviewing ' dev_type
[ -z "$dev_type" ] && dev_type=0
while [ 1 ]
do
    case ${dev_type:0:1} in
    1)  NTYPE='ios-router'
        break
        ;;
    2)  NTYPE='pix'
        break
        ;;
    3)  NTYPE='asa'
        break
        ;;
    4)  NTYPE='sonicos'
        break
        ;;
    5)  NTYPE='fwsm'
        break
        ;;
    6)  NTYPE='cp-firewall'
        break
        ;;
    7)  NTYPE='screenos'
        break
        ;;
    *)  read -p 'Please pick a valid number: ' dev_type
        [ -z "$dev_type" ] && dev_type=0
        ;;
    esac
done

# Tell me where the config file is located
read -p 'Enter the path to the firewall configuration file  ' config_path
[ -z "$config_path" ] && exit 0

# Tell me what to name the report
read -p 'Enter the name for the report  ' report_name
[ -z "$report_name" ] && exit 0

# Run the proper command to create the report
mkdir -p $WORKDIR
# Run the nipper tool
nipper --$NTYPE --input=$config_path --output=$WORKDIR/$report_name
echo "The report is located in $WORKDIR/$report_name.html"

# We are done
exit 0
